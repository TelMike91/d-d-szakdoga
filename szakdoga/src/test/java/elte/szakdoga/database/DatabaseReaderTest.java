package elte.szakdoga.database;

import static org.junit.Assert.assertEquals;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import elte.szakdoga.actors.models.enemy.Enemy;
import elte.szakdoga.items.models.HPConsumable;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.items.models.offense.OffenseEquipment;
import elte.szakdoga.items.models.offense.OneHandedSword;

// DBUnit testing later
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { DatabaseReaderTestConfig.class })
public class DatabaseReaderTest {
	private static DatabaseReader dr;
	private static AbstractApplicationContext apc;

	@BeforeClass
	public static void init() {
		apc = new AnnotationConfigApplicationContext(DatabaseReaderTestConfig.class);
		dr = apc.getBean(DatabaseReader.class);
	}

	@Test
	public void testTestSwordById() {
		Item item = dr.getItemById(1);
		OffenseEquipment sword = (OneHandedSword) item;

		assertEquals(100, sword.getAttackPoint());
		assertEquals(2, sword.getEffects().size());
	}
	
	@Test
	public void testWolfById() {
		Enemy enemy = dr.getEnemyById(1);
		
		assertEquals("Wolf", enemy.getName());
	}

	@Test
	public void testGetOintmentById() {
		Item item = dr.getItemById(2);
		HPConsumable hpConsumable = (HPConsumable) item;
		assertEquals("Ointment", hpConsumable.getName());
		assertEquals(100, hpConsumable.getValue());
	}

	@AfterClass
	public static void destruct() {
		apc.close();
	}
}
