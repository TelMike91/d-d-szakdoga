package elte.szakdoga.database;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
public class DatabaseReaderTestConfig {
    @Bean
    public DatabaseReader databaseReader() {
        return new DatabaseReader(jdbc());
    }
    
    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        // this should be in-memory later
        dataSource.setDriverClassName("org.sqlite.JDBC");
        dataSource.setUrl("jdbc:sqlite:dnd.db");
//        dataSource.setUrl("jdbc:mysql://localhost/dnd");
//        dataSource.setDriverClassName("org.h2.Driver");
//        dataSource.setUrl("jdbc:h2:mem:dnd;DB_CLOSE_DELAY=-1");
//        dataSource.setUsername("root");
//        dataSource.setPassword("LoLPer4Smt3NocT145");
        return dataSource;
    }
    
    @Bean
    public JdbcTemplate jdbc() {
        JdbcTemplate jdbc = new JdbcTemplate();
        jdbc.setDataSource(dataSource());
        return jdbc;
    }
}
