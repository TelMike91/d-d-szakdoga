package elte.szakdoga.database;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import elte.szakdoga.actors.models.player.Player;

public class DatabaseWriterTest {

	@Test
	public void test() {
		DatabaseConnection connection = DatabaseConnection.getInstance();
		Player player = Player.createNewPlayer(null);
		player.setName("OK");
		connection.writePlayer(player);	
		
		assertNotNull(connection.getPlayerById(14));
	}

}
