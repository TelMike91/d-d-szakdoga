package elte.szakdoga.items.models.defense;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.items.models.defense.Armor;
import elte.szakdoga.items.models.defense.DefenseEquipment;
import elte.szakdoga.items.models.defense.Head;

public class DefenseEquipmentTest {       
    private static DefenseEquipment headTest1;

    @BeforeClass
    public static void init() {
        headTest1 = new Head(10, "test" ,0);
    }
    
    @Test(expected = NullPointerException.class)
    public void testEffectOnWhenNull() {
    	headTest1.effectOn(null);
    }
    
    @Test
    public void testProperCallWhenHeadEquipment() {
    	Actor actorTest = Mockito.mock(Actor.class);
    	headTest1.equipOn(actorTest);
    	    	
    	Mockito.verify(actorTest).equipHeadGear((Head) headTest1);
    }
    
    @Test
    public void testProperCallWhenArmorEquipment() {
    	Armor armor = new Armor(10, "Test armor", 0);
    	Actor actorTest = Mockito.mock(Actor.class);
    	armor.equipOn(actorTest);
    	    	
    	Mockito.verify(actorTest).equipArmorGear(armor);
    }
    
    @Test
    public void testProperCallWhenLegsEquipment() {
    	Foot armor = new Foot(10, "Test armor", 0);
    	Actor actorTest = Mockito.mock(Actor.class);
    	armor.equipOn(actorTest);
    	    	
    	Mockito.verify(actorTest).equipLegGear(armor);
    }
    
    @Test
    public void testProperCallWhenArmsEquipment() {
    	Arms armor = new Arms(10, "Test armor", 0);
    	Actor actorTest = Mockito.mock(Actor.class);
    	armor.equipOn(actorTest);
    	    	
    	Mockito.verify(actorTest).equipArmsGear(armor);
    }
    
    @Test
    public void testHashCode() {
        DefenseEquipment headTest2 = new Head(10, "test", 0);
        assertTrue(headTest1.hashCode() == headTest1.hashCode());
        assertTrue(headTest1.hashCode() == headTest2.hashCode());
    }
    
    @Test
    public void testFalseEqualsWhenNull() {
        assertFalse(headTest1.equals(null)); 
    }
    
    @Test
    public void testFalseEquals() {
    	DefenseEquipment armorTest = new Armor(10, "test", 0);       
        assertFalse(headTest1.equals(armorTest));
    }
    
    @Test
    public void testEqualsReflexivity() {
        assertTrue(headTest1.equals(headTest1));
    }
    
    @Test
    public void testEqualsSymmetry() {
    	DefenseEquipment headTest2 = new Head(10, "test", 0);
        assertTrue(headTest1.equals(headTest2));
        assertTrue(headTest2.equals(headTest1));
    }
    
    @Test
    public void testEqualsTransitivity() {
    	DefenseEquipment headTest2 = new Head(10, "test", 0);
    	DefenseEquipment headTest3 = new Head(10, "test", 0);
        assertTrue(headTest1.equals(headTest2));
        assertTrue(headTest2.equals(headTest3));
        assertTrue(headTest1.equals(headTest3));
    }

}
