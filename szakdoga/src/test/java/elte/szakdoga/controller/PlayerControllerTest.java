package elte.szakdoga.controller;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;

import elte.szakdoga.actors.controllers.PlayerController;
import elte.szakdoga.actors.controllers.PlayerControllerInterface;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.items.models.Item;

public class PlayerControllerTest {
	private PlayerControllerInterface playerController;
	private Player player;
	
	@Before
	public void setUp() {
		playerController = new PlayerController();
		player = mock(Player.class);
		playerController.setPlayer(player);
	}
	
	@Test
	public void testStuff() {
		Item item = mock(Item.class);
		
		playerController.useItem(item);
		
		verify(item).effectOn(player);
	}
	
	@Test
	public void testDamage() {
		playerController.damage(100);
		
		verify(player).damage(100);
	}
		
}
