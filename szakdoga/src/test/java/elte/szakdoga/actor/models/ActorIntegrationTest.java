package elte.szakdoga.actor.models;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.actors.models.Position;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.items.models.HPConsumable;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.items.models.defense.Armor;
import elte.szakdoga.items.models.defense.Arms;
import elte.szakdoga.items.models.defense.Head;
import elte.szakdoga.items.models.effects.IncreaseMaxHPEffect;
import elte.szakdoga.items.models.effects.IncreaseStrengthEffect;
import elte.szakdoga.items.models.offense.OneHandedSword;

public class ActorIntegrationTest {
    private static Actor chara;
    private static Item consumableHP;
    private static Item consumableHP2;
    
    private Player createDefaultPlayer(Position position) {
    	Player player = Player.createNewPlayer(position);
    	player.setHP(100);
    	player.setStrength(100);
    	player.setLuck(100);
    	player.setEndurance(100);
    	player.setAgility(100);
    	return player;
    }
    
    @Before
    public void init() {
        chara = createDefaultPlayer(new Position(1,1));
        consumableHP = new HPConsumable(100, "Ointment", 0);
        consumableHP2 = new HPConsumable(1000, "Ointment", 0);
    }   
    
    @Test
    public void testEffectDefenseEquipment() {
        Armor effectEquipment = new Armor(10, "test", 0);
        effectEquipment.addEffect(new IncreaseMaxHPEffect(100));
        chara.equipArmorGear(effectEquipment);
        Head headEffectEquipment = new Head(10, "test", 0);
        headEffectEquipment.addEffect(new IncreaseMaxHPEffect(100));
        headEffectEquipment.addEffect(new IncreaseStrengthEffect(10));
        assertEquals(300, chara.getMaxHP());        
        chara.equipHeadGear(headEffectEquipment);
        assertEquals(400, chara.getMaxHP());
        assertEquals(110, chara.getStrength());
        chara.dequipHeadGear();
        assertEquals(300, chara.getMaxHP());
        assertEquals(100, chara.getStrength());
    }
    
    @Test
    public void testOffenseEquipment() {
        chara.equipRightHand(new OneHandedSword(10, "test", 0));
        assertEquals(10, chara.getAttackPoint());
    }
    
    @Test
    public void testDefenseEquipment() {
        chara.equipArmorGear(new Armor(10, "test", 0));
        chara.equipArmsGear(new Arms(15, "test2", 0));
        
        assertEquals(100, chara.getEndurance());
        assertEquals(125, chara.getDefensePoint());
    }
    
    @Test
    public void testHPConsumable() {
        chara.setHP(1);
        consumableHP.effectOn(chara);
        assertEquals(new Integer(101), new Integer(chara.getHP()));
        chara.setHP(1);
        consumableHP2.effectOn(chara);
        assertEquals(new Integer(200), new Integer(chara.getHP()));
    }
        
}
