package elte.szakdoga.actor.models;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.actors.models.Position.Direction;

public class PositionTest {
    private Position testPositionEqual1;
    private Position testPositionEqual2;
    private Position testPositionEqual3;
    private Position testPositionNotEqual;
    
    @Before
    public void init() {
        testPositionEqual1 = new Position(1,1);
        testPositionEqual2 = new Position(1,1);
        testPositionEqual3 = new Position(1,1);
        testPositionNotEqual = new Position(1,2);
    }
    
    @Test
    public void testDirectionUP() {
    	Position newPosition = Direction.UP.move(new Position(1,1));
    	
    	assertEquals(1, newPosition.getX());
    	assertEquals(0, newPosition.getY());
    }
    
    @Test
    public void testDirectionDown() {
    	Position newPosition = Direction.DOWN.move(new Position(1,1));
    	
    	assertEquals(1, newPosition.getX());
    	assertEquals(2, newPosition.getY());
    }
    
    
    @Test
    public void testDirectionLeft() {
    	Position newPosition = Direction.LEFT.move(new Position(1,1));
    	
    	assertEquals(0, newPosition.getX());
    	assertEquals(1, newPosition.getY());
    }
    
    
    @Test
    public void testDirectionRight() {
    	Position newPosition = Direction.RIGHT.move(new Position(1,1));
    	
    	assertEquals(2, newPosition.getX());
    	assertEquals(1, newPosition.getY());
    }
    
    @Test
    public void notEqualsTest() {
        assertFalse(testPositionEqual1.equals(testPositionNotEqual));
    }
    
    @Test
    public void nullEqualsTest() {
        assertFalse(testPositionEqual1.equals(null));
    }
    
    @Test
    public void reflexivityEqualsTest() {
        assertTrue(testPositionEqual1.equals(testPositionEqual1));
    }

    @Test
    public void symmetryEqualsTest() {
        assertTrue(testPositionEqual1.equals(testPositionEqual2));
        assertTrue(testPositionEqual2.equals(testPositionEqual1));
    }
    
    @Test
    public void transitivyEqualsTest() {
        assertTrue(testPositionEqual1.equals(testPositionEqual2));
        assertTrue(testPositionEqual2.equals(testPositionEqual3));
        assertTrue(testPositionEqual1.equals(testPositionEqual3));
    }
    
    @Test
    public void hashCodeTest() {
        assertTrue(testPositionEqual1.hashCode() == testPositionEqual2.hashCode());
    }

}
