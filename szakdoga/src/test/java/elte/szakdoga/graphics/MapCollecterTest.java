package elte.szakdoga.graphics;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import elte.szakdoga.game.graphics.MapCollecter;

public class MapCollecterTest {	
	@Test
	public void testFileSearch() {
		List<String >fileNames = MapCollecter.mapNames();
		assertEquals(1, fileNames.size());
	}
}
