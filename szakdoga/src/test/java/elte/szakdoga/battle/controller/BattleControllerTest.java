package elte.szakdoga.battle.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.battle.controller.commands.AttackCommand;
import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.game.controllers.GameControllerInterface;

@RunWith(MockitoJUnitRunner.class)
public class BattleControllerTest {	
	
	private BattleControllerInterface battleController;
	@Mock
	private BattleSystem battleSystem;
	@Mock
	private GameControllerInterface gameController;
	
	@Before
	public void setUp() {
		this.battleController = new BattleController(battleSystem, gameController);
	}

	@Test
	public void testInitBattleField() {
		BattleField battleField = mock(BattleField.class);
		
		battleController.initBattleField(battleField);
		
		assertEquals(battleField, battleController.getBattle());
		assertNotNull(battleController.getView());
		verify(battleField).init();					
	}

	@Test
	public void testOwnControl() {
		BattleField battleField = mock(BattleField.class);
		Player player = mock(Player.class);
		
		battleController.setBattleField(battleField);
		when(battleField.getCurrentActor()).thenReturn(player);
		when(gameController.getCurrentPlayer()).thenReturn(player);
		
		boolean actual = battleController.ownControl();
		
		verify(battleField).getCurrentActor();
		verify(gameController).getCurrentPlayer();
		assertTrue(actual);		
	}

	@Test
	public void testAttack() {
		Actor attacker = mock(Actor.class);
		BattleField battleField = mock(BattleField.class);
		
		battleController.setBattleField(battleField);
		
		when(battleField.getCurrentActor()).thenReturn(attacker);		
		when(battleSystem.executeAttackCommand(any(AttackCommand.class))).thenReturn(true);
		
		battleController.attack(attacker);
		
		verify(battleSystem).nextTurn(battleField);
	}

	@Test
	public void testDoNothing() {
		fail("Not yet implemented");
	}

	@Test
	public void testCheckIfOver() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetView() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetNumberOfEnemies() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetNumberOfFriends() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetEnemyStringRepresentation() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetPlayerStringRepresentation() {
		fail("Not yet implemented");
	}

	@Test
	public void testUpdateText() {
		fail("Not yet implemented");
	}

	@Test
	public void testFlee() {
		fail("Not yet implemented");
	}

	@Test
	public void testDefend() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetBattle() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetEnemy() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetPlayer() {
		fail("Not yet implemented");
	}

	@Test
	public void testNextActor() {
		fail("Not yet implemented");
	}

	@Test
	public void testIsBattleInitiated() {
		fail("Not yet implemented");
	}

	@Test
	public void testUseItem() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetActor() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetCurrentActor() {
		fail("Not yet implemented");
	}

}
