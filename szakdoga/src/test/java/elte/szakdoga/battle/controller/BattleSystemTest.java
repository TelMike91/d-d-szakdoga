package elte.szakdoga.battle.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import elte.szakdoga.actors.models.enemy.Enemy;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.battle.models.BattleField;

public class BattleSystemTest {
	private BattleSystem underTest;
	
	@Before
	public void setUp() {
		underTest = new BattleSystem();
	}
	
	@Test
	public void testWhenAllEnemiesDefeated() {
		BattleField battleField = mock(BattleField.class);
		Player player = mock(Player.class);
		Enemy enemy = mock(Enemy.class);
				
		when(battleField.getEnemies()).thenReturn(Arrays.asList(enemy, enemy));
		when(battleField.getRound()).thenReturn(Arrays.asList(player));
		when(enemy.getGold()).thenReturn(100);
		
		int reward = underTest.calculateGoldReward(battleField);
		
		assertEquals(200, reward);
	}
	
	@Test
	public void testWhenOneEnemyRemains() {
		BattleField battleField = mock(BattleField.class);
		Player player = mock(Player.class);
		Enemy enemy = mock(Enemy.class);
				
		when(battleField.getEnemies()).thenReturn(Arrays.asList(enemy, enemy));
		when(battleField.getRound()).thenReturn(Arrays.asList(enemy, player));
		when(enemy.getGold()).thenReturn(100);
		
		int reward = underTest.calculateGoldReward(battleField);
		
		assertEquals(100, reward);
	}
	
	@Test
	public void testWhenAllEnemyRemains() {
		BattleField battleField = mock(BattleField.class);
		Enemy enemy = mock(Enemy.class);
				
		when(battleField.getEnemies()).thenReturn(Arrays.asList(enemy, enemy));
		when(battleField.getRound()).thenReturn(Arrays.asList(enemy, enemy));
		when(enemy.getGold()).thenReturn(100);
		
		int reward = underTest.calculateGoldReward(battleField);
		
		assertEquals(0, reward);
	}
}
