package elte.szakdoga.battle.models;

import static org.junit.Assert.assertTrue;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import org.mockito.Mockito;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.actors.models.enemy.Enemy;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.battle.models.DiceRollBattleLogic;

public class DiceBattleLogicTest {	
	
	@Test
	public void testTryToFlee() {
		DiceRollBattleLogic diceRollLogic = new DiceRollBattleLogic();
		
		Player playerMock = Mockito.mock(Player.class);
		Enemy enemyMock = Mockito.mock(Enemy.class);
		
		Mockito.when(playerMock.getLuck()).thenReturn(150);
		Mockito.when(enemyMock.getLuck()).thenReturn(0);
		
		List<Actor> players = new LinkedList<>();
		List<Actor> enemies = new LinkedList<>();
		
		players.add(playerMock);
		enemies.add(enemyMock);
		
		assertTrue(diceRollLogic.tryToFlee(players, enemies));
	}
	
	@Test
	public void testTryToAttack() {
		DiceRollBattleLogic diceRollLogic = new DiceRollBattleLogic();
		
		Player playerMock = Mockito.mock(Player.class);
		Enemy enemyMock = Mockito.mock(Enemy.class);
		
		Mockito.when(playerMock.getAgility()).thenReturn(100);
		Mockito.when(enemyMock.getAgility()).thenReturn(0);
		
		assertTrue(diceRollLogic.tryToAttack(playerMock, enemyMock));
	}
}
