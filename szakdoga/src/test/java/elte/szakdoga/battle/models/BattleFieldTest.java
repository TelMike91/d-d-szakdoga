package elte.szakdoga.battle.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import org.mockito.Mockito;

import elte.szakdoga.actors.models.enemy.Enemy;
import elte.szakdoga.actors.models.player.Player;

public class BattleFieldTest {

    @Test
    public void testNumberOfEnemies() {
    	List<Enemy> enemies = new LinkedList<>();
    	enemies.add(mock(Enemy.class));
    	BattleField field = new BattleField(enemies);
    	
    	assertEquals(1, field.getNumberOfEnemies());
    }
    
    @Test
    public void testNextActorRoundability() {
    	List<Enemy> enemies = new LinkedList<>();
    	Enemy enemy = mock(Enemy.class);
    	Mockito.when(enemy.getAgility()).thenReturn(100);
    	enemies.add(enemy);
    	BattleField field = new BattleField(enemies);
    	Player player = Mockito.mock(Player.class);

    	Mockito.when(player.getAgility()).thenReturn(200);
    	field.addPlayer(player);    
    	field.initRound();
    	
    	field.nextActor();
    	field.nextActor();
    	field.nextActor();
    	
    	assertEquals(player, field.getCurrentActor());
    }
    
    @Test
    public void testNextActor() {
    	List<Enemy> enemies = new LinkedList<>();
    	Enemy enemy = Mockito.mock(Enemy.class);
    	Mockito.when(enemy.getAgility()).thenReturn(100);
    	enemies.add(enemy);
    	BattleField field = new BattleField(enemies);
    	Player player = Mockito.mock(Player.class);
    	Mockito.when(player.getAgility()).thenReturn(200);
    	field.addPlayer(player);    
    	field.initRound();
    	
    	field.nextActor();
    	
    	assertEquals(player, field.getCurrentActor());
    }
    
    @Test
    public void testGetPlayer() {
    	List<Enemy> enemies = new LinkedList<>();
    	Enemy enemy = Mockito.mock(Enemy.class);
    	enemies.add(enemy);
    	BattleField field = new BattleField(enemies);
    	Player player = Mockito.mock(Player.class);
    	field.addPlayer(player);
    	
    	assertEquals(player, field.getPlayer(0));
    }
    
    @Test
    public void testGetEnemy() {
    	List<Enemy> enemies = new LinkedList<>();
    	Enemy enemy = Mockito.mock(Enemy.class);
    	Mockito.when(enemy.getHP()).thenReturn(100);
    	Mockito.when(enemy.getName()).thenReturn("Test");
    	enemies.add(enemy);
    	BattleField field = new BattleField(enemies);
    	Player player = Mockito.mock(Player.class);
    	field.addPlayer(player);
    	
    	assertEquals("Test 100", field.getEnemyString(0));
    }  
}
