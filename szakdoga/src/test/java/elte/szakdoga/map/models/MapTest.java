package elte.szakdoga.map.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.actors.models.Position.Direction;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.map.models.fields.Field;

public class MapTest {
    private MyMap testMap;

    @Before
    public void init() {
        testMap = MyMap.createUselessMap(5, 5);
    }
    
    @Test
    public void getFieldTestWhenPositionDoesntExist() {
        Position mockedPosition = Mockito.mock(Position.class);
        Mockito.when(mockedPosition.getX()).thenReturn(-1);
        Mockito.when(mockedPosition.getY()).thenReturn(1);
        assertFalse(testMap.getField(mockedPosition).isPresent());
    }  
    
    @Test
    public void testSmallView() {
    	MyMap anotherMap = MyMap.createUselessMap(100, 100);
    	
    	List<List<Field>> fields = anotherMap.getSmallView(new Position(50,50));
    	
    	assertEquals(50, fields.get(8).get(8).getX());
    	assertEquals(15, fields.size());
    	assertEquals(15, fields.get(0).size());
    }
    
    @Test(expected = NullPointerException.class)
    public void getFieldTestWhenPositionIsNull() {
        testMap.getField(null);
    }
    
    // ez igazából integrációs teszt lesz
    @Test
    public void testUpdatePlayers() {
        List<Player> listOfMockedPlayers = new ArrayList<>();
        Player player1 = Mockito.mock(Player.class);
        Player player2 = Mockito.mock(Player.class);
                
        listOfMockedPlayers.add(player1);
        listOfMockedPlayers.add(player2);
        
        Mockito.when(player1.getPosition()).thenReturn(new Position(1,2));
        Mockito.when(player2.getPosition()).thenReturn(new Position(2,3));
                
        testMap.updatePlayerPosition(player1);
        testMap.updatePlayerPosition(player2); 
        
        verify(player1, times(2)).getPosition();
        verify(player2, times(2)).getPosition();     
    }
    
    @Test
    public void checkValidMove() {
        Position positionTest = new Position(0,0);
        assertFalse(testMap.checkIfMoveIsLegal(Direction.UP.move(positionTest)));
        assertFalse(testMap.checkIfMoveIsLegal(Direction.LEFT.move(positionTest)));
        assertTrue(testMap.checkIfMoveIsLegal(Direction.DOWN.move(positionTest)));
        assertTrue(testMap.checkIfMoveIsLegal(Direction.RIGHT.move(positionTest)));
    }
    
    @Test
    public void testGetSmallMapFromPlayer() {
    	MyMap map = MyMap.createUselessMap(5, 5);
    	MyMap copyMap = map.getSmallMapFromPlayerPositon(new Position(1,1));
    	    	    	
    	assertEquals(map.toString(), copyMap.toString());
    }
    
    @Test
    public void testGetSmallMapFromPlayerLargerMap() {
    	MyMap map = MyMap.createUselessMap(100, 100);
    	MyMap copyMap = map.getSmallMapFromPlayerPositon(new Position(1,1));
    	
    	assertEquals(15, copyMap.getRows());
    	assertEquals(15, copyMap.getColumns());
    	
    	StringBuilder stringBuilder = new StringBuilder();
    	for(int i = 0; i < 15; i++) {
    		for(int m = 0; m < 15; m++) {
    			stringBuilder.append("O ");
    		}
    		stringBuilder.codePointBefore(stringBuilder.length());    		
    		stringBuilder.append("\n");
    	}
    	assertEquals(stringBuilder.toString(), copyMap.toString());
    }

}
