package elte.szakdoga.logic.map.field.events;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.game.controllers.GameControllerInterface;
import elte.szakdoga.map.models.fields.events.BattleEvent;
import elte.szakdoga.map.models.fields.events.EventProcessor;
import elte.szakdoga.map.models.fields.events.ItemEvent;
import elte.szakdoga.map.models.fields.events.TrapEvent;

public class EventProcessorTest {
	private EventProcessor eventProcessor;
	@Mock
	private GameControllerInterface gameController;
	
	@Before
	public void setUp() {
		gameController = Mockito.mock(GameControllerInterface.class);
		eventProcessor = new EventProcessor(gameController);
	}
	
	@Test
	public void testTrapEventProcessing() {
		eventProcessor.processEvent(new TrapEvent(100));
		
		verify(gameController).damage(100);
		verifyNoMoreInteractions(gameController);
	}
	
	@Test
	public void testItemEventProcessing() {
		eventProcessor.processEvent(new ItemEvent(Arrays.asList(null, null)));
				
		verify(gameController, times(2)).addItem(null);
	}
	
	@Test
	public void testBattleEventProcessing() {
		Player player = Mockito.mock(Player.class);
		BattleField battleField = Mockito.mock(BattleField.class);
		BattleEvent battleEvent = new BattleEvent(battleField);				
		
		when(gameController.getCurrentPlayer()).thenReturn(player);
		
		eventProcessor.processEvent(battleEvent);					
		
		verify(gameController).setBattle(battleField);			
	}
}
