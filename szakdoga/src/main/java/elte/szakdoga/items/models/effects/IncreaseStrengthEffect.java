package elte.szakdoga.items.models.effects;

import elte.szakdoga.actors.models.Actor;

public class IncreaseStrengthEffect implements Effect {
    private int increase;

    public IncreaseStrengthEffect(int increase) {
        super();
        this.increase = increase;
    }

    @Override
    public void effect(Actor actor) {
        actor.addBonusStrength(increase);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Increases strength by ");
        stringBuilder.append(increase);
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }
}
