package elte.szakdoga.items.models.offense;

public class Fist extends OffenseEquipment {           
    public Fist() {
        super("Fist", 0);
        attackPoint = 1;
    }
}
