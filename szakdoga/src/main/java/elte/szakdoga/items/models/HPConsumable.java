package elte.szakdoga.items.models;

import java.util.Objects;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.logging.graphics.LoggingFieldLogger;

public class HPConsumable extends Item  {
    private int HP;
    
    public HPConsumable() {
    	super("", 0);
	}
    
    public HPConsumable(int HP, String name, int cost) {
        super(name, cost);
        this.HP = HP;        
        
    }
    
    @Override
    public String toString() {
        return getName() + " increases" + HP + " HP.";
    }

    @Override
    public void effectOn(Actor chara) {
        Objects.requireNonNull(chara);
        if(chara.getMaxHP() <= chara.getHP() + HP) {
            chara.setHP(chara.getMaxHP());
        } else {
            chara.setHP(chara.getHP() + HP);
        }
        LoggingFieldLogger.addText("Recovered HP: " + HP);
    }

    @Override
    public boolean isEquipable() {
        return false;
    }

    @Override
    public boolean isConsumable() {
        return true;
    }

	public int getValue() {
		return HP;
	}    
}
