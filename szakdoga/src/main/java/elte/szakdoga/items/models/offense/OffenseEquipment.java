package elte.szakdoga.items.models.offense;

import java.util.List;
import java.util.Objects;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.items.models.effects.CompositeEffect;
import elte.szakdoga.items.models.effects.Effect;

public abstract class OffenseEquipment extends Item {
    protected int attackPoint;
    protected final CompositeEffect effectOnPlayer;

    public OffenseEquipment(String name, int cost) {
        super(name, cost);
        this.effectOnPlayer = new CompositeEffect();
    }

    public int getAttackPoint() {
        return attackPoint;
    }

    @Override
    public void effectOn(Actor chara) {
        Objects.requireNonNull(chara);
        if (effectOnPlayer != null)
            effectOnPlayer.effect(chara);

    }

    @Override
    public boolean isEquipable() {
        return true;
    }

    @Override
    public boolean isConsumable() {
        return false;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getName());
        stringBuilder.append("\n---------\n");
        stringBuilder.append("Increases attack points by ");
        stringBuilder.append(getAttackPoint());
        if(effectOnPlayer.isEffectMoreThanZero()) {
            stringBuilder.append("\nHas the following effects: \n");
            stringBuilder.append(effectOnPlayer);
        }
        return stringBuilder.toString();
    }

    public void addEffect(Effect e) {
        effectOnPlayer.addEffect(e);
    }

	public List<Effect> getEffects() {
		return effectOnPlayer.getEffects();
	}
}
