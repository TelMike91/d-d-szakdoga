package elte.szakdoga.items.models.defense.nullobjects;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.items.models.defense.Foot;

public class FootNullObject extends Foot {

    public FootNullObject() {
        super(0, "", 0);
    }
    
    @Override
    public void effectOn(Actor actor) {
        
    }

}
