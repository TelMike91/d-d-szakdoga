package elte.szakdoga.items.models.defense.nullobjects;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.items.models.defense.Armor;

public class ArmorNullObject extends Armor {

    public ArmorNullObject() {
        super(0, "", 0);        
    }

    @Override
    public void effectOn(Actor actor) {
        
    }
}
