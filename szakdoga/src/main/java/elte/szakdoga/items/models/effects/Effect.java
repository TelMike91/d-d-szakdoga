package elte.szakdoga.items.models.effects;

import elte.szakdoga.actors.models.Actor;

public interface Effect {
    public void effect(Actor actor);
}
