package elte.szakdoga.items.models.defense.nullobjects;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.items.models.defense.Head;

public class HeadNullObject extends Head {
    public HeadNullObject() {
        super(0, "", 0);
    }
    
    @Override
    public void effectOn(Actor actor) {
        
    }
}
