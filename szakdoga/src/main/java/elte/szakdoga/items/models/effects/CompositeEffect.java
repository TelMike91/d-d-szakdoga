package elte.szakdoga.items.models.effects;

import java.util.LinkedList;
import java.util.List;

import elte.szakdoga.actors.models.Actor;

public class CompositeEffect implements Effect {
    protected List<Effect> effects;
    
    public CompositeEffect() {
        effects = new LinkedList<>();
    }

    @Override
    public void effect(Actor actor) {
        for(Effect e : effects) {
            e.effect(actor);
        }
    }

    public void addEffect(Effect e) {
        effects.add(e);        
    }
    
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for(Effect effect : effects) {
            stringBuilder.append(effect.toString());            
        }
        return stringBuilder.toString();
    }

    public boolean isEffectMoreThanZero() {
        return effects.size() > 0;
    }

	public List<Effect> getEffects() {
		return effects;
	}
    
}
