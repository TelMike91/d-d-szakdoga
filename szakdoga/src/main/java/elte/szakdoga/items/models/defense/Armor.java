package elte.szakdoga.items.models.defense;

import elte.szakdoga.actors.models.Actor;

public class Armor extends DefenseEquipment {
	public Armor() {
		super(0, "", 0);
	}
	
    public Armor(int armorPoint, String name, int cost) {
        super(armorPoint, name, cost);
    }
    
    @Override
    public void equipOn(Actor player) {
        player.equipArmorGear(this);
    }
}
