package elte.szakdoga.items.models.defense;

import elte.szakdoga.actors.models.Actor;

public class Head extends DefenseEquipment {
	public Head() {
		super(0, "", 0);
	}
	
    public Head(int armorPoint, String name, int cost) {
        super(armorPoint, name, cost);
    }

    @Override
    public void equipOn(Actor player) {
        player.equipHeadGear(this);
        
    }
}
