package elte.szakdoga.items.models.effects;

import elte.szakdoga.actors.models.Actor;

public class IncreaseMaxHPEffect implements Effect {
    private int increase;
    
    public IncreaseMaxHPEffect() {
		super();
	}
    
    public IncreaseMaxHPEffect(int increase) {
        super();
        this.increase = increase;
    }

    @Override
    public void effect(Actor actor) {
        actor.addBonusMaxHP(increase);
    }
    
    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Increases maximum HP by ");
        stringBuilder.append(increase);
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }
}
