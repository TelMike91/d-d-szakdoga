package elte.szakdoga.items.models.defense;

import java.util.Objects;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.items.models.effects.CompositeEffect;
import elte.szakdoga.items.models.effects.Effect;

public abstract class DefenseEquipment extends Item {
    protected final int armor;    
    private int hashCode;
    protected final CompositeEffect effectOnPlayer;

    public DefenseEquipment(int armor, String name, int cost) {
        super(name, cost);
        this.armor = armor;        
        this.effectOnPlayer = new CompositeEffect();
    }
    
    public void addEffect(Effect e) {
        effectOnPlayer.addEffect(e);
    }

    public int getArmorPoint() {
        return armor;
    }   

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (other == this) {
            return true;
        }
        if (other instanceof DefenseEquipment) {
            DefenseEquipment de = (DefenseEquipment) other;
            return de.getName().equals(getName()) && de.getArmorPoint() == armor && de.getClass().equals(this.getClass());
        } else {
            return false;
        }

    }
    
    @Override
    public int hashCode() {
        if(hashCode >= 0) {
            return hashCode;
        }
        int code = 0;
        for(char c : getName().toCharArray()) {
            code += Character.hashCode(c);
        }
        code += Integer.hashCode(armor);
        hashCode = code;
        return code;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(getName());
        stringBuilder.append("\n---------\n");
        stringBuilder.append("Increases armor by ");
        stringBuilder.append(armor);
        if(effectOnPlayer.isEffectMoreThanZero()) {
            stringBuilder.append("\nHas the following effects: \n");
            stringBuilder.append(effectOnPlayer);
        }
        return stringBuilder.toString();
    }

    @Override
    public void effectOn(Actor chara) {
        Objects.requireNonNull(chara);
        if(effectOnPlayer != null) {
            effectOnPlayer.effect(chara);
        }
        
    }

    @Override
    public boolean isEquipable() {
        return true;
    }
    
    @Override
    public boolean isConsumable() {
        return false;
    }
    public abstract void equipOn(Actor player);
}
