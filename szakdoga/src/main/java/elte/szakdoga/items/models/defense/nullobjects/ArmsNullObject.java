package elte.szakdoga.items.models.defense.nullobjects;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.items.models.defense.Arms;

public class ArmsNullObject extends Arms {

    public ArmsNullObject() {
        super(0, "", 0);
    }

    @Override
    public void effectOn(Actor actor) {
        
    }
}
