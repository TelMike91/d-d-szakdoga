package elte.szakdoga.items.models.defense;

import elte.szakdoga.actors.models.Actor;

public class Arms extends DefenseEquipment {

    public Arms(int armor, String name, int cost) {
        super(armor, name, cost);
    }

    @Override
    public void equipOn(Actor player) {
        player.equipArmsGear(this);
        
    }

}
