package elte.szakdoga.items.models.offense;

public class OneHandedSword extends OffenseEquipment {
	
	public OneHandedSword() {
        super("", 0);  
	}

    public OneHandedSword(int attackPoint, String name, int cost) {
        super(name, cost);     
        this.attackPoint = attackPoint;
    }
   
}
