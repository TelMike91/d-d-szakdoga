package elte.szakdoga.items.models;

import elte.szakdoga.database.DatabaseConnection;
import elte.szakdoga.items.models.offense.OneHandedSword;

public class ItemFactory {
    public static OneHandedSword createOneHandedSword(String name) {
        return new OneHandedSword(100, name, 100);
    }
    
    public static HPConsumable createDebugHPConsumable() {
    	return new HPConsumable(100, "hpPotion", 100);
    }

	public static Item getItemById(int id) {
		DatabaseConnection connection = DatabaseConnection.getInstance();
		return connection.getItemById(id);
	}
}
