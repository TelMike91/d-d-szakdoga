package elte.szakdoga.items.models;

import elte.szakdoga.actors.models.Actor;

public abstract class Item {
    private String name;
    private int cost;
    
    public Item() {
		// TODO Auto-generated constructor stub
	}
    
    public Item(String name, int cost) {        
        this.name = name;
        this.cost = cost;
    }    

    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    
    
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(name);
        sb.append(" ");
        sb.append(cost);
        return sb.toString();
    }

    public abstract void effectOn(Actor chara);
    public abstract boolean isEquipable();
    public abstract boolean isConsumable();

	public String getFileName() {
		return "assets/items/" + name.toLowerCase() + ".png";
	}

	public int getCost() {
		return cost;
	}
}
