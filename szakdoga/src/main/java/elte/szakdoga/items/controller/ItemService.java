package elte.szakdoga.items.controller;

import java.util.Optional;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.items.models.Item;

public class ItemService {

	public void useItem(Item currentItem, Player managedPlayer) {
		Optional<Item> foundItem = managedPlayer.getEquipment().getItemsList().stream()
					.filter(i -> i.equals(currentItem))
					.findFirst();
		if(foundItem.isPresent()) {
			foundItem.get().effectOn(managedPlayer);
			managedPlayer.removeItem(currentItem);
		}
	}

	public void useItemOnOther(Item item, Actor actor, Actor onActor) {
		Optional<Item> foundItem = actor.getEquipment().getItemsList().stream()
				.filter(i -> i.equals(item))
				.findFirst();
		if(foundItem.isPresent()) {
			foundItem.get().effectOn(onActor);
			actor.removeItem(item);
		}
	}

}
