package elte.szakdoga.items.controller;

import java.util.List;

import com.esotericsoftware.kryonet.rmi.ObjectSpace;

import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.items.models.defense.DefenseEquipment;
import elte.szakdoga.items.models.offense.OffenseEquipment;
import elte.szakdoga.network.MyClient;
import elte.szakdoga.network.rmi.ServerDataInterface;

public class ItemControllerWithNetwork implements ItemControllerInterface {
	private ItemService itemService;
    private Player managedPlayer;
    private Item currentItem;
    private int currentItemId;
    private MyClient client;
    
    public ItemControllerWithNetwork(MyClient client, Player player) {
        this.managedPlayer = player;
        this.client = client;
        this.itemService = new ItemService();
    }    

    /* (non-Javadoc)
	 * @see elte.szakdoga.items.controller.ItemControllerInterface#getPlayer()
	 */
    @Override
	public Player getPlayer() {
        return managedPlayer;
    }

    /* (non-Javadoc)
	 * @see elte.szakdoga.items.controller.ItemControllerInterface#getItemsList()
	 */
    @Override
	public List<Item> getItemsList() {
        return managedPlayer.getItemsList();
    }
    
    /* (non-Javadoc)
	 * @see elte.szakdoga.items.controller.ItemControllerInterface#getCurrentItem()
	 */
    @Override
	public Item getCurrentItem() {
        return currentItem;
    }
    
    /* (non-Javadoc)
	 * @see elte.szakdoga.items.controller.ItemControllerInterface#setCurrentItem(elte.szakdoga.items.models.Item)
	 */
    @Override
	public void setCurrentItem(Item item) {
        this.currentItem = item;
    }

    /* (non-Javadoc)
	 * @see elte.szakdoga.items.controller.ItemControllerInterface#getCurrentItemId()
	 */
    @Override
	public int getCurrentItemId() {
        return currentItemId;
    }

    /* (non-Javadoc)
	 * @see elte.szakdoga.items.controller.ItemControllerInterface#setCurrentItemId(int)
	 */
    @Override
	public void setCurrentItemId(int currentItemId) {
        this.currentItemId = currentItemId;
    }

	/* (non-Javadoc)
	 * @see elte.szakdoga.items.controller.ItemControllerInterface#setPlayer(elte.szakdoga.actors.models.player.Player)
	 */
	@Override
	public void setPlayer(Player player) {
		this.managedPlayer = player;
	}

	@Override
	public void useItem() {
		itemService.useItem(currentItem, managedPlayer);
		ServerDataInterface data = ObjectSpace.getRemoteObject(client.getConnection(), 1, ServerDataInterface.class);
		data.updatePlayer(managedPlayer);
		
	}

	@Override
	public void equipRightHand(OffenseEquipment currentItem) {
		managedPlayer.equipRightHand(currentItem);
		ServerDataInterface data = ObjectSpace.getRemoteObject(client.getConnection(), 1, ServerDataInterface.class);
		data.updatePlayer(managedPlayer);
	}

	@Override
	public void equipOn(Player player) {
		((DefenseEquipment) getCurrentItem()).equipOn(player);
		ServerDataInterface data = ObjectSpace.getRemoteObject(client.getConnection(), 1, ServerDataInterface.class);
		data.updatePlayer(managedPlayer);
	}

	@Override
	public void addItem(Item item) {
		managedPlayer.addItem(item);
		ServerDataInterface data = ObjectSpace.getRemoteObject(client.getConnection(), 1, ServerDataInterface.class);
		data.updatePlayer(managedPlayer);
	}

}
