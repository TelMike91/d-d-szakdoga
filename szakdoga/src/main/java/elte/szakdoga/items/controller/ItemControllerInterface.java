package elte.szakdoga.items.controller;

import java.util.List;

import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.items.models.offense.OffenseEquipment;

public interface ItemControllerInterface {

	Player getPlayer();

	List<Item> getItemsList();

	Item getCurrentItem();

	void setCurrentItem(Item item);

	int getCurrentItemId();

	void setCurrentItemId(int currentItemId);

	void setPlayer(Player player);

	void useItem();

	void equipRightHand(OffenseEquipment currentItem);

	void equipOn(Player player);
	
	void addItem(Item item);

}