package elte.szakdoga.items.graphics;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.graphics.listeners.ItemPanelMouseListener;
import elte.szakdoga.items.controller.ItemControllerInterface;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.logging.graphics.LoggingFieldLogger;

/**
 * A tárgyhasználathoz tartozó grafikai reprezentációja.
 * @author Teleki Miklós
 *
 */
@SuppressWarnings("serial")
public class ItemPanel extends JPanel {
    private Map<Integer, ItemImageLabel> itemImageMap;
    private static int idOfImages = 1;
    private JButton equipOrUseButton;
    private ItemControllerInterface itemController;
    private GridBagConstraints gbc = new GridBagConstraints();

    /**
     * Egy belső osztály, ami összekapcsolja a tárgyat és az id-t, és azt megjelenítjük a tárgy panelben.
     * @author Teleki Miklós
     *
     */
    public class ItemImageLabel extends JLabel {
        private final int id;
        private final Item item;

        /**
         * Létrehozzuk a JLabel-t ami tartalmazza a tárgy grafikáját, és annak adatait.
         * @param id Az id ami később kell az eltávolításhoz
         * @param icon Az icon, amit szeretnénk megjeleníteni a JLabelben.
         * @param item A tárgy adatai, amit szeretnénk fölhasználni, akkor amikor a játékos használja a tárgyat.
         */
        public ItemImageLabel(int id, ImageIcon icon, Item item) {
            super(icon);
            this.id = id;
            this.item = item;
            this.addMouseListener(new ItemPanelMouseListener(this, itemController, equipOrUseButton));
        }

        public int getId() {
            return id;
        }

        public Item getItem() {
            return item;
        }
    }

    public ItemPanel(ItemControllerInterface itemController) {    	
        this.itemController = itemController;
        itemImageMap = new HashMap<>();
        setLayout(new GridBagLayout());
        gbc.gridx = 3;
        gbc.gridy = 0;
        gbc.insets = new Insets(0, 50, 0, 0);
        equipOrUseButton = new JButton("Equip");
        equipOrUseButton.setEnabled(false);
        add(equipOrUseButton, gbc);
    }
    
    private void addConsumableItems() {
        for (Item item : itemController.getItemsList()) {
        	if(item.isConsumable())
        		addImage(item);
        }
    }
    
    private void addEquipableItems() {
        for (Item item : itemController.getItemsList()) {
        	if(!item.isConsumable())        		
        		addImage(item);
        }
    }

    public void updateItemPanel(boolean consumableOnly) {
        if (itemController.getPlayer() != null) {
            for (ItemImageLabel itemImageLabel : itemImageMap.values()) {
                remove(itemImageLabel);
            }
            if(consumableOnly)
            	addConsumableItems();
            else {
            	addConsumableItems();
            	addEquipableItems();
            }
        }
        repaint();
        revalidate();
    }
    
    public ItemControllerInterface getItemController() {
    	return itemController;
    }

    /**
     * Hozzáadunk egy új tárgyat a listánkhoz.
     * @param nameOfImage
     */
    public void addImage(Item item) {
    	File file = new File(item.getFileName());
    	if(file.exists()) {
	        Image itemImage = Toolkit.getDefaultToolkit().createImage(item.getFileName());        
	        ImageIcon icon = new ImageIcon(itemImage);
	        itemImageMap.put(idOfImages++, new ItemImageLabel(idOfImages - 1, icon, item));
	        GridBagConstraints gc = new GridBagConstraints();
	        gc.ipadx = 3;
	        if (item.isConsumable()) {
	            gc.gridx = 2;
	            gc.gridy = gc.gridy++;
	        } else {
	            gc.gridx = 1;
	            gc.gridy = gc.gridy++;
	        }
	        add(itemImageMap.get(idOfImages - 1), gc);
    	} else {
    		LoggingFieldLogger.addText("Warning: file was not found for: " + item.getName());
    		
    	}
        repaint();
        revalidate();
    }
    
    public void setItemActionListener(ActionListener listener) {
    	while(equipOrUseButton.getActionListeners().length > 0) {
    		equipOrUseButton.removeActionListener(equipOrUseButton.getActionListeners()[0]);
    	}
    	equipOrUseButton.addActionListener(listener);
    }

    public void removeImage(int id) {
        this.remove(this.itemImageMap.get(id));
    }

    public void setEquipOrUseButtonEnabled(boolean b) {
        this.equipOrUseButton.setEnabled(b);
    }
    
    public void updateModel(Player player) {
    	itemController.setPlayer(player);
    }

	public void dontShowItemButton() {
		this.remove(equipOrUseButton);
	}
}
