package elte.szakdoga.database;

import java.util.LinkedList;
import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import elte.szakdoga.items.models.HPConsumable;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.items.models.defense.Armor;
import elte.szakdoga.items.models.defense.Arms;
import elte.szakdoga.items.models.effects.Effect;
import elte.szakdoga.items.models.offense.OffenseEquipment;
import elte.szakdoga.items.models.offense.OneHandedSword;
import elte.szakdoga.items.models.defense.DefenseEquipment;
import elte.szakdoga.items.models.defense.Foot;
import elte.szakdoga.items.models.defense.Head;

public enum ItemType {
	HEAD {
		private static final String query = 
				"SELECT * FROM items "
				+ "INNER JOIN head "
				+ "ON head.id = items.id "
				+ "WHERE items.id = ?";
		
		@Override
		public Item processItem(JdbcTemplate jdbc, int id) {
			SqlRowSet rowSet = jdbc.queryForRowSet(query, id);
			if(rowSet.first()) { 
				DefenseEquipment armor = new Head(rowSet.getInt("defense"), rowSet.getString("name"), rowSet.getInt("cost"));
				for(Effect effect : getEffects(jdbc, id)) {
					armor.addEffect(effect);
				}			
				return armor;
			}
			return null;				
		}
		
	},
	ARMS {

		private static final String query = 
				"SELECT * FROM items "
				+ "INNER JOIN arms "
				+ "ON arms.id = items.id "
				+ "WHERE items.id = ?";
		
		@Override
		public Item processItem(JdbcTemplate jdbc, int id) {
			SqlRowSet rowSet = jdbc.queryForRowSet(query, id);
			if(rowSet.first()) { 
				DefenseEquipment armor = new Arms(rowSet.getInt("defense"), rowSet.getString("name"), rowSet.getInt("cost"));
				for(Effect effect : getEffects(jdbc, id)) {
					armor.addEffect(effect);
				}
				return armor;
			}
			return null;				
		}
		
	},
	FOOT {

		private static final String query = 
				"SELECT * FROM items "
				+ "INNER JOIN foot "
				+ "ON foot.id = items.id "
				+ "WHERE items.id = ?";
		
		@Override
		public Item processItem(JdbcTemplate jdbc, int id) {
			SqlRowSet rowSet = jdbc.queryForRowSet(query, id);
			if(rowSet.first()) { 
				DefenseEquipment armor = new Foot(rowSet.getInt("defense"), rowSet.getString("name"), rowSet.getInt("cost"));
				for(Effect effect : getEffects(jdbc, id)) {
					armor.addEffect(effect);
				}
				return armor;
			}
			return null;				
		}
		
	},
	
	ARMOR {
		private static final String query = 
				"SELECT * FROM items "
				+ "INNER JOIN armor "
				+ "ON armor.id = items.id "
				+ "WHERE items.id = ?";
		
		@Override
		public Item processItem(JdbcTemplate jdbc, int id) {
			SqlRowSet rowSet = jdbc.queryForRowSet(query, id);
			if(rowSet.first()) { 
				DefenseEquipment armor = new Armor(rowSet.getInt("defense"), rowSet.getString("name"), rowSet.getInt("cost"));
				for(Effect effect : getEffects(jdbc, id)) {
					armor.addEffect(effect);
				}
				return armor;
			}
			return null;				
		}
		
	},
	ONE_HANDED_SWORD {
		private static final String query = 
				"SELECT * FROM items "
				+ "INNER JOIN one_handed_swords "
				+ "ON one_handed_swords.id = items.id "
				+ "WHERE items.id = ?";
		
		@Override
		public Item processItem(JdbcTemplate jdbc, int id) {
			SqlRowSet rowSet = jdbc.queryForRowSet(query, id);
			if(rowSet.first()) { 
				OffenseEquipment oneHandedSword = new OneHandedSword(rowSet.getInt("attackpoints"), rowSet.getString("name"), rowSet.getInt("cost"));
				for(Effect effect : getEffects(jdbc, id)) {
					oneHandedSword.addEffect(effect);
				}
				return oneHandedSword;
			}
			return null;				
		}
	
	}, 
	HP_CONSUMABLE {
		private static final String query = 
				"SELECT * FROM items "
				+ "INNER JOIN hp_consumables "
				+ "ON hp_consumables.id = items.id "
				+ "WHERE items.id = ?";
		
		@Override
		public Item processItem(JdbcTemplate jdbc, int id) {
			SqlRowSet rowSet = jdbc.queryForRowSet(query, id);
			if(rowSet.first()) { 
				HPConsumable hpConsumable = new HPConsumable(rowSet.getInt("value_of_heal"), rowSet.getString("name"), rowSet.getInt("cost"));
				return hpConsumable;
			}
			return null;				
		}
	};
	
	private final static String effectQuery = "SELECT * FROM effects INNER JOIN effects_join ON effects.id = effects_join.effect_id WHERE item_id = ?";
	
	public List<Effect> getEffects(JdbcTemplate jdbc, int id) {
		List<Effect> effects = new LinkedList<>();
		SqlRowSet rowSet = jdbc.queryForRowSet(effectQuery, id);
		if(rowSet.first()) {
			do {
				EffectType effectType = EffectType.valueOf(rowSet.getString("type"));
				effects.add(effectType.processEffect(jdbc, rowSet.getInt("effect_id")));
			} while(rowSet.next());
		}
		return effects;
	}
	public abstract Item processItem(JdbcTemplate jdbc, int id);
}