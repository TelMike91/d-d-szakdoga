package elte.szakdoga.database;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import elte.szakdoga.actors.models.player.Player;

@Component
public class DatabaseWriter {
	@Autowired
	private JdbcTemplate jdbc;
	private static final String WRITE_PLAYER_SQL = "INSERT INTO `player`\r\n" + 
			"(`name`,\r\n" + 
			"`hp`,\r\n" + 
			"`strength`,\r\n" + 
			"`magic`,\r\n" + 
			"`endurance`,\r\n" + 
			"`agility`,\r\n" + 
			"`luck`,\r\n" + 
			"`maxhp`)\r\n" + 
			"VALUES(\r\n" + 
			"?,\r\n" + 
			"?,\r\n" + 
			"?,\r\n" + 
			"?,\r\n" + 
			"?,\r\n" + 
			"?,\r\n" + 
			"?,\r\n" + 
			"?);";
	private static final String UPDATE_PLAYER_SQL = "UPDATE `player`\r\n" + 
			"SET\r\n" + 
			"`name` = ?,\r\n" + 
			"`hp` = ?,\r\n" + 
			"`strength` = ?,\r\n" + 
			"`magic` = ?,\r\n" + 
			"`endurance` = ?,\r\n" + 
			"`agility` = ?,\r\n" + 
			"`luck` = ?,\r\n" + 
			"`maxhp` = ?\r\n" + 
			"WHERE `id` = ?;\r\n";
	public DatabaseWriter(JdbcTemplate jdbc) {
		this.jdbc = jdbc;
	}

	public void writePlayer(Player player) {
		if(DatabaseConnection.getInstance().getPlayerById(player.getId()) == null) {
			jdbc.update(WRITE_PLAYER_SQL, 
					player.getName(), 
					player.getHP(), 
					player.getStrength(), 
					player.getMagic(),
					player.getEndurance(), 
					player.getAgility(), 
					player.getLuck(), 
					player.getMaxHP());
		} else {
			jdbc.update(UPDATE_PLAYER_SQL, 
					player.getName(), 
					player.getHP(), 
					player.getStrength(), 
					player.getMagic(),
					player.getEndurance(), 
					player.getAgility(), 
					player.getLuck(), 
					player.getMaxHP(),
					player.getId());
		}
	}
}
