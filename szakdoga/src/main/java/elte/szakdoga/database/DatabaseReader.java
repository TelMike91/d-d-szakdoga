package elte.szakdoga.database;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Component;

import elte.szakdoga.actors.models.enemy.AggressiveEnemyIntelligence;
import elte.szakdoga.actors.models.enemy.Enemy;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.items.models.Item;

@Component
public class DatabaseReader {
	@Autowired
	private JdbcTemplate jdbc;

	public DatabaseReader(JdbcTemplate jdbc) {
		this.jdbc = jdbc;
	}

	public Item getItemById(int id) {
		SqlRowSet rowSet = jdbc.queryForRowSet("SELECT * FROM items WHERE id = ?", id);
		Item item = null;
		if (rowSet.first()) {
			ItemType type = ItemType.valueOf(rowSet.getString("type"));
			item = type.processItem(jdbc, id);
		}
		return item;

	}

	public Enemy getEnemyById(int id) {
		SqlRowSet rowSet = jdbc.queryForRowSet("SELECT * FROM enemies WHERE id = ?", id);
		Enemy enemy = null;
		if (rowSet.first()) {
			enemy = processEnemy(rowSet);
		}
		return enemy;
	}

	public Player getPlayerById(int id) {
		SqlRowSet rowSet = jdbc.queryForRowSet("SELECT * FROM player WHERE id = ?", id);
		Player player = null;
		if (rowSet.first()) {
			player = processPlayer(rowSet);
		}
		return player;
	}

	private Player processPlayer(SqlRowSet rowSet) {
		Player player;
		player = new Player();
		player.setId(rowSet.getInt("id"));
		player.setMaxHP(rowSet.getInt("maxhp"));
		player.setAgility(rowSet.getInt("agility"));
		player.setStrength(rowSet.getInt("strength"));
		player.setName(rowSet.getString("name"));
		player.setMag(rowSet.getInt("magic"));
		player.setHP(rowSet.getInt("hp"));
		player.setEndurance(rowSet.getInt("endurance"));
		player.setLuck(rowSet.getInt("luck"));
		return player;
	}

	public List<Player> getAllPlayers() {
		SqlRowSet rowSet = jdbc.queryForRowSet("SELECT * FROM player");
		List<Player> players = new LinkedList<>();
		if (rowSet.first()) {
			do {
				players.add(processPlayer(rowSet));
			} while (rowSet.next());
		}
		return players;
	}

	public List<Enemy> getAllEnemies() {
		SqlRowSet rowSet = jdbc.queryForRowSet("SELECT * FROM enemies");
		List<Enemy> enemies = new LinkedList<>();
		if (rowSet.first()) {
			do {
				enemies.add(processEnemy(rowSet));
			} while (rowSet.next());
		}
		return enemies;
	}

	private Enemy processEnemy(SqlRowSet rowSet) {
		Enemy enemy = new Enemy(100, new AggressiveEnemyIntelligence());
		enemy.setMaxHP(rowSet.getInt("maxhp"));
		enemy.setAgility(rowSet.getInt("agility"));
		enemy.setStrength(rowSet.getInt("strength"));
		enemy.setName(rowSet.getString("name"));
		enemy.setMag(rowSet.getInt("magic"));
		enemy.setHP(rowSet.getInt("hp"));
		enemy.setEndurance(rowSet.getInt("endurance"));
		enemy.setLuck(rowSet.getInt("luck"));
		return enemy;
	}
}
