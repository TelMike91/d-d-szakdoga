package elte.szakdoga.database;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import elte.szakdoga.items.models.effects.Effect;
import elte.szakdoga.items.models.effects.IncreaseMaxHPEffect;

public enum EffectType {
	INCREASE_MAX_HP {
		
		private static final String query = "SELECT * FROM increase_max_hp INNER JOIN effects ON effect_id = increase_max_hp.id WHERE effect_id = ?";
		@Override
		public Effect processEffect(JdbcTemplate jdbc, int id) {
			SqlRowSet rowSet = jdbc.queryForRowSet(query, id);
			if(rowSet.first()) {
			    IncreaseMaxHPEffect maxHPEffect = new IncreaseMaxHPEffect(rowSet.getInt("value"));
			    return maxHPEffect;
			}
			return null;
		}
	};	
	public abstract Effect processEffect(JdbcTemplate jdbc, int id);
}
