package elte.szakdoga.database;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

import elte.szakdoga.actors.models.enemy.Enemy;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.items.models.ItemFactory;
import elte.szakdoga.logging.graphics.LoggingFieldLogger;


@Component
public class DatabaseConnection {
	@Autowired
	private JdbcTemplate jdbc;
	@Autowired
	private DriverManagerDataSource dataSource;
	@Autowired
	private DatabaseReader reader;
	@Autowired
	private DatabaseWriter writer;

	private static class DatabaseConnectionSingleton {
		private static DatabaseConnection reader = new DatabaseConnection();
	}

	private DatabaseConnection() {
		dataSource = new DriverManagerDataSource();

        dataSource.setDriverClassName("org.sqlite.JDBC");
        dataSource.setUrl("jdbc:sqlite:dnd.db");	

		jdbc = new JdbcTemplate();
		jdbc.setDataSource(dataSource);
		
		reader = new DatabaseReader(jdbc);
		writer = new DatabaseWriter(jdbc);
	}

	public Item getItemById(int id) {
		try {
			dataSource.getConnection();
			return reader.getItemById(id);
		} catch (SQLException e) {
			LoggingFieldLogger.addText("Connection could not be estabilished with the database");
			return ItemFactory.createDebugHPConsumable();
		}
	}

	public static DatabaseConnection getInstance() {
		return DatabaseConnectionSingleton.reader;
	}

	public void writePlayer(Player createNewPlayer) {
		writer.writePlayer(createNewPlayer);
	}

	public Player getPlayerById(int i) {
		return reader.getPlayerById(i);
	}

	public Enemy getEnemyById(int id) {
		return reader.getEnemyById(id);
	}

	public List<Player> getAllPlayers() {
		return reader.getAllPlayers();
	}

	public List<Enemy> getAllEnemies() {
		return reader.getAllEnemies();
	}
}
