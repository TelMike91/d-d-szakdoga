package elte.szakdoga;

import elte.szakdoga.network.messages.UpdateMessage;

public interface MyObserver {
	public void update();

	public void update(UpdateMessage communication);
}
