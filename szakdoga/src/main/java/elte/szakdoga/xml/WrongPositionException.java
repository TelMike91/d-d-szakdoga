package elte.szakdoga.xml;

@SuppressWarnings("serial")
public class WrongPositionException extends Exception {
	private String cause;
	
	public WrongPositionException(String cause) {
		this.cause = cause;
	}
	
	@Override
	public String getMessage() {
		return cause;
	}
}
