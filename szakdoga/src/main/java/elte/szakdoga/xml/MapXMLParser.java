package elte.szakdoga.xml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.logging.graphics.LoggingFieldLogger;
import elte.szakdoga.map.models.MapBuilder;
import elte.szakdoga.map.models.MyMap;

/**
 * A class that parses a map xml file. The file should be validated with the approrpiate xsd.
 */
public class MapXMLParser {
    private MapBuilder mapBuilder;
    private XMLStreamReader reader;
//    private static DatabaseReader databaseReader;
    private static XMLInputFactory factory = XMLInputFactory.newInstance();
    private static Logger logger = LoggerFactory.getLogger(MapXMLParser.class);
    
    private MapXMLParser(XMLStreamReader reader) {
        this.reader = reader;
        mapBuilder = new MapBuilder();
    }
    
    /**
     * Try and create an XML parser. 
     * @param xmlFileName The filename which we want to open. It should be in main/java/resources.
     * @return A MapXMLParser if the specified file can be opened. Null otherwise
     */
    public static MapXMLParser createMapXMLParser(String xmlFileName) { 	
		logger.info("Getting map");
    	File file = new File("maps/" + xmlFileName);
    	InputStream xmlFile = null;
		try {
			xmlFile = new FileInputStream(file);
		} catch (FileNotFoundException e1) {
			return null;
		}
		
        try {
            XMLStreamReader streamReader = factory.createXMLStreamReader(xmlFile);
    		logger.info("Map acquired");
            return new MapXMLParser(streamReader);
        } catch (XMLStreamException e) {
            logger.error("Couldn't read XML File");
        }        
        return null;
    }

    /**
     * Start the processing of the xml file. By this point we can assume the xml file is open.
     */
    public boolean processXMLFile() {  
    	boolean success = true;
    	logger.info("Processing XML file");
        try {
            while (reader.hasNext()) {
                int eventType = reader.next();
                if(XMLStreamConstants.START_ELEMENT == eventType) {
                    InXMLElements currentElement = InXMLElements.valueOfXmlElement(reader.getName().getLocalPart().toUpperCase());
                    currentElement.doAppropiateThingWithMapBuilder(reader, mapBuilder);
                }
            }
        } catch (XMLStreamException e) {
            logger.error("Couldn't read XML File");
            success = false;
            
        } catch(WrongPositionException e) {
        	logger.error(e.getMessage());
        	LoggingFieldLogger.addText(e.getMessage());
        	success = false;
        } finally {
            try {
                reader.close();
            } catch (XMLStreamException e) {
                logger.error("Couldn't close XML stream");
            }
        }
        logger.info("XML file processed");
        return success;
    }

    /**
     * Returns the map from the xml file, if it was processed. So a processing should be done before this method is called.
     * @return The map according to the xml file.
     */
    public MyMap getMap() {
        return mapBuilder.buildMap();
    }

    public List<Position> getPlayerPositions() {
        return mapBuilder.getPlayerPositions();
    }
}
