package elte.szakdoga.xml;

import java.util.LinkedList;
import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.map.models.MapBuilder;
import elte.szakdoga.map.models.fields.events.Event;

/**
 * Az XML tagek amiket szeretnénk, ha fölismerne a reader. Érdemes a beolvasott
 * adatokat ennek a factory methodjával használni, hogy jó értékeket kapjunk
 */
public enum InXMLElements {
	WIDTH {
		@Override
		public void doAppropiateThingWithMapBuilder(XMLStreamReader reader, MapBuilder mapBuilder)
				throws XMLStreamException, WrongPositionException {
			reader.next();
			mapBuilder.setColumns(Integer.valueOf(reader.getText()));
		}
	},
	HEIGHT {
		@Override
		public void doAppropiateThingWithMapBuilder(XMLStreamReader reader, MapBuilder mapBuilder)
				throws XMLStreamException, WrongPositionException {
			reader.next();
			mapBuilder.setRows(Integer.valueOf(reader.getText()));
		}
	},
	EVENTFIELD {
		@Override
		public void doAppropiateThingWithMapBuilder(XMLStreamReader reader, MapBuilder mapBuilder)
				throws XMLStreamException, WrongPositionException {
			Position position = initPosition(reader);
			checkIfInBounds(mapBuilder, position);
			reader.nextTag();
			List<Event> eventList = new LinkedList<>();
			while (!reader.getLocalName().toUpperCase().equals("EVENTFIELD")) {
				if (reader.isStartElement()) {
					FieldEventType eventType = FieldEventType
							.valueOfFieldEventType(reader.getLocalName().toUpperCase());
					eventType.addEventToEventField(reader, mapBuilder, eventList);
				}
				reader.nextTag();
			}
			mapBuilder.addEventField(position, eventList);
		}
	},
	BLOCKFIELD {
		@Override
		public void doAppropiateThingWithMapBuilder(XMLStreamReader reader, MapBuilder mapBuilder)
				throws XMLStreamException, WrongPositionException {
			Position position = initPosition(reader);
			checkIfInBounds(mapBuilder, position);
			mapBuilder.addBlockField(position);
		}
	},
	ENDGAMEFIELD {

		@Override
		public void doAppropiateThingWithMapBuilder(XMLStreamReader reader, MapBuilder mapBuilder)
				throws XMLStreamException, WrongPositionException {
			Position position = initPosition(reader);
			checkIfInBounds(mapBuilder, position);
			mapBuilder.addEndGameField(position);
		}

	},
	NOTHING {
		@Override
		public void doAppropiateThingWithMapBuilder(XMLStreamReader reader, MapBuilder mapBuilder)
				throws XMLStreamException, WrongPositionException {

		}
	},
	PLAYERSTARTINGPOSITION {
		@Override
		public void doAppropiateThingWithMapBuilder(XMLStreamReader reader, MapBuilder mapBuilder)
				throws XMLStreamException, WrongPositionException {
			Position position = initPosition(reader);
			checkIfInBounds(mapBuilder, position);
			mapBuilder.addPlayerPosition(position);
		}
	},
	SHOPFIELD {
		@Override
		public void doAppropiateThingWithMapBuilder(XMLStreamReader reader, MapBuilder mapBuilder)
				throws XMLStreamException, WrongPositionException {
			Position position = initPosition(reader);
			checkIfInBounds(mapBuilder, position);
			List<Item> items = new LinkedList<>();
			reader.nextTag();
			while (!reader.getLocalName().toUpperCase().equals("SHOPFIELD")) {
				if (reader.isStartElement()) {
					SpecialFieldValues eventType = SpecialFieldValues
							.getValueOf(reader.getLocalName().toUpperCase());
					eventType.doApprorpiateThingWithSpecialFieldValue(reader, items);
				}
				reader.nextTag();
			}
			mapBuilder.addShopField(position, items);			
		}

	};
	
	/**
	 * Inicializálja a mező pozicíóját.
	 * 
	 * @param reader Az xml földolgozó, amivel elérhetjük az XML-ben lévő adatokat.
	 * @return A mező pozíciója.
	 */
	public Position initPosition(XMLStreamReader reader) {
		int x = 0, y = 0;
		for (int i = 0; i < reader.getAttributeCount(); i++) {
			switch (reader.getAttributeLocalName(i)) {
			case "x":
				x = Integer.valueOf(reader.getAttributeValue(i));
				break;
			case "y":
				y = Integer.valueOf(reader.getAttributeValue(i));
				break;
			}
		}
		return new Position(x, y);
	}

	/**
	 * According to the enum type we do certain things with the mapbuilder. Like if
	 * it's a blockfield we add a blockfield to the mapBuilder.
	 * 
	 * @param reader     The xml processor which we use to access the elements of
	 *                   the tag.
	 * @param mapBuilder The mapbuilder which we use to create the map from the xml
	 *                   file
	 * @throws XMLStreamException An exception if an unexpected event happens with
	 *                            the xml processing. Like an unexpected tag, or
	 *                            wrong paramater processing.
	 */
	public abstract void doAppropiateThingWithMapBuilder(XMLStreamReader reader, MapBuilder mapBuilder)
			throws XMLStreamException, WrongPositionException;
	
	private static void checkIfInBounds(MapBuilder mapBuilder, Position position) throws WrongPositionException {
		if(mapBuilder.getRows() <= position.getY() || position.getY() < 0) {
			throw new WrongPositionException("The specified row is out of bounds!");
		}
		if(mapBuilder.getColumns() <= position.getX() || position.getX() < 0) {
			throw new WrongPositionException("The specified column is out of bounds!");
		}
	}

	/**
	 * Our own static factory method to create an InXMLElements. We should add XML
	 * elements to make the XML parser recognize what to do with certain tags.
	 * 
	 * @param upperCase The uppercase String of the tag we want to transform into an
	 *                  InXMLElement
	 * @return An InXMLElement we transformed from the argument. If it returns
	 *         NOTHING it means we read a tag which shouldn't be processed.
	 */
	public static InXMLElements valueOfXmlElement(String upperCase) {
		switch (upperCase) {
		case "WIDTH":
			return WIDTH;
		case "HEIGHT":
			return HEIGHT;
		case "BLOCKFIELD":
			return BLOCKFIELD;
		case "EVENTFIELD":
			return EVENTFIELD;
		case "ENDGAMEFIELD":
			return ENDGAMEFIELD;
		case "PLAYERSTARTINGPOSITION":
			return PLAYERSTARTINGPOSITION;
		case "SHOPFIELD":
			return SHOPFIELD;
		}
		return NOTHING;
	}
}
