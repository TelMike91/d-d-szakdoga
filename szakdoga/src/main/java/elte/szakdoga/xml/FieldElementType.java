package elte.szakdoga.xml;

import java.util.LinkedList;
import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import elte.szakdoga.actors.models.enemy.Enemy;
import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.map.models.MapBuilder;
import elte.szakdoga.map.models.fields.events.BattleEvent;
import elte.szakdoga.map.models.fields.events.Event;
import elte.szakdoga.map.models.fields.events.ItemEvent;
import elte.szakdoga.map.models.fields.events.TrapEvent;

enum FieldEventType {
	TRAPEVENT {
		@Override
		public void addEventToEventField(XMLStreamReader reader, MapBuilder builder, List<Event> eventList) {
			eventList.add(new TrapEvent(Integer.valueOf(reader.getAttributeValue(0))));
		}
	},
	ITEMEVENT {
		@Override
		public void addEventToEventField(XMLStreamReader reader, MapBuilder builder, List<Event> eventList)
				throws XMLStreamException {
			ItemEvent event = new ItemEvent();
			List<Item> items = new LinkedList<>();
			reader.nextTag();
			while (!reader.getLocalName().equals("itemEvent")) {
				if (reader.isStartElement()) {		
					SpecialFieldValues fieldValues = SpecialFieldValues.getValueOf(reader.getLocalName().toUpperCase());
					fieldValues.doApprorpiateThingWithSpecialFieldValue(reader, items);
				}
				reader.nextTag();
			}
			for(Item item : items)
				event.addItem(item);
			eventList.add(event);
		}

	},
	BATTLEEVENT {

		@Override
		public void addEventToEventField(XMLStreamReader reader, MapBuilder builder, List<Event> eventList)
				throws XMLStreamException {
			List<Enemy> enemyList = new LinkedList<>();
			reader.nextTag();
			while (!reader.getLocalName().equals("battleEvent")) {
				if (reader.isStartElement()) {
					SpecialFieldValues fieldValues = SpecialFieldValues.getValueOf(reader.getLocalName().toUpperCase());
					fieldValues.doApprorpiateThingWithSpecialFieldValue(reader, enemyList);
				}
				reader.nextTag();
			}
			eventList.add(new BattleEvent(new BattleField(enemyList)));
		}

	},
	NOTHING {

		@Override
		public void addEventToEventField(XMLStreamReader reader, MapBuilder builder, List<Event> eventList)
				throws XMLStreamException {

		}

	};

	public abstract void addEventToEventField(XMLStreamReader reader, MapBuilder builder, List<Event> eventList)
			throws XMLStreamException;

	public static FieldEventType valueOfFieldEventType(String localName) {
		switch (localName) {
		case "TRAPEVENT":
			return TRAPEVENT;
		case "ITEMEVENT":
			return ITEMEVENT;
		case "BATTLEEVENT":
			return BATTLEEVENT;
		default:
			return NOTHING;
		}
	}
}
