package elte.szakdoga.xml;

import java.util.List;

import javax.xml.stream.XMLStreamReader;

import elte.szakdoga.actors.models.enemy.Enemy;
import elte.szakdoga.actors.models.enemy.EnemyFactory;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.items.models.ItemFactory;

public enum SpecialFieldValues {	
	ITEM {
		@Override
		public void doApprorpiateThingWithSpecialFieldValue(XMLStreamReader reader, List<?> event) {			
			int id = Integer.parseInt(reader.getAttributeValue(0));
			helper(event, ItemFactory.getItemById(id));
		}

		@SuppressWarnings("unchecked")
		private <T> void helper(List<T> items, Item itemById) {
			doHelper((List<Item>) items, itemById);
		}
		
		private void doHelper(List<Item> items, Item itemById) {
			items.add(itemById);
		}
				
		
	}, ENEMY {
		@Override
		public void doApprorpiateThingWithSpecialFieldValue(XMLStreamReader reader, List<?> listOfSpecialField) {
			int id = Integer.parseInt(reader.getAttributeValue(0));
			helper(listOfSpecialField, EnemyFactory.getEnemyById(id));
		}

		@SuppressWarnings("unchecked")
		private <T> void helper(List<T> listOfSpecialField, Enemy enemy) {
			doHelper((List<Enemy>) listOfSpecialField, enemy);			
		}
		
		private void doHelper(List<Enemy> enemies, Enemy enemy) {
			enemies.add(enemy);
		}
	}, NOTHING {
		@Override
		public void doApprorpiateThingWithSpecialFieldValue(XMLStreamReader reader, List<?> items) {
			// TODO Auto-generated method stub
			
		}
	};

	public abstract void doApprorpiateThingWithSpecialFieldValue(XMLStreamReader reader, List<?> items);			

	public static SpecialFieldValues getValueOf(String upperCaseString) {
		switch(upperCaseString) {
		case "ITEM":
			return ITEM;
		case "ENEMY":
			return ENEMY;
		default:
			return NOTHING;
		}
	}
}
