package elte.szakdoga.actors.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.esotericsoftware.kryonet.rmi.ObjectSpace;

import elte.szakdoga.actors.graphics.PlayerPanel;
import elte.szakdoga.actors.graphics.PlayerPanelWithChat;
import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.actors.models.Position;
import elte.szakdoga.actors.models.Position.Direction;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.items.controller.ItemControllerWithNetwork;
import elte.szakdoga.items.controller.ItemService;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.messages.models.MyMessage;
import elte.szakdoga.network.MyClient;
import elte.szakdoga.network.rmi.ServerDataInterface;

public class MultiPlayerController implements PlayerControllerInterface {
	private PlayerPanel playerPanel;	
	private Player player;
	private MyClient client;
	private ItemService itemService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MultiPlayerController.class);

    public MultiPlayerController(MyClient client) {
    	this.client = client;
    }
    
    /* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#movePlayer(elte.szakdoga.actors.models.Position.Direction)
	 */
    @Override
	public void movePlayer(Direction direction) {  
    	try {
    		ServerDataInterface serverData = ObjectSpace.getRemoteObject(client.getConnection(), 1, ServerDataInterface.class);		
			serverData.move(getCurrentPlayer().getId(), direction);
			setPlayer(serverData.getPlayer(player));
    	} catch(Exception exception) {
    		LOGGER.debug("movePlayer threw exception with parameter: {} StackTrace", direction, exception);
    	}
    }
    
    /* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#setPlayer(elte.szakdoga.actors.models.player.Player)
	 */
    @Override
	public void setPlayer(Player player) {
    	this.player = player;
    	if(playerPanel == null)
    		playerPanel = new PlayerPanelWithChat(new ItemControllerWithNetwork(client, player), client);
    	else
    		playerPanel.updateModel(player);
    }          

    /* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#getCurrentPlayer()
	 */
    @Override
	public Player getCurrentPlayer() {
    	return player;
    }

    /* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#addItem(elte.szakdoga.items.models.Item)
	 */
    @Override
	public void addItem(Item item) {
        getCurrentPlayer().addItem(item);
    }

    /* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#useItem(elte.szakdoga.items.models.Item)
	 */
    @Override
	public void useItem(Item item) {
    	itemService.useItem(item, player);        
    }
    
    /* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#useItem(elte.szakdoga.items.models.Item, elte.szakdoga.actors.models.Actor)
	 */
    @Override
	public void useItem(Item item, Actor onActor) {
        itemService.useItemOnOther(item, player, onActor);
    }

	/* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#gotMessages()
	 */
	@Override
	public boolean gotMessages() {
		if(getCurrentPlayer() != null)
			return getCurrentPlayer().gotMessages();
		else
			return false;
	}

	/* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#getMessages()
	 */
	@Override
	public List<MyMessage> getMessages() {
		return getCurrentPlayer().getMessages();
	}

	/* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#getPlayerView()
	 */
	@Override
	public PlayerPanel getPlayerView() {
		return playerPanel;
	}

	@Override
	public Player getPlayerInShop() {
		return getCurrentPlayer().getShop() == null ? null : getCurrentPlayer();
	}

	@Override
	public void playerLeavesShop() {
		ServerDataInterface serverData = ObjectSpace.getRemoteObject(client.getConnection(), 1, ServerDataInterface.class);
		serverData.playerLeavesShop(getCurrentPlayer().getId());
		setPlayer(serverData.getPlayer(player));
	}

	@Override
	public boolean buyItem(Item item) {
		ServerDataInterface serverData = ObjectSpace.getRemoteObject(client.getConnection(), 1, ServerDataInterface.class);
		boolean success = serverData.buyItem(getCurrentPlayer().getId(), item);
		setPlayer(serverData.getPlayer(player));
		return success;
	}

	@Override
	public void damage(int damage) {
		player.damage(damage);
	}

	@Override
	public Position getCurrentPlayerPosition() {
		return player.getPosition();
	}

	@Override
	public void setPosition(Position position) {
		player.setPosition(position);
	}

}
