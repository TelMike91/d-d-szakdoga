package elte.szakdoga.actors.controllers;

import java.util.List;

import elte.szakdoga.actors.graphics.PlayerPanel;
import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.actors.models.Position;
import elte.szakdoga.actors.models.Position.Direction;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.messages.models.MyMessage;

public interface PlayerControllerInterface {

	/**
	 * Mozgatjuk a jelenlegi játékost a megfelelő pozícióba
	 * @param direction
	 */
	void movePlayer(Direction direction);

	/**
	 * Hozzáadjuk a játékost a PlayerControllerhez, és létrehozzuk a hozzá kapcsolodó grafikát. 
	 * @param player A játékos amit szeretnénk mostantól felügyelni.
	 */
	void setPlayer(Player player);

	Position getCurrentPlayerPosition();

	/**
	 * Visszaadjuk a jelenlegi játékost. Ha nincsen játékost akkor null-t ad vissza.
	 * @return A játékos akit irányítunk. Csak akkor null ha nincs egy játékos se.
	 */
	Player getCurrentPlayer();

	void addItem(Item item);

	void useItem(Item item);

	void useItem(Item item, Actor onActor);

	boolean gotMessages();

	List<MyMessage> getMessages();

	PlayerPanel getPlayerView();

	Player getPlayerInShop();

	void playerLeavesShop();

	boolean buyItem(Item item);

	void damage(int damage);

	void setPosition(Position position);
}