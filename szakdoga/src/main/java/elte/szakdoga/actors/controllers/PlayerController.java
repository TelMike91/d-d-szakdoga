package elte.szakdoga.actors.controllers;

import java.util.List;

import elte.szakdoga.actors.graphics.PlayerPanel;
import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.actors.models.Position;
import elte.szakdoga.actors.models.Position.Direction;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.items.controller.ItemController;
import elte.szakdoga.items.controller.ItemService;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.messages.models.MyMessage;

/**
 * A játékos kontroller, amivel szemmel tartható a játékosoknak a pozíciói.
 * @author Teleki Miklós
 */
public class PlayerController implements PlayerControllerInterface {
	private ItemService itemService;
	private PlayerPanel playerPanel;	
	private Player player;
    
    public PlayerController() { 
    	itemService = new ItemService();
    }
    
    /* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#movePlayer(elte.szakdoga.actors.models.Position.Direction)
	 */
    @Override
	public void movePlayer(Direction direction) {   
    	setPosition(direction.move(player.getPosition()));	
    }
    
    /* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#setPlayer(elte.szakdoga.actors.models.player.Player)
	 */
    @Override
	public void setPlayer(Player player) {
    	this.player = player;
    	if(playerPanel == null)
    		playerPanel = new PlayerPanel(new ItemController(player));
    	else
    		playerPanel.updateModel(player);
    }          

    /* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#getCurrentPlayerPosition()
	 */
    @Override
	public Position getCurrentPlayerPosition() {
    	return player.getPosition();      
    }

    /* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#getCurrentPlayer()
	 */
    @Override
	public Player getCurrentPlayer() {
    	return player;
    }

    /* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#addItem(elte.szakdoga.items.models.Item)
	 */
    @Override
	public void addItem(Item item) {
        getCurrentPlayer().addItem(item);
    }

    /* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#useItem(elte.szakdoga.items.models.Item)
	 */
    @Override
	public void useItem(Item item) {
    	itemService.useItem(item, player);
    }
    
    /* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#useItem(elte.szakdoga.items.models.Item, elte.szakdoga.actors.models.Actor)
	 */
    @Override
	public void useItem(Item item, Actor onActor) {
    	itemService.useItemOnOther(item, player, onActor);
    }

	/* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#gotMessages()
	 */
	@Override
	public boolean gotMessages() {
		if(getCurrentPlayer() != null)
			return getCurrentPlayer().gotMessages();
		else
			return false;
	}

	/* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#getMessages()
	 */
	@Override
	public List<MyMessage> getMessages() {
		return getCurrentPlayer().getMessages();
	}

	/* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#getPlayerView()
	 */
	@Override
	public PlayerPanel getPlayerView() {
		return playerPanel;
	}

	@Override
	public Player getPlayerInShop() {		
		return getCurrentPlayer().getShop() == null ? null : getCurrentPlayer();
	}

	@Override
	public void playerLeavesShop() {
		getCurrentPlayer().getShop().removePlayer(getCurrentPlayer());
		getCurrentPlayer().setShop(null);
	}

	@Override
	public boolean buyItem(Item item) {
		return getCurrentPlayer().buy(item);
	}

	@Override
	public void damage(int damage) {
		player.damage(damage);
	}

	@Override
	public void setPosition(Position position) {
		player.setPosition(position);
	}
}
