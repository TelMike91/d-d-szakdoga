package elte.szakdoga.actors.controllers;

import java.util.List;

import com.esotericsoftware.kryonet.rmi.ObjectSpace;

import elte.szakdoga.actors.graphics.PlayerPanel;
import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.actors.models.Position;
import elte.szakdoga.actors.models.Position.Direction;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.messages.models.MyMessage;
import elte.szakdoga.network.MyClient;
import elte.szakdoga.network.rmi.ServerDataInterface;

public class NarratorPlayerController implements PlayerControllerInterface {	
	private Player player;
	private MyClient client;
	private Position position;
	
    public NarratorPlayerController(MyClient client) {
    	this.client = client;
    }
    
    /* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#movePlayer(elte.szakdoga.actors.models.Position.Direction)
	 */
    @Override
	public void movePlayer(Direction direction) {   
		ServerDataInterface serverData = ObjectSpace.getRemoteObject(client.getConnection(), 1, ServerDataInterface.class);
		serverData.moveNarrator(getCurrentPlayer().getId(), direction);
//		 TODO emiatt nem megy a narrator
		setPlayer(serverData.getPlayer(player));
    }
    
    /* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#setPlayer(elte.szakdoga.actors.models.player.Player)
	 */
    @Override
	public void setPlayer(Player player) {
    	this.player = player;
    }          

    /* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#getCurrentPlayerPosition()
	 */
    @Override
	public Position getCurrentPlayerPosition() {
    	return position;      
    }

    /* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#getCurrentPlayer()
	 */
    @Override
	public Player getCurrentPlayer() {
    	return player;
    }

    /* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#addItem(elte.szakdoga.items.models.Item)
	 */
    @Override
	public void addItem(Item item) {
        getCurrentPlayer().addItem(item);
    }

    /* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#useItem(elte.szakdoga.items.models.Item)
	 */
    @Override
	public void useItem(Item item) {
        
    }
    
    /* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#useItem(elte.szakdoga.items.models.Item, elte.szakdoga.actors.models.Actor)
	 */
    @Override
	public void useItem(Item item, Actor onActor) {

    }

	/* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#gotMessages()
	 */
	@Override
	public boolean gotMessages() {
		if(getCurrentPlayer() != null)
			return getCurrentPlayer().gotMessages();
		else
			return false;
	}

	/* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#getMessages()
	 */
	@Override
	public List<MyMessage> getMessages() {
		return getCurrentPlayer().getMessages();
	}

	/* (non-Javadoc)
	 * @see elte.szakdoga.actors.controllers.PlayerControllerInterface#getPlayerView()
	 */
	@Override
	public PlayerPanel getPlayerView() {
		return null;
	}

	@Override
	public Player getPlayerInShop() {
		return getCurrentPlayer().getShop() == null ? null : getCurrentPlayer();
	}

	@Override
	public void playerLeavesShop() {
		ServerDataInterface serverData = ObjectSpace.getRemoteObject(client.getConnection(), 1, ServerDataInterface.class);
		serverData.playerLeavesShop(getCurrentPlayer().getId());
		setPlayer(serverData.getPlayer(player));
	}

	@Override
	public boolean buyItem(Item item) {
		return false;
	}

	@Override
	public void damage(int damage) {
		player.damage(damage);
	}

	@Override
	public void setPosition(Position position) {
		this.position = position;
	}
}
