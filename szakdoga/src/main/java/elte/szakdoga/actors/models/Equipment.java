package elte.szakdoga.actors.models;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import elte.szakdoga.items.models.Item;
import elte.szakdoga.items.models.defense.Armor;
import elte.szakdoga.items.models.defense.Arms;
import elte.szakdoga.items.models.defense.Foot;
import elte.szakdoga.items.models.defense.Head;
import elte.szakdoga.items.models.defense.nullobjects.ArmorNullObject;
import elte.szakdoga.items.models.defense.nullobjects.ArmsNullObject;
import elte.szakdoga.items.models.defense.nullobjects.FootNullObject;
import elte.szakdoga.items.models.defense.nullobjects.HeadNullObject;
import elte.szakdoga.items.models.offense.Fist;
import elte.szakdoga.items.models.offense.OffenseEquipment;

/**
 * Az karakter felszerelését reprezentálja.
 * @author Teleki Miklós
 *
 */
public class Equipment {
    private List<Item> items;
    private Head headGear;
    private Arms armGear;
    private Armor chestGear;
    private Foot legsGear;
    private OffenseEquipment offenseEquipment;    
    
    public Equipment() {
        offenseEquipment = new Fist();        
        items = new LinkedList<>();
        headGear = new HeadNullObject();
        armGear = new ArmsNullObject();
        chestGear = new ArmorNullObject();
        legsGear = new FootNullObject();
    }
    
    /**
     * Hozzáad egy itemet a tárgyak listájához.
     * @param item Tárgy amit szeretnénk ha hozzáadna.
     */
    public void addItem(Item item) {
        items.add(item);        
    }  
    
    /**
     * Setter ami beállítja az offenseEquipmentet
     * @param piece Az OffenseEquipment amit szeretnénk, ha fölvenne a játékos.
     */
    public void equipOffenseEquipment(OffenseEquipment piece) {
        offenseEquipment = piece;
    }
           
    public void equipHeadGear(Head gear) {
        this.headGear = gear;
    }
    
    public void equipArmorGear(Armor armor) {
        this.chestGear = armor;
    }
    
    public void equipLegGear(Foot armor) {
        this.legsGear = armor;
    }
    
    public void equipArmsGear(Arms arms) {
        this.armGear = arms;
    }
    
    public void dequipHeadGear() {
        this.headGear = new HeadNullObject();
    }
    public void dequipChestGear() {
        this.chestGear = new ArmorNullObject();
    }
    public void dequipLegGear() {
        this.legsGear = new FootNullObject();
    }
    public void dequipArmsGear() {
        this.armGear = new ArmsNullObject();
    }
    
    public int getOffensePoint() {
        return offenseEquipment.getAttackPoint();
    }    
    
    public int getArmorPoint() {
        return armGear.getArmorPoint() + legsGear.getArmorPoint() + chestGear.getArmorPoint() + headGear.getArmorPoint();
    }

    public void effectsOn(Actor owner) {
        armGear.effectOn(owner);
        headGear.effectOn(owner);
        chestGear.effectOn(owner);
        legsGear.effectOn(owner);
        offenseEquipment.effectOn(owner);        
    }

    /**
     * Kikérjük a tárgyat amit szeretnénk használni.
     * @param item Tárgy amit szeretnénk lekérni
     * @return A tárgy vagy null ha nincs ilyen.
     */
    public Optional<Item> getItem(Item item) {
        int index = items.indexOf(item);
        if(index >= 0) {
            return Optional.of(items.get(index));
        }
        return Optional.empty();
    }

    /**
     * Eltávolít egy tárgyat a listából
     * @param item Tárgy, amit szeretnénk eltávolítani
     */
    public void removeItem(Item item) {
        items.remove(item);
    }

    /**
     * Visszaadja a használható tárgyak listáját.
     * @return A tárgyak listája
     */
    public List<Item> getItemsList() {
        return Collections.unmodifiableList(items);
    }
}
