package elte.szakdoga.actors.models;

/**
 * A bonúsz attribútumok, amik a tárgyakból jöhet.
 */
public class Bonus {
    private int strength, magic, endurance, agility, luck;
    private int maxHP;

    public int getStrength() {
        return strength;
    }

    public int getMagic() {
        return magic;
    }

    public int getEndurance() {
        return endurance;
    }

    public int getAgility() {
        return agility;
    }

    public int getLuck() {
        return luck;
    }

    public int getMaxHP() {
        return maxHP;
    }


    public void addStrength(int strength) {
        this.strength += strength;
    }

    public void addMagic(int magic) {
        this.magic += magic;
    }

    public void addAgility(int agility) {
        this.agility += agility;
    }

    public void addEndurance(int endurance) {
        this.endurance += endurance;
    }

    public void addLuck(int luck) {
        this.luck += luck;
    }

    public void addMaxHP(int maxHP) {
        this.maxHP += maxHP;
    }

}
