package elte.szakdoga.actors.models.enemy;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.battle.controller.BattleControllerInterface;

/**
 * Az ellenfél modellje.
 * @author Teleki Miklós
 *
 */
public class Enemy extends Actor {
	private EnemyIntelligence intelligence;
	
	public Enemy() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Az ellenfél konstruktora, amiben a megadott paraméterek szerint fog viselkedni.
	 * @param goldReward Az arany nyeremény, amit a játékosok kapnak ha legyőzik ezt az ellenfelet.
	 * @param intelligence Az ellenfél intelligencáiáját meghatározó enum, amiből megkapjuk milyen okos legyen az ellenfél.
	 */
	public Enemy(int goldReward, EnemyIntelligence intelligence) {
		setGold(goldReward);
		this.intelligence = intelligence;
	}

	public Enemy(Enemy enemyAt) {
		this.intelligence = enemyAt.intelligence;
		this.setAgility(enemyAt.getAgility());
		this.setEndurance(enemyAt.getEndurance());
		this.setHP(enemyAt.getHP());
		this.setMaxHP(enemyAt.getMaxHP());
		this.setLuck(enemyAt.getLuck());
		this.setStrength(enemyAt.getStrength());
		this.setGold(enemyAt.getGold());
		this.setMag(enemyAt.getMagic());
		this.setName(enemyAt.getName());
	}

	@Override
	public void takeAction(BattleControllerInterface battleField) {
		intelligence.takeAction(battleField.getBattle()).action(battleField);
	}

	/**
	 * Létrehoz egy dummy aggresszív ellenfelet.
	 * @return Egy alap ellenfél.
	 */
	public static Enemy createDefaultEnemy() {
		Enemy enemy = new Enemy(100, new AggressiveEnemyIntelligence());
		enemy.setName("TestEnemy");
		enemy.setStrength(100);
		enemy.setMag(100);
		enemy.setEndurance(100);
		enemy.setAgility(100);
		enemy.setLuck(100);
		enemy.setHP(100);
		enemy.setMaxHP(200);
		enemy.setEquipment(enemy);
		return enemy;
	}
}
