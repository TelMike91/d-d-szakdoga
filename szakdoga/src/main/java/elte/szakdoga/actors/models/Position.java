package elte.szakdoga.actors.models;

/**
 * Ez az osztály a modelje a térképen való pozícióknak
 * @author Miki
 *
 */
public class Position {
    private final int x;
    private final int y;
    
    public static enum Direction {
        UP (0, -1),
        DOWN (0, 1),  
        LEFT (-1, 0), 
        RIGHT (1, 0);
        
        private int y;
        private int x;

        private Direction(int x, int y) {
            this.x = x;
            this.y = y;
        }
        
        public Position move(Position from) {
            return new Position(from.getX() + this.x, from.getY() + this.y);
        }
    }
    
    public Position() { x = 0; y = 0; }
    
    /**
     * Létrehozunk egy új pozíciót.
     * @param x az oszlop paraméter.
     * @param y a sor paraméter.
     */
    public Position(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Lekérjük a sor reprezentációját.
     * @return A sor száma.
     */
    public int getY() {
        return y;
    }

    /**
     * Lekérjük az X-et ami az oszlop reprezentációja
     * @return Az oszlop száma.
     */
    public int getX() {
        return x;
    }
    
    @Override
    public String toString() {
        return "X: " + x + " Y: " +  y;
    }
    
    @Override
    public int hashCode() {
        return Integer.hashCode(x) + Integer.hashCode(y);
    }
    
    @Override
    public boolean equals(Object other) {
        if(this == other) 
            return true;
        if(other instanceof Position) {
            Position otherPosition = (Position) other;
            return x == otherPosition.getX() && y == otherPosition.getY();
        } else 
            return false;            
    }
}
