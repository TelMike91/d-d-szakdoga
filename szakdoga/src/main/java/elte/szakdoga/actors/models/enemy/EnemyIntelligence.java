package elte.szakdoga.actors.models.enemy;

import elte.szakdoga.actors.models.enemy.battleactions.EnemyAction;
import elte.szakdoga.battle.models.BattleField;

public interface EnemyIntelligence {

	public EnemyAction takeAction(BattleField battleField);
}
