package elte.szakdoga.actors.models.enemy.battleactions;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.battle.controller.BattleControllerInterface;
import elte.szakdoga.battle.models.BattleField;

public class AttackAction implements EnemyAction {
	private Actor target;

	public AttackAction(BattleField battleField) {
		int chosenTarget = (int) (Math.random() * battleField.getNumberOfFriends());
		target = battleField.getPlayer(chosenTarget);
	}

	@Override
	public void action(BattleControllerInterface battleController) {
		battleController.attack(target);
	}

}
