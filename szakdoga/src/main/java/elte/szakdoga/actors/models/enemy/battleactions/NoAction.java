package elte.szakdoga.actors.models.enemy.battleactions;

import elte.szakdoga.battle.controller.BattleControllerInterface;

public class NoAction implements EnemyAction {
	public NoAction() { }
	
	@Override
	public void action(BattleControllerInterface battleController) {
		battleController.nextActor();
	}

}
