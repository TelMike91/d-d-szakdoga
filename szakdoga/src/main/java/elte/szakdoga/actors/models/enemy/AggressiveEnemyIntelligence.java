package elte.szakdoga.actors.models.enemy;

import elte.szakdoga.actors.models.enemy.battleactions.AttackAction;
import elte.szakdoga.actors.models.enemy.battleactions.EnemyAction;
import elte.szakdoga.actors.models.enemy.battleactions.NoAction;
import elte.szakdoga.battle.models.BattleField;

public class AggressiveEnemyIntelligence implements EnemyIntelligence {
	private final static int AGGRESSIVENESS = 100;

	@Override
	public EnemyAction takeAction(BattleField battleField) {
		if (battleField.getCurrentActor().getEquipment().getItemsList().size() > 0) {
			for (Enemy enemy : battleField.getEnemies()) {
				if (enemy.getHP() < enemy.getMaxHP() / 2) {
//					return new UseHealingItemAction(battleField, enemy);
				}
			}
		}
		int random = (int) Math.floor(Math.random() * 100);
		if (random < AGGRESSIVENESS) {
			return new AttackAction(battleField);
		}
		return new NoAction();
	}
}
