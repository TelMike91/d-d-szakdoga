package elte.szakdoga.actors.models.enemy.battleactions;

import elte.szakdoga.battle.controller.BattleControllerInterface;

public interface EnemyAction {
	/**
	 * Az osztályok amik ezt az interfész megvalósítják, leírják hogy milyen cselekvést hajtson végre a paraméterben
	 * megadott ellenfél.
	 * @param enemy Az ellenfél, akivel cselekvést hajtunk végre.
	 */
	public void action(BattleControllerInterface battleController);
}
