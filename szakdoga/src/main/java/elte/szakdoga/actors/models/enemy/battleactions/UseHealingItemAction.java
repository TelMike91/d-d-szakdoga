package elte.szakdoga.actors.models.enemy.battleactions;

import java.util.List;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.actors.models.enemy.Enemy;
import elte.szakdoga.battle.controller.BattleControllerInterface;
import elte.szakdoga.items.models.HPConsumable;
import elte.szakdoga.items.models.Item;

public class UseHealingItemAction implements EnemyAction {
	private Actor usedOn;
	private Actor user;

	public UseHealingItemAction(Enemy user, Enemy enemy) {
		this.user = enemy;
		usedOn = enemy;
	}

	@Override
	public void action(BattleControllerInterface battleController) {
		List<Item> items = user.getEquipment().getItemsList();
		for(Item item : items) {
			if(item.isConsumable() && item instanceof HPConsumable) {
				battleController.useItem(user, usedOn, item);
				return;
			}
		}
	}

}
