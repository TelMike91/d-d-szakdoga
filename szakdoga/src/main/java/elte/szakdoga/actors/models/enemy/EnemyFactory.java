package elte.szakdoga.actors.models.enemy;

import elte.szakdoga.database.DatabaseConnection;

public class EnemyFactory {

	public static Enemy getEnemyById(int id) {
		DatabaseConnection connection = DatabaseConnection.getInstance();
		return connection.getEnemyById(id);
	}

}
