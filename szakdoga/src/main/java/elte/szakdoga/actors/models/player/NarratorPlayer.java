package elte.szakdoga.actors.models.player;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.map.models.fields.ShopField;

public class NarratorPlayer extends Player {
	
	public NarratorPlayer() {

	}

	public NarratorPlayer(Position position) {
		super(position);
	}

	public void setBattle(BattleField battleField) {
		
	}
	
	public void setShop(ShopField shopField) {
	
	}
	
	@Override
	public boolean addableToBattle() {
		return false;
	}

	public static NarratorPlayer createNarratorPlayer(Position position) {
		NarratorPlayer player = new NarratorPlayer(position);
		player.setName("Narrator");
		player.setStrength(100);
		player.setMag(100);
		player.setEndurance(100);
		player.setAgility(100);
		player.setLuck(100);
		player.setHP(100);
		player.setMaxHP(200);
		player.setEquipment(player);
		return player;
	}
	
}
