package elte.szakdoga.actors.models;

import java.util.Optional;

import elte.szakdoga.battle.controller.BattleControllerInterface;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.items.models.defense.Armor;
import elte.szakdoga.items.models.defense.Arms;
import elte.szakdoga.items.models.defense.Foot;
import elte.szakdoga.items.models.defense.Head;
import elte.szakdoga.items.models.offense.OffenseEquipment;

/**
 * Egy olyan absztrakt osztály, amiben megadjuk hogy minden játékos és ellenfél
 * ezekkel a statisztikákkal rendelkeznek.
 */
public abstract class Actor {
	private String name;
	private int strength, magic, endurance, agility, luck;
	private int maxHP;
	private int HP;
	private Bonus bonuses = new Bonus();
	private Equipment equipment;
	private int gold;
	
	public Actor() {
		equipment = new Equipment();
	}
	
	public String getFileName() {
		return "assets/images/character.png";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Actor other = (Actor) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public void addItem(Item item) {
		equipment.addItem(item);
	}

	protected void setEquipment(Actor owner) {
		this.equipment = new Equipment();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStrength() {
		return strength + bonuses.getStrength();
	}

	/**
	 * Increases the bonus of the player's max HP.
	 * 
	 * @param maxHP The amount increased.
	 */
	public void addBonusMaxHP(int maxHP) {
		bonuses.addMaxHP(maxHP);
	}

	/**
	 * Increases the bonus of the player's luck.
	 * 
	 * @param maxHP The amount increased.
	 */
	public void addBonusLuck(int luck) {
		bonuses.addLuck(luck);
	}


	public Equipment getEquipment() {
		return equipment;
	}

	/**
	 * Increases the bonus of the player's Agility.
	 * 
	 * @param agility The amount increased.
	 */
	public void addBonusAgility(int agility) {
		bonuses.addAgility(agility);
	}

	/**
	 * Megnöveli a bónusz kitartást
	 * 
	 * @param endurance A bónusz kitartás, amit szeretnénk hozzáadni.
	 */
	public void addBonusEndurance(int endurance) {
		bonuses.addEndurance(endurance);
	}

	/**
	 * Increases the bonus of the player's magic.
	 * 
	 * @param maxHP The amount increased.
	 */
	public void addBonusMagic(int magic) {
		bonuses.addMagic(magic);
	}

	/**
	 * Increases the bonus of the player's strength.
	 * 
	 * @param strength The amount of strength increased.
	 */
	public void addBonusStrength(int strength) {
		bonuses.addStrength(strength);
	}

	public void setStrength(int str) {
		this.strength = str;
	}

	public int getMagic() {
		return magic + bonuses.getMagic();
	}

	public void setMag(int mag) {
		this.magic = mag;
	}

	public int getEndurance() {
		return endurance + bonuses.getEndurance();
	}

	public void setEndurance(int end) {
		this.endurance = end;
	}

	public int getAgility() {
		return agility + bonuses.getAgility();
	}

	public void setAgility(int ag) {
		this.agility = ag;
	}

	public int getLuck() {
		return luck + bonuses.getLuck();
	}

	public void setLuck(int lck) {
		this.luck = lck;
	}

	public int getHP() {
		return HP;
	}

	public void setHP(int HP) {
		this.HP = HP;
	}

	public int getMaxHP() {
		return maxHP + bonuses.getMaxHP();
	}

	public void setMaxHP(int maxHP) {
		this.maxHP = maxHP;
	}

	public int getDefensePoint() {
		return equipment.getArmorPoint();
	}

	/**
	 * Equips the weapon in the right hand. Also adds the bonuses this weapon
	 * offers.
	 * 
	 * @param weapon The weapon to be equipped
	 */
	public void equipRightHand(OffenseEquipment weapon) {
		equipment.equipOffenseEquipment(weapon);
		effect();
	}

	/**
	 * Applies the effects from the weapons and equipment to the player.
	 */
	public void effect() {
		bonuses = new Bonus();
		equipment.effectsOn(this);
	}

	public int getAttackPoint() {
		return equipment.getOffensePoint();
	}

	public void damage(int damagePoints) {		
		this.HP -= damagePoints;
	}

	public void equipHeadGear(Head head) {
		equipment.equipHeadGear(head);
		effect();
	}

	public void equipArmorGear(Armor armor) {
		equipment.equipArmorGear(armor);
		effect();
	}

	public void equipLegGear(Foot legs) {
		equipment.equipLegGear(legs);
		effect();
	}

	public void equipArmsGear(Arms arms) {
		equipment.equipArmsGear(arms);
		effect();
	}

	public void dequipHeadGear() {
		equipment.dequipHeadGear();
		effect();
	}

	public void dequipChestGear() {
		equipment.dequipChestGear();
		effect();
	}

	public void dequipLegGear() {
		equipment.dequipLegGear();
		effect();
	}

	public void dequipArmsGear() {
		equipment.dequipArmsGear();
		effect();
	}

	/**
	 * A metódus ami akor fut le amikor a szereőlőnek csleekednie kell.
	 * @param battleField A harcmező amire értelmezzük ezt a cselekményt.
	 */
	public abstract void takeAction(BattleControllerInterface battleField);

	public int getGold() {
		return gold;
	}

	public void setGold(int gold) {
		this.gold = gold;
	}

	public void addGold(int goldReward) {
		gold += goldReward;
	}
	
	public Optional<Item> getItem(Item currentItem) {
		return this.equipment.getItem(currentItem);
	}

	public void removeItem(Item item) {
		this.equipment.removeItem(item);
	}
}
