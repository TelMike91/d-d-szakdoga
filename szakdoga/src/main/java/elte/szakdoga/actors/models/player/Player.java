package elte.szakdoga.actors.models.player;

import java.util.LinkedList;
import java.util.List;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.actors.models.Position;
import elte.szakdoga.battle.controller.BattleControllerInterface;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.map.models.fields.ShopField;
import elte.szakdoga.messages.models.MyMessage;

/**
 * The player of the game. It will have extra feautres which Actors don't have
 * like points, party.
 */
public class Player extends Actor{
	private Position position;
	private transient List<MyMessage> messages;
	private int id;
	private static int idGeneration = 0;
	private ShopField shop;
	private boolean gameOver;	

	public Player() {
		super();
		messages = new LinkedList<>();	
	}

	protected Player(Position startingSpot) {
		super();
		this.position = startingSpot;
		messages = new LinkedList<>();
		id = idGeneration++;
	}

	public Player(Player player) {
		super();
		setName(player.getName());
		setStrength(player.getStrength());
		setMag(player.getMagic());
		setEndurance(player.getEndurance());
		setAgility(player.getAgility());
		setLuck(player.getLuck());
		setHP(player.getHP());
		setMaxHP(player.getMaxHP());
		setEquipment(this);
		setPosition(player.getPosition());
		setId(player.getId());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public static Player createNewPlayer(Position startingSpot) {
		Player player = new Player(startingSpot);
		player.setName("Teszt");
		player.setStrength(100);
		player.setMag(100);
		player.setEndurance(100);
		player.setAgility(100);
		player.setLuck(100);
		player.setHP(100);
		player.setMaxHP(200);
		player.setEquipment(player);
		return player;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Name: ");
		sb.append(getName());
		sb.append("\n");
		sb.append("\n");
		sb.append("HP");
		sb.append(getHP());
		return sb.toString();
	}

	public List<Item> getItemsList() {
		return getEquipment().getItemsList();
	}

	@Override
	public void takeAction(BattleControllerInterface battleField) {

	}

	public void addMessage(MyMessage message) {
		this.messages.add(message);
	}

	public boolean gotMessages() {
		return this.messages.size() > 0;
	}

	public List<MyMessage> getMessages() {
		List<MyMessage> messages = new LinkedList<>(this.messages);
		this.messages = new LinkedList<>();
		return messages;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setShop(ShopField shopField) {
		this.shop = shopField;
	}
	
	public ShopField getShop() {
		return shop;
	}
	
	public boolean isGameOver() {
		return gameOver;
	}

	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}

	public boolean addableToBattle() {
		return true;
	}

	public boolean buy(Item item) {
		if(this.getGold() - item.getCost() >= 0) {
			this.addGold(-item.getCost());
			this.addItem(item);
			return true;
		}
		return false;
	}
}
