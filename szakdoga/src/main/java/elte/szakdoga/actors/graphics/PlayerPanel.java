package elte.szakdoga.actors.graphics;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.graphics.listeners.EquipOrUseButtonActionListener;
import elte.szakdoga.items.controller.ItemControllerInterface;
import elte.szakdoga.items.graphics.ItemPanel;
import elte.szakdoga.items.models.Item;

/**
 * A játékosnak a grafikai reprezentációja.
 * @author Teleki Miklós
 *
 */
@SuppressWarnings("serial")
public class PlayerPanel extends JPanel {
    
    private PlayerStats playerStats;
    private JScrollPane scrollPaneForItemPanel;
    private ItemPanel itemPanel;

    /**
     * A játékos panelt létrehozza, és a hozzá tartozó tárgy grafikai reprezentációját.
     * @param player A játékos, ami szerint hozzuk létre a játékos panelt.
     */
    public PlayerPanel(ItemControllerInterface itemController) {
        this.setLayout(new GridBagLayout());
        GridBagConstraints gc = new GridBagConstraints();        
        gc.gridx = 0;
        gc.gridy = 0; 
        gc.ipady = 30;
        
        this.playerStats = new PlayerStats(itemController.getPlayer());
        playerStats.setPreferredSize(new Dimension(100,100));
        add(playerStats, gc);
        
        gc.gridy = 1;               
        
        this.itemPanel = new ItemPanel(itemController);
        this.itemPanel.setItemActionListener(new EquipOrUseButtonActionListener(this.itemPanel.getItemController(), this.itemPanel));
        
        scrollPaneForItemPanel = new JScrollPane(itemPanel);
        scrollPaneForItemPanel.setPreferredSize(new Dimension(200, 150));
        add(scrollPaneForItemPanel, gc);   

    } 
    
    /**
     * Hozzáadunk egy új tárgyat az tárgy reprezentációhoz.
     * @param item A tárgy, amit szeretnénk hozzáadni.
     */
    public void addImage(Item item) {
        itemPanel.addImage(item);
    }
    
    public void removeImage(int id) {
        itemPanel.removeImage(id);
    }

    public void updatePlayerPanel() {
        playerStats.repaint();     
        itemPanel.updateItemPanel(false);
    }

	public void updateModel(Player player) {
		if(playerStats != null)
			this.playerStats.setModel(player);
		if(itemPanel != null)
			this.itemPanel.updateModel(player);
	}    
}
