package elte.szakdoga.actors.graphics;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import elte.szakdoga.network.MyClient;
import elte.szakdoga.network.messages.LoggingMessage;

@SuppressWarnings("serial")
public class ChatField extends JPanel {
	private JTextArea chatMessage;
	private JButton send;
	
	public ChatField() {
		this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.black), "Chat"));
		
		this.chatMessage = new JTextArea();
		chatMessage.setText("Enter text here");
		chatMessage.setPreferredSize(new Dimension(100, 100));
		chatMessage.setMaximumSize(new Dimension(100, 100));
		this.send = new JButton("Send");
		this.setLayout(new GridBagLayout());
		
		GridBagConstraints gc = new GridBagConstraints();
		
		gc.gridx = 0;
		gc.gridy = 0;
		gc.ipady = 20;
		this.add(chatMessage, gc);
		
		gc.ipady = 10;
		gc.gridy = 1;
		
		this.add(send, gc);
	}

	public void setClient(String name, MyClient client) {
		send.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				client.sendLoggingMessage(new LoggingMessage(chatMessage.getText(), name));
			}
		});
	}
}
