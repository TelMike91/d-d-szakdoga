package elte.szakdoga.actors.graphics;

import java.awt.Graphics;

import javax.swing.JPanel;

import elte.szakdoga.actors.models.player.Player;

/**
 * Ez az osztály felel azért hogy kirajzolja a játékosnak az adatait.
 */
@SuppressWarnings("serial")
public class PlayerStats extends JPanel {
    private Player player;
    
    public PlayerStats(Player player) {
        this.player = player;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.drawString("Name: ", 10, 10);
        g.drawString(player.getName(), 60, 10);
        g.drawString("HP: ", 10, 20);
        g.drawString(String.valueOf(player.getHP()) + "/" + String.valueOf(player.getMaxHP()), 40, 20);
        g.drawString("Attack point", 10, 30);
        g.drawString(String.valueOf(player.getAttackPoint()), 80, 30);
        g.drawString("Armor point" , 10, 40);
        g.drawString(String.valueOf(player.getDefensePoint()), 80, 40);
        g.drawString("Gold", 10, 50);
        g.drawString(String.valueOf(player.getGold()), 80, 50);
        g.drawString("Strength", 10, 60);
        g.drawString(String.valueOf(player.getStrength()), 80, 60);
        g.drawString("Magic", 10, 70);
        g.drawString(String.valueOf(player.getMagic()), 80, 70);
        g.drawString("Endurance", 10, 80);
        g.drawString(String.valueOf(player.getEndurance()), 80, 80);
        g.drawString("Agility", 10, 90);
        g.drawString(String.valueOf(player.getAgility()), 80, 90);
        g.drawString("Luck", 10, 100);
        g.drawString(String.valueOf(player.getLuck()), 80, 100);
    }

    public Player getPlayer() {
        return player;
    }

	public void setModel(Player player) {
		this.player = player;
	}
}
