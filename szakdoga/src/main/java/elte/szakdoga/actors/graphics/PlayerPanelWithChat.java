package elte.szakdoga.actors.graphics;

import java.awt.GridBagConstraints;

import elte.szakdoga.items.controller.ItemControllerInterface;
import elte.szakdoga.network.MyClient;

@SuppressWarnings("serial")
public class PlayerPanelWithChat extends PlayerPanel {
    private ChatField chatField;
	
	public PlayerPanelWithChat(ItemControllerInterface itemController, MyClient client) {
		super(itemController);
		
		GridBagConstraints gc = new GridBagConstraints();
        
        gc.gridy = 2;
        chatField = new ChatField();
        chatField.setClient(itemController.getPlayer().getName(), client);
        this.add(chatField, gc);
	}
}
