package elte.szakdoga;

public interface MyObserved {
	public void addObserver(MyObserver observer);
	public void removeObserver(MyObserver observer);
	public void update();
}
