package elte.szakdoga.logging.graphics;

/**
 * A loggoláshoz tartozó utility osztály, amivel megjeleníthetjük a dolgokat a képernyőn.
 * @author Teleki Miklós
 *
 */
public class LoggingFieldLogger {
    private static final LoggingField loggingField = new LoggingField();
    
    public static LoggingField getLoggingField() {
        return loggingField;
    }            
    
    public static void addText(String text) {
        loggingField.addText(text);
    }
    
    public static void resetLoggingField() {
        loggingField.resetLoggingField();
    }
}
