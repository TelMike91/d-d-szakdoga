package elte.szakdoga.logging.graphics;

import java.awt.Color;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

/**
 * A logolás grafikai reprezentációja
 * @author Teleki Miklós
 *
 */
@SuppressWarnings("serial")
public class LoggingField extends JScrollPane {
	private JTextArea textArea;
	
	public LoggingField() {
		textArea = new JTextArea();
		textArea.setBackground(Color.GRAY);
		this.setViewportView(textArea);		
		textArea.setForeground(Color.WHITE);
		this.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED), "Log", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.TOP));
	}
    
    public void addText(String text) {
    	textArea.setText(textArea.getText()  + text + "\n");
    }
    
    public void resetLoggingField() {
    	textArea.setText("");
    }
}
