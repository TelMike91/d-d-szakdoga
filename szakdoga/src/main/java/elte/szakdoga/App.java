package elte.szakdoga;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

public class App {   
	
	public static final boolean NARRATOR_TEST = false;	
	
    public static void main(String[] args) {   
    	AbstractApplicationContext apc = new AnnotationConfigApplicationContext(AppConfig.class);    	  	
    	apc.close();
    }
}
