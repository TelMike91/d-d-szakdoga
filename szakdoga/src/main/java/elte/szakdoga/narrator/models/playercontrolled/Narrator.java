package elte.szakdoga.narrator.models.playercontrolled;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.actors.models.enemy.Enemy;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.game.controllers.GameControllerInterface;
import elte.szakdoga.map.models.MyMap;
import elte.szakdoga.map.models.fields.EventField;
import elte.szakdoga.map.models.fields.NoEventField;
import elte.szakdoga.messages.models.MyMessage;
import elte.szakdoga.messages.models.PlainMessage;
import elte.szakdoga.narrator.models.ai.NarratorInterface;

public class Narrator implements Runnable, NarratorInterface {
	private Player player;
	private GameControllerInterface gameController;
	private MyMap map;

	public Narrator(MyMap map) {
		this.map = map;
	}

	public void addEvent(EventField eventField) {
		map.setField(eventField);
	}

	public void sendMessage(MyMessage message) {
		player.addMessage(message);
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public void addEnemyToBattle(BattleField battleField, Enemy enemy) {
		battleField.addEnemy(enemy);
	}

	@Override
	public void run() {
		System.out.println("Control is in the narrator's hands");
	}

	@Override
	public void putIntoBattle(BattleField battleField) {
		battleField.addPlayer(player);
	}

	@Override
	public Player getPlayer() {
		return player;
	}

	@Override
	public void sendMessage(String message) {
		player.addMessage(new PlainMessage(message));
	}

	@Override
	public void removeHealth(int amount) {
		player.damage(amount);
	}

	@Override
	public void setMap(MyMap map) {
		this.map = map;
	}

	@Override
	public void shutDown() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteField(Position position) {
		map.setField(new NoEventField(position));
		
	}
}
