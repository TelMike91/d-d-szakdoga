package elte.szakdoga.narrator.models.ai;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.map.models.MyMap;
import elte.szakdoga.map.models.fields.EventField;
import elte.szakdoga.messages.models.MyMessage;

public interface NarratorInterface extends Runnable{
	public void addEvent(EventField eventField);

	public void sendMessage(MyMessage message);

	public void setPlayer(Player player);

	public void putIntoBattle(BattleField battleField);

	public Player getPlayer();

	public void sendMessage(String message);

	public void removeHealth(int amount);

	public void setMap(MyMap map);

	public void shutDown();

	public void deleteField(Position position);
}
