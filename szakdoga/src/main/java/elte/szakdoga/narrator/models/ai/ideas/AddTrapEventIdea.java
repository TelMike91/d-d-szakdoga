package elte.szakdoga.narrator.models.ai.ideas;

import java.util.LinkedList;
import java.util.List;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.map.models.fields.EventField;
import elte.szakdoga.map.models.fields.events.Event;
import elte.szakdoga.map.models.fields.events.TrapEvent;
import elte.szakdoga.narrator.models.ai.NarratorInterface;

public class AddTrapEventIdea implements NarratorAIIdeas {
	private final int damage;
	private final Position position;
	
	public AddTrapEventIdea(int damage, Position position) {
		this.damage = damage;
		this.position = position;
	}

	@Override
	public void doAction(NarratorInterface narrator) {
		List<Event> eventList = new LinkedList<>();
		eventList.add(new TrapEvent(damage));
		narrator.addEvent(new EventField(position, eventList));	
	}

}
