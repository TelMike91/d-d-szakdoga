package elte.szakdoga.narrator.models.ai;

import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.map.models.MyMap;
import elte.szakdoga.map.models.fields.EventField;
import elte.szakdoga.messages.models.MyMessage;

public abstract class NarratorDecorator implements NarratorInterface {
	protected NarratorInterface narrator;

	@Override
	public void run() {
		new Thread(narrator).start();
	}

	@Override
	public void addEvent(EventField eventField) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void sendMessage(MyMessage message) {
		narrator.sendMessage(message);		
	}

	@Override
	public void setPlayer(Player player) {
		narrator.setPlayer(player);		
	}

	@Override
	public Player getPlayer() {
		return narrator.getPlayer();
	}

	@Override
	public void setMap(MyMap map) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void shutDown() {
		// TODO Auto-generated method stub
		
	}

}
