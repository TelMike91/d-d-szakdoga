package elte.szakdoga.narrator.models.ai;

import elte.szakdoga.narrator.models.ai.intelligence.NarratorIntelligenceAction;

public class ListenerNarrator extends NarratorIntelligenceAction {

	public ListenerNarrator(long waitingTime, NarratorInterface narrator) {
		super(waitingTime, narrator);
	}

	@Override
	public void doAction() {

	}

}
