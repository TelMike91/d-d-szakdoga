package elte.szakdoga.narrator.models.ai.ideas;

import elte.szakdoga.narrator.models.ai.NarratorInterface;

public interface NarratorAIIdeas {
	public void doAction(NarratorInterface narrator);
}
