package elte.szakdoga.narrator.models.ai.ideas;

import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.narrator.models.ai.NarratorInterface;

public class SetIntoBattleIdea implements NarratorAIIdeas {
	
	private BattleField battleField;

	public SetIntoBattleIdea(BattleField battleField) {
		this.battleField = battleField;
	}

	@Override
	public void doAction(NarratorInterface narrator) {
		narrator.putIntoBattle(battleField);
	}

}
