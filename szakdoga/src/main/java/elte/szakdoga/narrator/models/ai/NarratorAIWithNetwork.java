package elte.szakdoga.narrator.models.ai;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.map.models.fields.EventField;
import elte.szakdoga.messages.models.MyMessage;
import elte.szakdoga.messages.models.PlainMessage;
import elte.szakdoga.network.MyClient;

public class NarratorAIWithNetwork extends NarratorDecorator {
		
	private MyClient client;

	public NarratorAIWithNetwork(NarratorAI narratorAI, MyClient client) {
		this.narrator = narratorAI;
		this.client = client;		
	}

	@Override
	public void run() {		
		new Thread(narrator).start();
		while(client.isConnected()) {	
			try {
				Thread.sleep(1000);								
			} catch (InterruptedException e) {
				e.printStackTrace();
			}			
			if(!client.getMessages().isEmpty()) {
				for(MyMessage message : client.getMessages()) {					
					narrator.sendMessage(message);					
				}
				client.removeAllMessages();
			}
		}
	}
	
	

	@Override
	public void addEvent(EventField eventField) {
		narrator.addEvent(eventField);
		client.sendAddEventMessage(eventField);
	}

	@Override
	public void sendMessage(MyMessage message) {						
		client.sendNarratorMessage(message);		
	}

	@Override
	public void setPlayer(Player player) {
		narrator.setPlayer(player);		
	}

	@Override
	public Player getPlayer() {
		return narrator.getPlayer();
	}

	@Override
	public void sendMessage(String message) {
		client.sendNarratorMessage(new PlainMessage(message));
	}

	@Override
	public void removeHealth(int amount) {
		narrator.removeHealth(amount);
		client.sendUpdatePlayerMessage(getPlayer());
	}

	@Override
	public void putIntoBattle(BattleField battleField) {
		narrator.putIntoBattle(battleField);		
		client.sendUpdatePlayerMessage(getPlayer());
	}

	@Override
	public void deleteField(Position position) {
		narrator.deleteField(position);
		client.sendDeleteEventMessage(position);
	}
	
}
