package elte.szakdoga.narrator.models.ai.ideas;

import elte.szakdoga.messages.models.PlainMessage;
import elte.szakdoga.narrator.models.ai.NarratorInterface;

public class SendPlainMessageIdea implements NarratorAIIdeas {
	
	private String messageText;

	public SendPlainMessageIdea(String messageText) {
		this.messageText = messageText;
	}

	@Override
	public void doAction(NarratorInterface narrator) {
		narrator.sendMessage(new PlainMessage(messageText));
	}

}
