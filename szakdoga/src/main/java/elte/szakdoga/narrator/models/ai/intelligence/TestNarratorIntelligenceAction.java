package elte.szakdoga.narrator.models.ai.intelligence;

import java.util.LinkedList;
import java.util.List;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.actors.models.enemy.Enemy;
import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.graphics.decisions.BattleDecision;
import elte.szakdoga.graphics.decisions.Decision;
import elte.szakdoga.graphics.decisions.DoNothingDecision;
import elte.szakdoga.narrator.models.ai.NarratorInterface;
import elte.szakdoga.narrator.models.ai.ideas.AddTrapEventIdea;
import elte.szakdoga.narrator.models.ai.ideas.NarratorAIIdeas;
import elte.szakdoga.narrator.models.ai.ideas.SendDecisionMessageIdea;
import elte.szakdoga.narrator.models.ai.ideas.SendPlainMessageIdea;

public class TestNarratorIntelligenceAction extends NarratorIntelligenceAction {	
	public TestNarratorIntelligenceAction(long waitingTime, NarratorInterface narrator) {
		super(waitingTime, narrator);
		
		List<Enemy> testEnemy = new LinkedList<>();
		
		testEnemy.add(Enemy.createDefaultEnemy());
		
		ideas.add(new SendPlainMessageIdea("Ok"));
		List<Decision> decisions = new LinkedList<>();
		
		List<Enemy> enemiesInBattle = new LinkedList<>();			
		enemiesInBattle.add(Enemy.createDefaultEnemy());
		decisions.add(new BattleDecision(new BattleField(enemiesInBattle)));
		decisions.add(new DoNothingDecision());		
		ideas.add(new SendDecisionMessageIdea(decisions));
		
		ideas.add(new AddTrapEventIdea(10, new Position(0,0)));
	}
	
	public void addIdea(NarratorAIIdeas idea) {
		ideas.add(idea);
	}
	
	public long getWaitingTime() {
		return waitingTime;
	}

	public void doAction() {
		if(ideas.size() > 0) {
			ideas.get(0).doAction(narrator);
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			ideas.remove(0);
		}
	}
}
