package elte.szakdoga.narrator.models.playercontrolled;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.map.models.MyMap;
import elte.szakdoga.map.models.fields.EventField;
import elte.szakdoga.messages.models.MyMessage;
import elte.szakdoga.messages.models.PlainMessage;
import elte.szakdoga.narrator.models.ai.NarratorInterface;
import elte.szakdoga.network.MyClient;

public class NarratorWithNetwork implements NarratorInterface {
	private MyClient client;
	private NarratorInterface narrator;
	
	public NarratorWithNetwork(MyClient client) {
		this.client = client;
	}
	
	@Override
	public void addEvent(EventField eventField) {
		client.sendAddEventMessage(eventField);
	}

	@Override
	public void sendMessage(MyMessage message) {	
		client.sendNarratorMessage(message);			
	}

	@Override
	public void setPlayer(Player player) {
			
	}

	@Override
	public Player getPlayer() {
		return narrator.getPlayer();
	}

	@Override
	public void sendMessage(String message) {
		client.sendNarratorMessage(new PlainMessage(message));
	}

	@Override
	public void removeHealth(int amount) {
		narrator.removeHealth(amount);		
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setMap(MyMap map) {
		
	}

	@Override
	public void putIntoBattle(BattleField battleField) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void shutDown() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteField(Position position) {
		client.sendDeleteEventMessage(position);
	}
}
