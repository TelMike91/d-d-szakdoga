package elte.szakdoga.narrator.models.ai.intelligence;

import java.util.ArrayList;
import java.util.List;

import elte.szakdoga.narrator.models.ai.NarratorInterface;
import elte.szakdoga.narrator.models.ai.ideas.NarratorAIIdeas;

public abstract class NarratorIntelligenceAction {
	protected long waitingTime;
	protected NarratorInterface narrator;
	protected List<NarratorAIIdeas> ideas;
	
	public NarratorIntelligenceAction(long waitingTime, NarratorInterface narrator) {
		this.waitingTime = waitingTime;
		this.narrator = narrator;
		ideas = new ArrayList<>();
	}
	
	public long getWaitingTime() {
		return waitingTime;
	}

	public abstract void doAction();
}
