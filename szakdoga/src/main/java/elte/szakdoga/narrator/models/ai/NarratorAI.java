package elte.szakdoga.narrator.models.ai;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.actors.models.enemy.Enemy;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.map.models.MyMap;
import elte.szakdoga.map.models.fields.EventField;
import elte.szakdoga.map.models.fields.NoEventField;
import elte.szakdoga.messages.models.MyMessage;
import elte.szakdoga.messages.models.PlainMessage;
import elte.szakdoga.narrator.models.ai.intelligence.NarratorIntelligenceAction;
import elte.szakdoga.narrator.models.ai.intelligence.TestNarratorIntelligenceAction;

public class NarratorAI implements Runnable, NarratorInterface {
	private Player narratedPlayer;
	private MyMap map;

	public enum NarratorIntelligence {
		ACTIVE {
			@Override
			public NarratorIntelligenceAction getAction(NarratorInterface narrator) {
				return null;
			}
		},
		LISTENER_NARRATOR {

			@Override
			public NarratorIntelligenceAction getAction(NarratorInterface narrator) {
				return new ListenerNarrator(1000, narrator);
			}
			
		},
		TEST {
			@Override
			public NarratorIntelligenceAction getAction(NarratorInterface narrator) {
				return new TestNarratorIntelligenceAction(3000, narrator);
			}
		};

		public abstract NarratorIntelligenceAction getAction(NarratorInterface narrator);
	}

	private NarratorIntelligenceAction intelligence;
	private boolean shutDown = false;

	public NarratorAI(MyMap map) {
		this.map = map;		
	}

	public NarratorIntelligenceAction getIntelligence() {
		return intelligence;
	}

	public void setIntelligence(NarratorIntelligenceAction intelligence) {
		this.intelligence = intelligence;
	}

	@Override
	public void run() {
		while (!shutDown) {
			try {
				Thread.sleep(intelligence.getWaitingTime());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}			
			intelligence.doAction();
		}
	}

	public void addEvent(EventField eventField) {
		map.setField(eventField);
	}

	public void sendMessage(MyMessage message) {
		if(narratedPlayer != null)
			narratedPlayer.addMessage(message);
	}

	public void setPlayer(Player player) {
		this.narratedPlayer = player;
	}

	public void addEnemyToBattle(BattleField battleField, Enemy enemy) {
		battleField.addEnemy(enemy);
	}


	public void putIntoBattle(BattleField battleField) {
		System.out.println(narratedPlayer);
		System.out.println("PUT INTO BATTLE");
//		narratedPlayer.setBattle(battleField);
		battleField.addPlayer(narratedPlayer);
	}

	public Player getPlayer() {
		return narratedPlayer;
	}

	@Override
	public void sendMessage(String message) {		
		narratedPlayer.addMessage(new PlainMessage(message));		
	}

	@Override
	public void removeHealth(int amount) {
		narratedPlayer.damage(amount);
	}

	@Override
	public void setMap(MyMap map) {
		this.map = map;
	}

	@Override
	public void shutDown() {
		this.shutDown = true;
	}

	@Override
	public void deleteField(Position position) {
		map.setField(new NoEventField(position));
	}
}
