package elte.szakdoga.narrator.models.ai.ideas;

import java.util.List;

import elte.szakdoga.graphics.decisions.Decision;
import elte.szakdoga.messages.models.DecisionMessage;
import elte.szakdoga.narrator.models.ai.NarratorInterface;

public class SendDecisionMessageIdea implements NarratorAIIdeas {
	private List<Decision> decisions;


	public SendDecisionMessageIdea(List<Decision> decisions) {
		this.decisions = decisions;
	}
	

	@Override
	public void doAction(NarratorInterface narrator) {		
		narrator.sendMessage(new DecisionMessage("Decide", decisions));
	}

}
