package elte.szakdoga.narrator.graphics;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
//import javax.swing.JList;
import javax.swing.JPanel;

import elte.szakdoga.actors.graphics.ChatField;
import elte.szakdoga.game.controllers.GameControllerInterface;
import elte.szakdoga.game.graphics.MainWindow;
import elte.szakdoga.narrator.controllers.NarratorController;
import elte.szakdoga.network.MyClient;

/**
 * A Narrátor panelja, amit csak a narrátor láthat majd, és írányítja a játékmenetet.
 * @author Teleki Miklós
 *
 */
@SuppressWarnings("serial")
public class NarratorPanel extends JPanel {
	private JButton addEvent;
	private JButton sendMessage;
	private JButton decisionMessage;
	private JButton deleteEvent;
	private NarratorController narratorController;	
	private MainWindow mainWindow;
	private ChatField chatField;
	
	/**
	 * Létrehozzuk az alap narrátori panelt.
	 * @param gameController A GameController ami tartalmazza a játék menetét.
	 */
	public NarratorPanel(GameControllerInterface gameController, MyClient client) {
		
		addEvent = new JButton("Add event");
		deleteEvent = new JButton("Delete event");
		sendMessage = new JButton("Send message");
		narratorController = gameController.getNarratorController();
		mainWindow = gameController.getMainFrame();
		chatField = new ChatField();
		
		gameController.addNarratorMouseListenerForMap(narratorController.getNarrator());

		addEvent.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				showMessage(new AddEventDialog(narratorController));
			}
		});
		
		deleteEvent.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				gameController.addDeleteEventListener(narratorController.getNarrator());
			}
		});
		
		sendMessage.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent actionEvent) {
				showMessage(new NarratorSendMessageDialog(narratorController));
			}
		});
		
		decisionMessage = new JButton("Decision message");
		
		decisionMessage.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				showMessage(new NarratorSendDecisionDialog(narratorController));
			}
		});

		this.setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();
		
		gc.gridx = 0;
		gc.gridy = 0;
		this.add(addEvent, gc);
		gc.gridy = 1;
		this.add(deleteEvent,gc );
		gc.gridy = 2;
		this.add(decisionMessage,gc);
		gc.gridy = 3;
		this.add(sendMessage, gc);
		gc.gridy = 4;
		this.add(chatField, gc);
		
		chatField.setClient("Narrator", client);
	}
	
	protected void showMessage(JDialog dialogBox) {
		JDialog sendMessage = dialogBox;
		sendMessage.setLocation(mainWindow.getMiddlePoint());
		sendMessage.setVisible(true);
		sendMessage.pack();
	}
}
