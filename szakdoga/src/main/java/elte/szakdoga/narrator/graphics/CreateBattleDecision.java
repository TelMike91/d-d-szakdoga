package elte.szakdoga.narrator.graphics;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JScrollPane;

import elte.szakdoga.battle.models.BattleField;

@SuppressWarnings("serial")
public class CreateBattleDecision extends JDialog {

	private JScrollPane enemiesScrollPane;
	private JScrollPane addedEnemiesScrollPane;
	private JList<String> enemies;
	private JList<String> addedEnemies;
	private JButton addEnemy;
	private JButton removeEnemy;
	private JButton confirm;
	private JButton cancel;
	private EnemiesModel enemiesModel;
	private EnemiesModel addedEnemiesModel;
	

	public CreateBattleDecision(NarratorSendDecisionDialog dialog) {
		dialog.setEnabled(false);
		
		enemiesModel = new EnemiesModel();
		addedEnemiesModel = new EnemiesModel();
		enemiesModel.addEnemies();
		enemies = new JList<>(enemiesModel);
		addedEnemies = new JList<>(addedEnemiesModel);
		this.enemiesScrollPane = new JScrollPane(enemies);
		this.addedEnemiesScrollPane = new JScrollPane(addedEnemies);
		
		addEnemy = new JButton("Add");
		
		addEnemy.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {				
				if(!enemies.isSelectionEmpty() && addedEnemiesModel.getSize() < 4) {
					addedEnemiesModel.addEnemy(enemiesModel.getEnemyAt(enemies.getSelectedIndex()));
					addedEnemies.setModel(new EnemiesModel());
					addedEnemies.setModel(addedEnemiesModel);
					addedEnemies.repaint();
					addedEnemies.revalidate();					
				}
			}
		});
		removeEnemy = new JButton("Remove");
				
		confirm = new JButton("Confirm");
		
		confirm.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				dialog.addFightDecision(new BattleField(addedEnemiesModel.getEnemies()));
				dialog.setEnabled(true);
				dispose();
			}
		});
		
		cancel = new JButton("Cancel");
		
		cancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				dialog.setEnabled(true);
				dispose();
			}
		});
		
		this.setLayout(new GridBagLayout());
		
		GridBagConstraints gc = new GridBagConstraints();
		
		gc.gridx = 0;
		gc.gridy = 0;
		
		this.add(enemiesScrollPane,gc );
		gc.gridx = 1;
		this.add(addedEnemiesScrollPane, gc);
		gc.gridx = 0;
		gc.gridy = 1;
		this.add(addEnemy, gc);
		gc.gridx = 1;
		this.add(removeEnemy, gc);
		gc.gridx = 2;

		this.add(confirm,gc );
		gc.gridx = 3;
		this.add(cancel, gc);
	}

}
