package elte.szakdoga.narrator.graphics;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;

import elte.szakdoga.game.graphics.ChangeObserver;
import elte.szakdoga.game.graphics.LabelAndTextField;
import elte.szakdoga.graphics.decisions.LoseHealthDecision;

@SuppressWarnings("serial")
public class CreateLoseHealthDecision extends JDialog implements ChangeObserver {
	private LabelAndTextField value;
	private JButton confirm;
	private JButton back;

	public CreateLoseHealthDecision(NarratorSendDecisionDialog dialog) {
		dialog.setEnabled(false);
		value = new LabelAndTextField(this, "How much health: ");
		confirm = new JButton("Confirm");
		confirm.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				dialog.setEnabled(true);
				dialog.addLoseHealthDecision(new LoseHealthDecision(value.getValue()));
				dispose();
			}
		});
		back = new JButton("Back");
		
		back.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				dialog.setEnabled(true);
				dispose() ;
			}
		});
		
		this.setLayout(new GridBagLayout());
		
		GridBagConstraints gc = new GridBagConstraints();
		gc.gridx = 0;
		gc.gridy = 0;
		
		this.add(value.getName(), gc);
		gc.gridx = 1;
		this.add(value.getTextField(), gc);
		gc.gridy = 1;
		gc.gridx = 0;
		this.add(confirm, gc);
		gc.gridx = 1;
		this.add(back, gc);
		
		this.pack();
	}

	@Override
	public void update() {
		
	}
	
	

}
