package elte.szakdoga.narrator.graphics;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.JTextArea;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.graphics.decisions.BattleDecision;
import elte.szakdoga.graphics.decisions.Decision;
import elte.szakdoga.graphics.decisions.LoseHealthDecision;
import elte.szakdoga.messages.models.DecisionMessage;
import elte.szakdoga.narrator.controllers.NarratorController;

@SuppressWarnings("serial")
public class NarratorSendDecisionDialog extends JDialog {
	private List<Decision> decisionList;
	private List<DecisionListEnum> addedDecisions;
	private JList<DecisionListEnum> addedDecisionGraphicList;
	private JList<DecisionListEnum> decisionGraphicList;
	private JTextArea message;
	private JButton sendDialog;
	private JButton addDecision;

	public enum DecisionListEnum {
		LOSE_HEALTH {
			@Override
			public void createDecision(NarratorSendDecisionDialog dialog) {
				new CreateLoseHealthDecision(dialog).setVisible(true);
			}
		},
		FIGHT {
			@Override
			public void createDecision(NarratorSendDecisionDialog dialog) {
				new CreateBattleDecision(dialog).setVisible(true);
			}
		};

		public abstract void createDecision(NarratorSendDecisionDialog dialog);
	}

	public class DecisionListModel implements ListModel<DecisionListEnum> {
		private List<DecisionListEnum> decisions;
		private List<ListDataListener> listDataListeners;

		public DecisionListModel(List<DecisionListEnum> decisions) {
			this.decisions = new LinkedList<>(decisions);
			listDataListeners = new LinkedList<>();
		}

		@Override
		public void addListDataListener(ListDataListener listener) {
			listDataListeners.add(listener);
		}

		@Override
		public DecisionListEnum getElementAt(int index) {
			return decisions.get(index);
		}

		@Override
		public int getSize() {
			return decisions.size();
		}

		@Override
		public void removeListDataListener(ListDataListener listener) {
			listDataListeners.remove(listener);
		}

	}

	public NarratorSendDecisionDialog(NarratorController narrator) {
		decisionList = new LinkedList<>();
		sendDialog = new JButton("Send message");
		addDecision = new JButton("Add decision");
		message = new JTextArea();
		decisionGraphicList = new JList<>();
		addedDecisions = new LinkedList<>();

		List<DecisionListEnum> decisions = new LinkedList<>();

		decisions.add(DecisionListEnum.FIGHT);
		decisions.add(DecisionListEnum.LOSE_HEALTH);

		decisionGraphicList.setModel(new DecisionListModel(decisions));

		this.setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();
		gc.gridx = 0;
		gc.gridy = 0;
		decisionGraphicList.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED), "Selectable decisions"));
		
		this.add(decisionGraphicList, gc);
		gc.gridx = 1;
		addedDecisionGraphicList = new JList<>();
		addedDecisionGraphicList.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.red), "Added decisions"));
		this.add(addedDecisionGraphicList, gc);

		sendDialog.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(decisionList.size() > 0)
					narrator.sendMessage(new DecisionMessage(message.getText(), decisionList));
				dispose();
			}
		});
		gc.gridy = 1;
		gc.gridx = 0;
		message.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.RED), "Message"));
		gc.ipadx = 30;
		gc.ipady = 30;
		this.add(message, gc);
		gc.ipadx = 0;
		gc.ipady = 0;
		gc.gridx = 0;
		gc.gridy = 2;
		this.add(sendDialog, gc);
		gc.gridx = 1;
		this.add(addDecision, gc);
		addDecision.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!decisionGraphicList.isSelectionEmpty()) {
					decisionGraphicList.getSelectedValue().createDecision(getThis());
				}
			}
		});
	}
	
	private NarratorSendDecisionDialog getThis() {
		return this;
	}
	
	public void addLoseHealthDecision(LoseHealthDecision decision) {
		decisionList.add(decision);
		addedDecisions.add(DecisionListEnum.LOSE_HEALTH);
		addedDecisionGraphicList.setModel(new DecisionListModel(addedDecisions));
	}

	public void addFightDecision(BattleField battleField) {
		decisionList.add(new BattleDecision(battleField));
		addedDecisions.add(DecisionListEnum.FIGHT);
		addedDecisionGraphicList.setModel(new DecisionListModel(addedDecisions));
	}

}
