package elte.szakdoga.narrator.graphics;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JList;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

import elte.szakdoga.map.models.fields.EventField;
import elte.szakdoga.narrator.controllers.NarratorController;

@SuppressWarnings("serial")
public class AddEventDialog extends JDialog {
	private JButton send;
	private JButton add;
	private JButton cancel;
	private JList<EventTypes> eventsList;	
	private JList<String> addedEvents;
	private AddedEvents addedEventsModel;
	
	private enum EventTypes {
		TRAP {
			@Override
			public void showDialog(AddedEvents addedEventsModel, AddEventDialog addEventDialog) {
				new AddTrapEventDialog(addedEventsModel, addEventDialog).setVisible(true);					
			}
		}, BATTLE {
			@Override
			public void showDialog(AddedEvents addedEventsModel, AddEventDialog addEventDialog) {
				new AddBattleDialog(addedEventsModel, addEventDialog).setVisible(true);
			}
		};
		
		public abstract void showDialog(AddedEvents addedEventsModel, AddEventDialog addEventDialog);
	}
	
	class EventListModel implements ListModel<EventTypes> {
		private List<EventTypes> events;
		
		public EventListModel() {
			events = new LinkedList<>();
		}
		
		public void init() {
			for(EventTypes eventType : EventTypes.values()) {
				events.add(eventType);
			}
		}

		@Override
		public void addListDataListener(ListDataListener l) {
			
		}

		@Override
		public EventTypes getElementAt(int index) {
			// TODO Auto-generated method stub
			return events.get(index);
		}

		@Override
		public int getSize() {
			// TODO Auto-generated method stub
			return events.size();
		}

		@Override
		public void removeListDataListener(ListDataListener l) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	public AddEventDialog getThis() {
		return this;
	}
	
	public AddEventDialog(NarratorController controller) {
		send = new JButton("Send");
		add = new JButton("Add");
		cancel = new JButton("Cancel");
		
		cancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		
		EventListModel types = new EventListModel();
		types.init();
		eventsList = new JList<>();
		eventsList.setModel(types);	
		addedEvents = new JList<>();
		addedEventsModel = new AddedEvents();
		
		addedEvents.setModel(addedEventsModel);
		this.setLayout(new FlowLayout());
		
		this.add(eventsList);
		this.add(addedEvents);
		this.add(send);
		this.add(add);
		this.add(cancel);
		
		
		
		add.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				eventsList.getSelectedValue().showDialog(addedEventsModel, getThis());
				EventField eventField = new EventField();
				eventField.setEvents(addedEventsModel.getEvents());
				controller.addNewEventFieldListener(controller.getNarrator(), eventField);
				getThis().setEnabled(false);

			}
		});
		
		this.pack();
	}
}
