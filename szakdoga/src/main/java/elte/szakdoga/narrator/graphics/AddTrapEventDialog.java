package elte.szakdoga.narrator.graphics;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import elte.szakdoga.map.models.fields.events.TrapEvent;

@SuppressWarnings("serial")
public class AddTrapEventDialog extends JDialog {
	private JLabel valueOfTrap;
	private JButton confirm;
	private JButton cancel;
	private JTextField value;
	
	public AddTrapEventDialog(AddedEvents addedEventsModel, AddEventDialog addEventDialog) {
		addEventDialog.setEnabled(false);
		
		valueOfTrap = new JLabel("Value of damage");
		value = new JTextField(4);
		confirm = new JButton("Confirm");
		cancel = new JButton("Cancel");
		
		confirm.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				addedEventsModel.addEvent(new TrapEvent(Integer.parseInt(value.getText())));
				addEventDialog.setEnabled(true);
				JOptionPane.showMessageDialog(addEventDialog, "Choose a field to add the event", "Message", JOptionPane.INFORMATION_MESSAGE);
				addEventDialog.revalidate();
				addEventDialog.repaint();
				dispose();				
			}
		});

		cancel.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				addEventDialog.setEnabled(true);				
				dispose();
			}
		});
		
		this.setLayout(new FlowLayout());
		this.add(valueOfTrap);
		this.add(value);
		this.add(confirm);
		this.add(cancel);
		this.pack();
	}
	
	public int getValue() {
		return Integer.parseInt(value.getText());
	}
}
