package elte.szakdoga.narrator.graphics;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JTextArea;

import elte.szakdoga.messages.models.MyMessage;
import elte.szakdoga.messages.models.PlainMessage;
import elte.szakdoga.narrator.controllers.NarratorController;

@SuppressWarnings("serial")
public class NarratorSendMessageDialog extends JDialog {
	private JTextArea textArea;
	private JButton send;

	public NarratorSendMessageDialog(NarratorController narratorController) {
		textArea = new JTextArea();
		send = new JButton("Send message");
		
		this.setLayout(new GridLayout(2,1));	
		
		send.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent actionEvent) {			
				MyMessage message = new PlainMessage(textArea.getText());
				narratorController.sendMessage(message);
				dispose();
				
			}});
		
		this.add(textArea);				
		this.add(send);
	}
}
