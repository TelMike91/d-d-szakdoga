package elte.szakdoga.narrator.graphics;

import java.util.ArrayList;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

import elte.szakdoga.actors.models.enemy.Enemy;
import elte.szakdoga.database.DatabaseConnection;

public class EnemiesModel implements ListModel<String> {
	private List<Enemy> enemies = new ArrayList<>();
	
	public EnemiesModel() {

	}

	@Override
	public void addListDataListener(ListDataListener l) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getElementAt(int index) {
		// TODO Auto-generated method stub
		return enemies.get(index).getName();
	}

	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		return enemies.size();
	}

	@Override
	public void removeListDataListener(ListDataListener l) {
		// TODO Auto-generated method stub
		
	}

	public void addEnemies() {
		enemies = DatabaseConnection.getInstance().getAllEnemies();
	}

	public Enemy getEnemyAt(int selectedIndex) {
		return enemies.get(selectedIndex);
	}

	public void addEnemy(Enemy enemyAt) {
		
		enemies.add(new Enemy(enemyAt));
	}

	public List<Enemy> getEnemies() {
		return enemies;
	}

}
