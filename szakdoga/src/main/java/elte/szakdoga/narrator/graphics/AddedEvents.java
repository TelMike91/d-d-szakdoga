package elte.szakdoga.narrator.graphics;

import java.util.LinkedList;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

import elte.szakdoga.map.models.fields.events.Event;

public class AddedEvents implements ListModel<String> {
	private List<Event> events;
	private List<String> eventsString;
	
	public void addEvent(Event event) {
		events.add(event);
		eventsString.add(event.toString());
	}
	
	public AddedEvents() {
		events = new LinkedList<>();
		eventsString = new LinkedList<>();
	}

	@Override
	public void addListDataListener(ListDataListener l) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getElementAt(int index) {
		// TODO Auto-generated method stub
		return eventsString.get(index);
	}

	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		return eventsString.size();
	}

	@Override
	public void removeListDataListener(ListDataListener l) {
		// TODO Auto-generated method stub
		
	}

	public List<Event> getEvents() {
		return events;
	}
	
	
}
