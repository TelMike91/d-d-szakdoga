package elte.szakdoga.narrator.controllers;

import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.game.controllers.GameControllerInterface;
import elte.szakdoga.map.models.MyMap;
import elte.szakdoga.map.models.fields.EventField;
import elte.szakdoga.messages.models.MyMessage;
import elte.szakdoga.narrator.models.ai.NarratorInterface;

public class NarratorController {
	private GameControllerInterface gameController;
	private NarratorInterface narrator;
	
	public NarratorController(GameControllerInterface gameController, NarratorInterface narrator) {
		this.gameController = gameController;
		this.narrator = narrator;
	}
	
	public void addFieldMouseListener() {
		gameController.addNarratorMouseListenerForMap(narrator);
	}
	
	public void addEvent(EventField eventField) {
		narrator.addEvent(eventField);
		gameController.update();
	}

	public void start() {
		new Thread(narrator).start();
	}

	public void setPlayer(Player addedPlayer) {		
		narrator.setPlayer(addedPlayer);
	}
	
	public void sendMessage(MyMessage message) {
		narrator.sendMessage(message);
	}

	public NarratorInterface getNarrator() {
		return narrator;
	}

	public void putIntoBattle(BattleField battleField) {
		narrator.putIntoBattle(battleField);
	}

	public void setMap(MyMap map) {
		narrator.setMap(map);
	}

	public void addNewEventFieldListener(NarratorInterface narrator, EventField eventField) {
		gameController.addNewEventFieldListener(narrator, eventField);
	}

	public void shutDown() {
		narrator.shutDown();
	}		
}
