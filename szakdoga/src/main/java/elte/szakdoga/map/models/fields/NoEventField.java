package elte.szakdoga.map.models.fields;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.map.graphics.MyFieldType;

/**
 * A field where nothing will happen, if the player steps here.
 */
public class NoEventField extends Field {

	public NoEventField() {
		super(null);
		type = MyFieldType.NOTHING;
	}

	public NoEventField(Position position) {
		super(position);
		type = MyFieldType.NOTHING;
	}

	@Override
	public String toString() {
		return getSteppedPlayerHere() != null ? "X" : "O";
	}

	@Override
	public boolean isValid() {
		return true;
	}

	@Override
	public String getMessage() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public void visit(FieldProcessorInterface fieldProcessor) {
		fieldProcessor.processField(this);
	}

}
