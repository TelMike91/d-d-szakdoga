package elte.szakdoga.map.models.fields.events;

/**
 * A trap event where the player loses x amount of health.
 */
public class TrapEvent implements Event {
    private int damage;
    
    public TrapEvent() {
    	
	}
    
    public TrapEvent(int damage) {
        this.damage = damage;
    }
    
    public int getDamage() {
    	return damage;
    }
    
    @Override
    public String toString() {
    	return "Trap event. Damage: " + damage;
    }

	@Override
	public String getMessage() {
		return "Trap! Damage suffered: " + damage;
	}

	@Override
	public void visit(EventProcessorInterface eventProcessor) {
		eventProcessor.processEvent(this);
	}

}
