package elte.szakdoga.map.models.fields.events;

import elte.szakdoga.battle.models.BattleField;

/**
 * Esemény, ahol a játékosoknak ellenfelekkel kell megküzdeniük
 */
public class BattleEvent implements Event {
	private BattleField field;

	public BattleEvent() {

	}

	/**
	 * Létrehozzuk a harci eseményt, amire ha rálépnek a játékosok, akkor majd az
	 * itt megadott ellenfelekkel kell harcolniuk.
	 * 
	 * @param Az ellenfelek listája, amikkel harcolni kell.
	 */
	public BattleEvent(BattleField battleField) {
		field = battleField;
	}

	public BattleField getField() {
		return field;
	}

	@Override
	public String getMessage() {
		return "Initiating battle";
	}

	@Override
	public void visit(EventProcessorInterface eventProcessor) {
		eventProcessor.processEvent(this);		
	}
}
