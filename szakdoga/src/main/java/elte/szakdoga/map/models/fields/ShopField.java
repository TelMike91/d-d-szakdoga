package elte.szakdoga.map.models.fields;

import java.util.LinkedList;
import java.util.List;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.map.graphics.MyFieldType;

public class ShopField extends Field {
	private List<Item> purchasableItems;
	private List<Player> visitingPlayers;
	
	public ShopField() {
		super(null);
		purchasableItems = new LinkedList<>();
		visitingPlayers = new LinkedList<>();
	}

	public ShopField(Position position, List<Item> purchasableItems) {
		super(position);
		this.purchasableItems = purchasableItems;
		this.type = MyFieldType.SHOP;
		visitingPlayers = new LinkedList<>();
	}


	@Override
	public boolean isValid() {
		return true;
	}

	public List<Item> getPurchasableItems() {
		return purchasableItems;
	}

	public void setPurchasableItems(List<Item> purchasableItems) {
		this.purchasableItems = purchasableItems;
	}

	public void removePlayer(Player currentPlayer) {
		visitingPlayers.remove(currentPlayer);
	}
	
	@Override
	public String toString() {
		return "S";
	}

	@Override
	public String getMessage() {
		return "Arrived to shop";
	}

	@Override
	public void visit(FieldProcessorInterface fieldProcessor) {
		fieldProcessor.processField(this);
	}

}
