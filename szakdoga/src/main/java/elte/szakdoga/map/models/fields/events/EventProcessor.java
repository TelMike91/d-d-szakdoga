package elte.szakdoga.map.models.fields.events;

import elte.szakdoga.game.controllers.GameControllerInterface;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.logging.graphics.LoggingFieldLogger;

public class EventProcessor implements EventProcessorInterface {
	private GameControllerInterface gameService;		

	public EventProcessor(GameControllerInterface gameService) {
		this.gameService = gameService;
	}
	
	public void processEvent(TrapEvent trapEvent) {
		LoggingFieldLogger.addText("Trap event");
		gameService.damage(trapEvent.getDamage());
	}
	
	public void processEvent(ItemEvent itemEvent) {
        for(Item item : itemEvent.getItems()) {
        	gameService.addItem(item);
        }
	}
	
	public void processEvent(BattleEvent battleEvent) {
		gameService.setBattle(battleEvent.getField());
	}

	public void processEvent(Event e) {
		e.visit(this);
	}
}
