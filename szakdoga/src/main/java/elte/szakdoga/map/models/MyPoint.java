package elte.szakdoga.map.models;

public class MyPoint {
	private final int x;
	private final int y;
	
	public MyPoint() {
		x = 0;
		y = 0;
	}
	
	
	public MyPoint(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
	
	@Override
	public String toString() {
		return  "X: " + x + " Y: " + y;
	}
		
}
