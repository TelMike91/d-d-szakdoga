package elte.szakdoga.map.models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Logger;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.actors.models.Position.Direction;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.map.models.fields.EndGameField;
import elte.szakdoga.map.models.fields.Field;
import elte.szakdoga.map.models.fields.NoEventField;

/**
 * A térkép modellje. 
 * @author Teleki Miklós
 *
 */
public class MyMap {
	private Map<Position, MyPoint> correspondingField;
    private List<List<Field>> fieldList;
    private static Logger logger = Logger.getLogger(MyMap.class.getName());
    private EndGameField endGameField;
    
    public MyMap() {
    	fieldList = new LinkedList<>(); 
    	correspondingField = new HashMap<>();
    }

    private MyMap(int rows, int cols) {
        fieldList = new ArrayList<>();
        correspondingField = new HashMap<>();
        for (int i = 0; i < rows; i++) {
            fieldList.add(new ArrayList<>());
            for (int m = 0; m < cols; m++) {
            	Position position = new Position(i, m);
                fieldList.get(i).add(new NoEventField(position));
                correspondingField.put(position, new MyPoint(i,m)); 
            }
        }
    }
    
    public MyMap(MyMap model) {
    	this.fieldList = new LinkedList<>(model.getFields());  
    	this.correspondingField = new HashMap<>(model.getCorrespondingFields());
    	this.endGameField = new EndGameField(model.endGameField);
	}
    
    private Map<Position, MyPoint> getCorrespondingFields() {
		return correspondingField;
	}

	public MyMap getSmallMapFromPlayerPositon(Position position) {
    	MyMap map = new MyMap();
    	List<List<Field>> fields = getSmallView(position);
    	int rowNumber = 0;
    	
    	for(List<Field> row : fields) {
    		map.fieldList.add(new LinkedList<>());
    		int columnNumber = 0;
    		for(Field column : row) {
    			map.fieldList.get(rowNumber).add(column);
    			map.correspondingField.put(column.getPosition(), new MyPoint(rowNumber, columnNumber++));
    		}
    		rowNumber++;
    	}
    	return map;
    }	

	/**
     * Creates a map with special features listed in the specialFields. It always creates a Map though if a field has wrong arguments it will not add it.
     * 
     * @param rows
     *            Number of rows this map has
     * @param cols
     *            Number of columns this map has.
     * @param specialFields
     *            The list of fields that has a special feature to it.
     * @return A map with special features. Care should be taken for the special fields so that it should be inbounds.
     */
    public static MyMap createMap(int rows, int cols, List<Field> specialFields) {
        MyMap map = new MyMap(rows, cols);
        for (Field field : specialFields) {
            try {
                map.setField(field);
            } catch (IllegalArgumentException exception) {
                logger.warning(exception.getMessage());
            }
        }
        return map;
    }
    
    /**
     * Beállítjuk a végmezőt, ahol amire rálép a játékos, véget ér a játék.
     * @param endGameField A végmező.
     */
    public void setEndField(EndGameField endGameField) {
    	Objects.requireNonNull(endGameField);
    	this.endGameField = endGameField;
    	setField(endGameField);
    }        

    /**
     * Beállítja a mezőt egy új mezőre.
     * @throws NullPointerException
     *            Ha a megadott paraméter null, akkor várható egy exception.
     * @param newField
     *            Az új mező, amire szeretnénk beállítani.
     */
    public void setField(Field newField) {
        Objects.requireNonNull(newField);
        MyPoint point = correspondingField.get(newField.getPosition());
        if(point != null)
        	fieldList.get(point.getX()).set(point.getY(), newField);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (List<Field> fieldsRow : fieldList) {
            for (Field fieldColumn : fieldsRow) {
                sb.append(fieldColumn);
                sb.append(" ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    /**
     * Frissíti a játékos pozícióját
     * 
     * @param player
     *           	A játékos akinek a pozícióját frissítjuk.
     */
    public String updatePlayerPosition(Player player) {
        for (List<Field> fieldRow : fieldList) {
            for (Field field : fieldRow) {
                field.playerSteppedAway();
            }
        }        
        getField(player.getPosition()).ifPresent(field -> field.playerStepsHere(player));
        return getField(player.getPosition()).map(field -> field.getMessage()).orElse("");
    }
    
    public boolean isInBounds(Position position) {
    	return correspondingField.containsKey(position);
    }
    

    /**
     * Ellenőrízzük hogy a mozdulat legális-e vagy sem.
     * 
     * @param position
     *            A pozíció, amit ellenőrízni szeretnénk hogy ráléphet a játékos.
     * @return Igaz ha ráléphet a játékos, hamis különben.
     */
    public boolean checkIfMoveIsLegal(Position position) {    	
        return isInBounds(position) && getField(position)
        		.map(field -> field.isValid())
        		.orElse(false);
    }

    /**
     * Statikus factory metódus, amivel egy egyszerű üres pályát hozhatunk létre a megadott paraméterekkel.
     * 
     * @param rows
     *            A sorok számna.
     * @param cols
     *            Az oszlopok száma.
     * @return Egy térkép, amin nincsenek eseményke.
     */
    public static MyMap createUselessMap(int rows, int cols) {
        return new MyMap(rows, cols);
    }

    /**
     * Returns the field using the argument.
     * 
     * @throws NullPointerException
     *             If the position is null a NullPointerException is expected.
     * @throws IllegalArgumentException
     *             If the position is out of bounds and IllegalArgumentException is expected. Catch it to make to program carry on.
     * @param position
     *            The position of the field we want to get.
     * @return A field we specified.
     */
    public Optional<Field> getField(Position position) {
        Objects.requireNonNull(position);
        MyPoint point = correspondingField.get(position);
        if(point != null) {
        	return Optional.of(fieldList.get(point.getX()).get(point.getY()));
        } else {
        	return Optional.empty();	
        }
        
    }

    /**
     * Returns the number of rows
     * 
     * @return The number of rows
     */
    public int getRows() {
        return fieldList.size();
    }

    /**
     * Returns the number of columns
     * 
     * @return The number of columns
     */
    public int getColumns() {
        return fieldList.size() > 0 ? fieldList.get(0).size() : 0;
    }

    /**
     * Returns an unmodifiable list of fields.
     * 
     * @return An unmodifiable list of fields.
     */
    public List<List<Field>> getFields() {
        return Collections.unmodifiableList(fieldList);
    }

	public static MyMap createMapWithEndField(int rows, int cols, List<Field> specialFields,
			EndGameField endGameField) {
        MyMap map = createMap(rows, cols, specialFields);
        map.setEndField(endGameField);
		return map;
	}

	/**
	 * Frissíti a játékosok pozícióját. Ebből a jelenlegi játékost úgy hogy az esemény végrehajtódjon, a többieknek csak a pozícióját frissítjük.
	 * @param currentPlayer A jelenlegi játékos, aki a helyi játékban van, így rajta végrehajtuk az eseményeket.
	 * @param requestPlayers A játékosok, akiknek csak a pozícióját frissítjük csak.
	 */
	public void updatePlayerPositions(Player currentPlayer, Player[] requestPlayers) {
		for (List<Field> fieldRow : fieldList) {
			fieldRow.stream()
				.filter(f -> f.getSteppedPlayerHere() != null)
				.forEach(f -> f.playerSteppedAway()); 
        }		
        for (Player player : requestPlayers) {        	
    		getField(player.getPosition()).ifPresent(field -> field.playerStepsHere(player));        		
        }
	}
	
	private Position getUpperLeftCorner(Position position) {
		boolean valid = true;
		Position upperLeftCorner = position;
		for(int m = 0; m < 8 && valid; m++) {
			Position upperLeftCornerTest = Direction.LEFT.move(upperLeftCorner);
			if(!isInBounds(upperLeftCornerTest)) {
				valid = false;
			} else {
				upperLeftCorner = upperLeftCornerTest;
			}
		}
		valid = true;
		for(int m = 0; m < 8 && valid; m++) {
			Position upperLeftCornerTest = Direction.UP.move(upperLeftCorner);
			if(!isInBounds(upperLeftCornerTest)) {
				valid = false;
			} else {
				upperLeftCorner = upperLeftCornerTest;
			}
		}
		return upperLeftCorner;		
	}

	public List<List<Field>> getSmallView(Position from) {
		Position upperLeftCorner = getUpperLeftCorner(from);
		List<List<Field>> fields = new LinkedList<>();
		for(int i = 0; i < 15; i++) {			
			if(isInBounds(new Position(upperLeftCorner.getX() + i, upperLeftCorner.getY()))) {
				fields.add(new LinkedList<>());
				addFieldsForSmallView(upperLeftCorner, fields, i);
			}
		}
		return fields;
	}

	// TODO : multiplayernél ez nem jó ugyanis kicsi térképnél másak a koordináták.
	private void addFieldsForSmallView(Position upperLeftCorner, List<List<Field>> fields, int i) {
		for(int m = 0; m < 15; m++) {
			Position testPosition = new Position(upperLeftCorner.getX() + i, upperLeftCorner.getY() + m);
			if(isInBounds(testPosition)) {
				MyPoint point = correspondingField.get(testPosition);
				fields.get(i).add(fieldList.get(point.getX()).get(point.getY()));
			}
		}
	}
}
