package elte.szakdoga.map.models.fields;

import java.util.LinkedList;
import java.util.List;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.map.graphics.MyFieldType;
import elte.szakdoga.map.models.fields.events.Event;

public class EventField extends Field {
    private List<Event> eventList;
    private boolean disappearsAfterUse = false;
    
    public EventField() {
    	super(null);
    	eventList = new LinkedList<>();
    	type = MyFieldType.EVENT;
	}

    public EventField(Position position, List<Event> eventList) {
        super(position);
        this.eventList = new LinkedList<>(eventList);
    	type = MyFieldType.EVENT;
    }
      
    public List<Event> getEventList() {
		return eventList;
	}


	@Override
    public String toString() {
        return getSteppedPlayerHere() != null ? "X" : "E";
    }

    @Override
    public boolean isValid() {
        return true;
    }

	public boolean isDisappearsAfterUse() {
		return disappearsAfterUse;
	}

	public void setDisappearsAfterUse(boolean disappearsAfterUse) {
		this.disappearsAfterUse = disappearsAfterUse;
	}

	public void setEvents(List<Event> events) {
		this.eventList = events;
	}

	@Override
	public String getMessage() {
		StringBuilder sb = new StringBuilder();
		for(Event event : eventList) {
			sb.append(event.getMessage());
			sb.append("\n");
		}
		return sb.toString();
	}

	@Override
	public void visit(FieldProcessorInterface fieldProcessor) {
		fieldProcessor.processField(this);
	}

}
