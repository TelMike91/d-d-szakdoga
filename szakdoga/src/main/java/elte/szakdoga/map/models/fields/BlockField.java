package elte.szakdoga.map.models.fields;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.map.graphics.MyFieldType;

public class BlockField extends Field {   	
	public BlockField() {
		super(null);
		type = MyFieldType.BLOCK;
	}

    public BlockField(Position position) {
        super(position);
		type = MyFieldType.BLOCK;
    }
    
    @Override
    public String toString() {
        return "B";
    }
    
    @Override
    public boolean isValid() {
        return false;
    }

	@Override
	public String getMessage() {
		return "";
	}

	@Override
	public void visit(FieldProcessorInterface fieldProcessor) {
		fieldProcessor.processField(this);
	}

}
