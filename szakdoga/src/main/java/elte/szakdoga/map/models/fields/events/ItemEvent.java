package elte.szakdoga.map.models.fields.events;

import java.util.LinkedList;
import java.util.List;

import elte.szakdoga.items.models.Item;

public class ItemEvent implements Event {
    private List<Item> itemList;
    
    public ItemEvent() {
    	itemList = new LinkedList<>();
	}

    public ItemEvent(List<Item> itemList) {
        this.itemList = new LinkedList<>(itemList);
    }
    
    public void addItem(Item item) {
    	itemList.add(item);
    }

	public List<Item> getItems() {
		return itemList;
	}

	@Override
	public String getMessage() {
		return "Got items!";
	}

	@Override
	public void visit(EventProcessorInterface eventProcessor) {
		eventProcessor.processEvent(this);		
	}

}
