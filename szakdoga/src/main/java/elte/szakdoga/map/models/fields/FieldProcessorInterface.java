package elte.szakdoga.map.models.fields;

public interface FieldProcessorInterface {
	public void processField(EventField eventField);

	public void processField(EndGameField endGameField);

	public void processField(NoEventField noEventField);
	public void processField(BlockField blockField);

	public void processField(ShopField shopField);
}
