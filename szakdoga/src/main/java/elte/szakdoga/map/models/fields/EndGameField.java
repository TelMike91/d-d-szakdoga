package elte.szakdoga.map.models.fields;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.map.graphics.MyFieldType;

public class EndGameField extends Field {		
	public EndGameField() {
		super(null);
		type = MyFieldType.ENDFIELD;
	}

	public EndGameField(Position position) {
		super(position);
		type = MyFieldType.ENDFIELD;
	}
	
	public EndGameField(EndGameField endGameField) {
		super(endGameField.position);
		type = MyFieldType.ENDFIELD;
	}

	@Override
	public boolean isValid() {
		return true;
	}
	
	@Override
	public String toString() {
		return getSteppedPlayerHere() != null ? "X" : "J";
	}

	@Override
	public String getMessage() {
		return "Endgame field reached";
	}

	@Override
	public void visit(FieldProcessorInterface fieldProcessor) {
		fieldProcessor.processField(this);
	}

}
