package elte.szakdoga.map.models.fields;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.map.graphics.MyFieldType;

/**
 * An abstract class which represents a field on the map. Should be used for inheritance and the abstract methods should be overwritten only.
 */
public abstract class Field {
    private String description;
    private Player steppedHereActor;
    protected Position position;
    protected MyFieldType type;
    protected boolean activated = false;
    
    public Field(Position position) {
        this.position = position;   
    }
    
	public void setPosition(Position position) {
		this.position = position;
	}
    
    /**
     * A player steps here an effect should be played on it.
     * @param player Which player steps here.
     */
    public void playerStepsHere(Player player) {
        steppedHereActor = player;
    }
    
    /**
     * A getter which returns the player staying here.
     * @return The player who stay on this field.
     */
    public Player getSteppedPlayerHere() {
        return steppedHereActor;
    }
    
    @Override
    public String toString() {
        return description;
    }
    
    public Position getPosition() {
        return position;
    }

    /**
     * If a player steps away it resets the player staying on this field to null.
     */
    public void playerSteppedAway() {
        steppedHereActor = null;        
    }
    
    /**
     * 
     * @return The column position of the field.
     */
    public int getX() {
        return position.getX();
    }

    /**
     * 
     * @return The row position of the field.
     */
    public int getY() {
        return position.getY();
    }

    /**
     * Returns if a player can step here.
     * @return True if a player can step here, false otherwise.
     */
    public abstract boolean isValid();

	public  MyFieldType getFieldType() {
		if(activated)
			return MyFieldType.NOTHING;
		else
			return type;
	}

	public abstract void visit(FieldProcessorInterface fieldProcessor);
	public abstract String getMessage();
}
