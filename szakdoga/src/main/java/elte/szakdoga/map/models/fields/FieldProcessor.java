package elte.szakdoga.map.models.fields;

import elte.szakdoga.game.controllers.GameControllerInterface;
import elte.szakdoga.map.models.fields.events.EventProcessor;

public class FieldProcessor implements FieldProcessorInterface {
	private EventProcessor eventProcessor;
	private GameControllerInterface gameController;
	
	public FieldProcessor(GameControllerInterface gameController) {
		this.gameController = gameController;
		eventProcessor = new EventProcessor(gameController);
	}
	
	public void processField(EventField eventField) {
		eventField.getEventList().stream().forEach(e -> eventProcessor.processEvent(e));
	}

	public void processField(EndGameField endGameField) {
		gameController.gameOver();		
	}

	public void processField(NoEventField noEventField) {
		// TODO Auto-generated method stub
		
	}

	public void processField(BlockField blockField) {
		// TODO Auto-generated method stub
		
	}

	public void processField(ShopField shopField) {
		// TODO Auto-generated method stub
		
	}
}
