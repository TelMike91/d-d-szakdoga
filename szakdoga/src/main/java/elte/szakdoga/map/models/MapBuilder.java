package elte.szakdoga.map.models;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.map.models.fields.BlockField;
import elte.szakdoga.map.models.fields.EndGameField;
import elte.szakdoga.map.models.fields.EventField;
import elte.szakdoga.map.models.fields.Field;
import elte.szakdoga.map.models.fields.ShopField;
import elte.szakdoga.map.models.fields.events.Event;

/**
 * A map builder class which creates a map with the settings. All fields are required and should not be used without setting them.
 */
public class MapBuilder {
    private int rows;
    private int cols;
    private List<Field> specialFields = new LinkedList<>();
    private List<Position> playerPositionList = new LinkedList<>();
    private EndGameField endGameField;

    /**
     * Sets the number of columns
     * @throws IllegalArgumentException If the cols is zero or negative.
     * @param cols
     */
    public void setColumns(int cols) {
        if(cols <= 0) {
            throw new IllegalArgumentException("Cols should be at least 1");
        }
        this.cols = cols;
    }

    /**
     * Sets the number of rows.
     * @throws IllegalArgumentException if the rows is zero or negative.
     * @param rows The number of rows
     */
    public void setRows(int rows) {
        if(rows <= 0) {
            throw new IllegalArgumentException("Rows should be at least 1");
        }
        this.rows = rows;
    }

    /**
     * Create the map with the number of rows and cols specified. Also add the special fields.
     * @return
     */
    public MyMap buildMap() {
    	if(endGameField != null)
    		return MyMap.createMapWithEndField(rows, cols, specialFields, endGameField);
    	else
    		return MyMap.createMap(rows, cols, specialFields);
    }

    /**
     * Adds a new block field to the specialfields.
     * @throws NullPointerException If the position is null.
     * @param position The position of the field.
     */
    public void addBlockField(Position position) {
        Objects.requireNonNull(position);
        specialFields.add(new BlockField(position));
    }

    /**
     * Add a new eventfield to specialFields we would like to add to the map.
     * @throws NullPointerException If the position or the eventList is null. eventList should be a 0 length list if we want to simulate a null.
     * @param position The position of the field.
     * @param eventList The list of events this field has.
     */
    public void addEventField(Position position, List<Event> eventList) {
        Objects.requireNonNull(position);
        Objects.requireNonNull(eventList);
        specialFields.add(new EventField(position, eventList));
    }

    public void addPlayerPosition(Position startingSpot) {
        playerPositionList.add(startingSpot);
    }

    public List<Position> getPlayerPositions() {
        return playerPositionList;
    }

	public void addEndGameField(Position position) {
		endGameField = new EndGameField(position);		
	}

	public void addShopField(Position startingSpot, List<Item> items) {
		specialFields.add(new ShopField(startingSpot, items));
	}

	public int getRows() {
		return rows;
	}
	
	public int getColumns() {
		return cols;
	}
}
