package elte.szakdoga.map.models.fields.events;

public interface EventProcessorInterface {
	public void processEvent(TrapEvent trapEvent);
	
	public void processEvent(ItemEvent itemEvent);
	
	public void processEvent(BattleEvent battleEvent);

	public void processEvent(Event e);
}
