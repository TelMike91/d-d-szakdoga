package elte.szakdoga.map.models.fields.events;

/**
 * The interface of the events. Every event should implement this.
 */
public interface Event {
	public String getMessage();

	public void visit(EventProcessorInterface eventProcessor);
}
