package elte.szakdoga.map.graphics;

public enum MyFieldType {
	EVENT {
		@Override
		protected String getName() {
			// TODO Auto-generated method stub
			return "event.png";
		}
	}, BATTLE {
		@Override
		protected String getName() {
			// TODO Auto-generated method stub
			return "grass.png";
		}
	}, NOTHING {
		@Override
		protected String getName() {
			// TODO Auto-generated method stub
			return "grass.png";
		}
	}, BLOCK {
		@Override
		protected String getName() {
			// TODO Auto-generated method stub
			return "rock.png";
		}
	}, PLAYER {
		@Override
		protected String getName() {
			// TODO Auto-generated method stub
			return "character.png";
		}
	}, SHOP {
		@Override
		protected String getName() {
			return "shop.png";
		}
	}, ENDFIELD {
		@Override
		protected String getName() {
			// TODO Auto-generated method stub
			return "endField.png";
		}
	};

	protected abstract String getName();
}
