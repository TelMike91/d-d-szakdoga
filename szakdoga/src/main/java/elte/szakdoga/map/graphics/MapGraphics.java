package elte.szakdoga.map.graphics;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.swing.JPanel;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.map.models.fields.EventField;
import elte.szakdoga.map.models.fields.Field;
import elte.szakdoga.narrator.models.ai.NarratorInterface;

/**
 * Ez a térkép grafikai reprezentációja.
 * 
 * @author Teleki Miklós
 *
 */
@SuppressWarnings("serial")
public class MapGraphics extends JPanel {
	private List<List<FieldGraphic>> fields;
	private int rows;
	private int cols;

	private MapGraphics(int rows, int cols) {
		setLayout(new GridLayout(rows, cols));
		this.rows = rows;
		this.cols = cols;
		fields = new ArrayList<>();
		for (int i = 0; i < rows; i++) {
			fields.add(new ArrayList<>());
			for (int m = 0; m < cols; m++) {
				FieldGraphic button = new FieldGraphic(new Position(i, m));
				fields.get(i).add(button);
				add(fields.get(i).get(m));
			}
		}
	}

	/**
	 * Factory method, amivel létrehozzuk a térképet a megadott paraméterekkel.
	 * 
	 * @param rows Sorok száma
	 * @param cols Oszlopok száma
	 * @return Egy térkép grafikai reprezentáció.
	 */
	public static MapGraphics createMapGraphics(int rows, int cols) {
		if(rows > 15)
			rows = 15;
		if(cols > 15)
			cols = 15;
		return new MapGraphics(rows, cols);
	}

	/**
	 * Frissítjük a térképet aszerint hogy mely mezők foglaltak.
	 * 
	 * @param positions A mezők listája, ami szerint frissítjük a térképet.
	 */
	public void updateMap(List<List<Field>> fields) {
		for (int i = 0; i < fields.size(); i++) {
			for (int m = 0; m < fields.get(i).size(); m++) {
				Field field = fields.get(i).get(m);
				MyFieldType fieldStatus = field.getFieldType();
				FieldGraphic currentGraphicField = this.fields.get(m).get(i);
				currentGraphicField.setPosition(field.getPosition());
				if(field.getSteppedPlayerHere() != null) {
					currentGraphicField.setPlayerHere(true);
				} else
					currentGraphicField.setPlayerHere(false);
				currentGraphicField.setGraphic(fieldStatus);
				currentGraphicField.repaint();
				currentGraphicField.revalidate();
			}
		}
		repaint();
		revalidate();
	}

	/**
	 * Hozzáadjuk a mezőkhöz a gomb listenereket.
	 * 
	 * @exception NullPointerException Ha a keyListener paraméter null.
	 * @param keyListener Az a listener, amit szeretnénk hozzáadni.
	 */
	public void addKeyListenerForButtons(KeyListener keyListener) {
		Objects.requireNonNull(keyListener);
		for (int i = 0; i < rows; i++) {
			for (int m = 0; m < cols; m++) {
				fields.get(i).get(m).addKeyListener(keyListener);
			}
		}
	}

	/**
	 * Hozzáadjuk az egérfigyelőt a gombokhoz.
	 * 
	 * @param mouseListener
	 */
	public void addMouseListenerForButtons(MouseListener mouseListener) {
		Objects.requireNonNull(mouseListener);
		for (int i = 0; i < rows; i++) {
			for (int m = 0; m < cols; m++) {
				fields.get(i).get(m).addMouseListener(mouseListener);
			}
		}
	}

	public void addNarratorMouseListenerForMap(NarratorInterface narrator) {
		Objects.requireNonNull(narrator);
		for (int i = 0; i < rows; i++) {
			for (int m = 0; m < cols; m++) {
				
				FieldGraphic currentFieldGraphic = fields.get(i).get(m);
				currentFieldGraphic.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						
					}
				});
				
			}
		}
	}

	public void addNewEventFieldListener(NarratorInterface narrator, EventField eventField) {
		for(int i = 0; i < rows; i++) {
			for(int m = 0; m < cols; m++) {
				FieldGraphic currentFieldGraphic = fields.get(i).get(m);
				currentFieldGraphic.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						eventField.setPosition(currentFieldGraphic.getPosition());
						narrator.addEvent(eventField);
						for(int i = 0; i < rows; i++) {
							for(int m = 0; m < cols; m++) {
								ActionListener[] listeners = fields.get(i).get(m).getActionListeners();
								while(listeners.length > 0) {
									fields.get(i).get(m).removeActionListener(listeners[0]);
									listeners = fields.get(i).get(m).getActionListeners();
								}
							}
						}
					}
				});
			}
		}
	}

	public void addDeleteEventListener(NarratorInterface narrator) {
		for(int i = 0; i < rows; i++) {
			for(int m = 0; m < cols; m++) {
				FieldGraphic currentFieldGraphic = fields.get(i).get(m);
				currentFieldGraphic.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						narrator.deleteField(currentFieldGraphic.getPosition());						
						for(int i = 0; i < rows; i++) {
							for(int m = 0; m < cols; m++) {
								ActionListener[] listeners = fields.get(i).get(m).getActionListeners();
								while(listeners.length > 0) {
									fields.get(i).get(m).removeActionListener(listeners[0]);
									listeners = fields.get(i).get(m).getActionListeners();
								}
							}
						}
					}
				});
			}
		}
	}
}
