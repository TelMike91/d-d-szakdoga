package elte.szakdoga.map.graphics;

import java.awt.Dimension;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;

import elte.szakdoga.actors.models.Position;

@SuppressWarnings("serial")
public class FieldGraphic extends JButton {
	private boolean playerHere;
	private Position position;
	
	public FieldGraphic() {

		super();
		this.setBorderPainted(true); 
		this.setContentAreaFilled(true); 
		this.setFocusPainted(true); 
		this.setOpaque(true);
		this.setMinimumSize(new Dimension(50,50));
		this.setSize(new Dimension(50,50));
		this.setPreferredSize(new Dimension(50, 50));
	}
	
	public FieldGraphic(Position position) {
		this();
		
		this.position = position;
	}

	public void setGraphic(MyFieldType fieldStatus) {
		while(this.getComponentCount() > 0)
			this.remove(0);
		if(playerHere) {
			ImageIcon test = new ImageIcon("assets/images/" + fieldStatus.getName());
			ImageIcon test2 = new ImageIcon("assets/images/" + MyFieldType.PLAYER.getName());
			JLabel test3 = new JLabel(test2);		
			this.add(test3);
			this.setIcon(test);
		} else {
			ImageIcon test = new ImageIcon("assets/images/" + fieldStatus.getName());
			this.setIcon(test);
		}
	}

	public boolean isPlayerHere() {
		return playerHere;
	}

	public void setPlayerHere(boolean playerHere) {
		this.playerHere = playerHere;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}
}
