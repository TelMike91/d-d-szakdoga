package elte.szakdoga.map.controller;

import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.Optional;

import javax.swing.JPanel;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.map.graphics.MapGraphics;
import elte.szakdoga.map.models.MyMap;
import elte.szakdoga.map.models.fields.EventField;
import elte.szakdoga.map.models.fields.Field;
import elte.szakdoga.narrator.models.ai.NarratorInterface;

/**
 * A térképet ezzel a controllerrel irányítjuk.
 * @author Teleki Miklós
 *
 */
public class MapController {
	private MyMap map;
    private final Optional<MapGraphics> graphics;
    
    public MapController(MyMap map, MapGraphics graphics) {
        this.map = map;
        this.graphics = Optional.ofNullable(graphics);
    }        
    
    /**
     * Ellenőrizzük hogy az adott pozícióra való lépés érvényes-e.
     * @param position A pozíció amit ellenőrizni szeretnénk.
     * @return Igaz, ha ráléphető, hamis különben.
     */
    public boolean checkIfMoveIsValid(Position position) {
        return map.checkIfMoveIsLegal(position);
    }

    /**
     * Frissítjük a térképet aszerint ahogy a játékosok állnak.
     * @param player A játékosok listája, ami szerint frissítjük a térképet.
     */
    public void updateMap(Player player) {
    	map.updatePlayerPosition(player);
        graphics.filter(f -> f != null).get().updateMap(map.getSmallView(player.getPosition()));
    }
    
    public List<List<Field>> getSmallView(Position position) {
    	return map.getSmallView(position);
    }

    public List<List<Field>> getFields() {
        return map.getFields();
    }

    public int getColumns() {
        return map.getColumns();
    }

    public int getRows() {
        return map.getRows();
    }

	public JPanel getMapView() {
		return graphics.orElse(MapGraphics.createMapGraphics(10, 10));
	}

	/**
	 * Hozzáadjuk a grafikához a megfelelő gomb lenyomásos listenereket.
	 * @param keyListener A gomb listener, amit szeretnénk hozzáadni a térképhez.
	 */
	public void addKeyListenerForButtons(KeyListener keyListener) {
		graphics.filter(f -> f != null).get().addKeyListenerForButtons(keyListener);
	}   
	
	/**
	 * Hozzáadjuk a grafikához a megfelelő egér eseményt.
	 * @param mouseListener Az egér felügyelő
	 */
	public void addMouseListenerForButton(MouseListener mouseListener) {
		graphics.filter(f -> f != null).get().addMouseListenerForButtons(mouseListener);
	}

	public void addNarratorMouseListenerForMap(NarratorInterface narrator) {
		graphics.filter(f -> f != null).get().addNarratorMouseListenerForMap(narrator);
		
	}

	public void updateMap(Player currentPlayer, Player[] requestPlayers) {
//		map.updatePlayerPositions(currentPlayer, requestPlayers); // Concurrent Mod exception ls
		graphics.filter(f -> f != null).get().updateMap(map.getSmallView(currentPlayer.getPosition()));
	}

	public MyMap getModel() {
		return map;
	}

	public void setMap(MyMap map) {
		this.map = map;		
	}

	public void addNewEventFieldListener(NarratorInterface narrator, EventField eventField) {
		graphics.filter(f -> f != null).get().addNewEventFieldListener(narrator, eventField);
	}

	public void addDeleteEventListener(NarratorInterface narrator) {
		graphics.filter(f -> f != null).get().addDeleteEventListener(narrator);		
	}

	public Field getField(Position currentPlayerPosition) {
		return map.getField(currentPlayerPosition).get();
	}
}
