package elte.szakdoga.graphics.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.game.controllers.GameService;
import elte.szakdoga.game.controllers.GameControllerInterface;
import elte.szakdoga.game.graphics.GamePanel;
import elte.szakdoga.game.graphics.MainWindow;
import elte.szakdoga.game.graphics.menus.ConfigurationMenu;
import elte.szakdoga.logging.graphics.LoggingFieldLogger;
import elte.szakdoga.xml.MapXMLParser;

public class StartGameActionListener implements ActionListener {

	private MainWindow window;
	private GameControllerInterface gameController;
	private ConfigurationMenu configuration;
	private static Logger logger = LoggerFactory.getLogger(StartGameActionListener.class);

	public StartGameActionListener(MainWindow mainWindow, ConfigurationMenu configuration) {
		this.window = mainWindow;
		this.configuration = configuration;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String value = configuration.getMapName();
		if (value == null) {
			LoggingFieldLogger.addText("No file selected!");
		} else if (!configuration.everythingFilled()) {
			LoggingFieldLogger.addText("One or more attribute is not filled!");
		} else {
			doAction(value);
		}

	}

	public void doAction(String mapName) {
		if(!configuration.isCalculatingDisabled()) {
			if (configuration.getPoints() < 0) {
				LoggingFieldLogger.addText("Points should be equal or more than 0!");
				return;
			}
		}
		if (configuration.getNarratorType() == null) {
			LoggingFieldLogger.addText("Please choose a narrator intelligence");
			return;
		}
		MapXMLParser mapXMLParser = MapXMLParser.createMapXMLParser(mapName + ".xml");
		if (mapXMLParser != null) {
			if (mapXMLParser.processXMLFile()) {
				gameController = new GameService(mapXMLParser.getMap(), window, configuration.getNarratorType());
				Player player = configuration.buildPlayer();				
				gameController.addPlayer(player);
				player.setPosition(mapXMLParser.getPlayerPositions().get(0));
				GamePanel gamePanel = GamePanel.createGamePanel(gameController);
				gameController.setGamePanel(gamePanel);
				gameController.changePanel(gamePanel);
				logger.info("Game started");
				gameController.pack();
			}
		} else {
			LoggingFieldLogger.addText("File was not found!");
		}
	}

}
