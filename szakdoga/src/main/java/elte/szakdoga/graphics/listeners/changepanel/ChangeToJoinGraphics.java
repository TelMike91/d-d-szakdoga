package elte.szakdoga.graphics.listeners.changepanel;

import java.awt.event.ActionEvent;

import elte.szakdoga.game.graphics.MainWindow;
import elte.szakdoga.game.graphics.menus.JoinGameGraphics;
import elte.szakdoga.graphics.listeners.ChangePanelButtonListener;

public class ChangeToJoinGraphics extends ChangePanelButtonListener{

	public ChangeToJoinGraphics(MainWindow mainWindow) {
		super(mainWindow);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		changePanel(new JoinGameGraphics(getMainWindow()));
	}

}
