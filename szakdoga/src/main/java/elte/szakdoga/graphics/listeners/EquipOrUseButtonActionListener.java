package elte.szakdoga.graphics.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import elte.szakdoga.items.controller.ItemControllerInterface;
import elte.szakdoga.items.graphics.ItemPanel;
import elte.szakdoga.items.models.defense.DefenseEquipment;
import elte.szakdoga.items.models.offense.OffenseEquipment;

/**
 * Az a listener, ami tárgyhasználatkor jön életbe. 
 * @author Teleki Miklós
 *
 */
public class EquipOrUseButtonActionListener implements ActionListener {
    private ItemControllerInterface itemController;
    private ItemPanel itemPanel;
    
    public EquipOrUseButtonActionListener(ItemControllerInterface itemController, ItemPanel itemPanel) {
         this.itemController = itemController;
         this.itemPanel = itemPanel;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        doAction();
    }

	public void doAction() {
		if (itemController.getCurrentItem().isEquipable()) {
            if (itemController.getCurrentItem() instanceof OffenseEquipment) {
                itemController.equipRightHand((OffenseEquipment) itemController.getCurrentItem());
            } else if (itemController.getCurrentItem() instanceof DefenseEquipment) {
                itemController.equipOn(itemController.getPlayer());
            }
        }
        if (itemController.getCurrentItem().isConsumable()) {
            itemController.useItem();           
            itemPanel.removeImage(itemController.getCurrentItemId());
        }
        itemController.setCurrentItemId(-1);
        itemPanel.setEquipOrUseButtonEnabled(false);
        itemPanel.repaint();
        itemPanel.revalidate();
	}
}
