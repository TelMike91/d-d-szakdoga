package elte.szakdoga.graphics.listeners.changepanel;

import java.awt.event.ActionEvent;

import elte.szakdoga.game.graphics.MainWindow;
import elte.szakdoga.game.graphics.menus.MainMenuGraphics;
import elte.szakdoga.graphics.listeners.ChangePanelButtonListener;

public class ChangeToMainMenuGraphics extends ChangePanelButtonListener {

	public ChangeToMainMenuGraphics(MainWindow mainWindow) {
		super(mainWindow);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		changePanel(new MainMenuGraphics(getMainWindow()));
		
	}

}
