package elte.szakdoga.graphics.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import elte.szakdoga.game.controllers.WindowlessGameController;
import elte.szakdoga.game.graphics.NarratorTypeGraphic.NarratorType;
import elte.szakdoga.game.graphics.menus.Configuration;
import elte.szakdoga.logging.graphics.LoggingFieldLogger;
import elte.szakdoga.map.models.MyMap;
import elte.szakdoga.network.MyClient;
import elte.szakdoga.network.MyServer;
import elte.szakdoga.network.Ports.Builder;
import elte.szakdoga.xml.MapXMLParser;

/**
 * Ez a listener felel azért, hogy amikor a host elindítja a játékot, akkor a szerver föltödjön adatokkal.
 * @author Teleki Miklós
 *
 */
public class HostStartGameListener implements ActionListener {
	private Configuration configuration;
	private MyServer server;
	private Logger logger = LoggerFactory.getLogger(HostStartGameListener.class);
	
	/**
	 * Létrehozzuk a listenert a megfelelő paraméterekkel.
	 * @param listOfNames A textfield referenciája, amitől lekérjük majd a pálya nevét. 
	 * @param server A szerver referenciája, ami ne legyen null.
	 */
	public HostStartGameListener(Configuration configuration, MyServer server) {		
		this.configuration = configuration;
		this.server = server;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		doAction(configuration.getMapName(), server);
	}

	
	public void doAction(String mapName, MyServer server) {
		MapXMLParser mapXMLParser = MapXMLParser.createMapXMLParser(mapName + ".xml");
        if (mapXMLParser != null) {
            mapXMLParser.processXMLFile();
            MyMap anotherMap = mapXMLParser.getMap();
            server.setMapData(anotherMap);
    		server.sendStartMessage(true);  
    		initBotNarrator(mapXMLParser);
        } else {
            LoggingFieldLogger.addText("File was not found!");
        }
	}

	private void initBotNarrator(MapXMLParser mapXMLParser) {
		if(configuration.getNarratorType() != NarratorType.None) {
			MyClient narratorAiClient = new MyClient(MyClient.LOCAL_ADDRESS, new Builder().buildDefault());        	
			try {
				logger.info("AI client initiation");
				narratorAiClient.connect();
				new WindowlessGameController(mapXMLParser.getMap(), configuration.getNarratorType(), narratorAiClient); 
			} catch (IOException e) {
				logger.error("AI client creation failed");
				logger.debug(e.getMessage());
				LoggingFieldLogger.addText("AI client creation failed");
			} 
		}
	}
}
