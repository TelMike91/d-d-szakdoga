package elte.szakdoga.graphics.listeners.changepanel;

import java.awt.event.ActionEvent;

import elte.szakdoga.game.graphics.MainWindow;
import elte.szakdoga.game.graphics.menus.SinglePlayerGraphics;
import elte.szakdoga.graphics.listeners.ChangePanelButtonListener;

public class ChangeToSinglePlayerGraphics extends ChangePanelButtonListener {

	public ChangeToSinglePlayerGraphics(MainWindow mainWindow) {
		super(mainWindow);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		changePanel(new SinglePlayerGraphics(getMainWindow()));
	}

}
