package elte.szakdoga.graphics.listeners.changepanel;

import java.awt.event.ActionEvent;

import elte.szakdoga.game.graphics.MainWindow;
import elte.szakdoga.game.graphics.menus.HostGameGraphics;
import elte.szakdoga.graphics.listeners.ChangePanelButtonListener;

public class ChangeToHostGraphics extends ChangePanelButtonListener{

	public ChangeToHostGraphics(MainWindow mainWindow) {
		super(mainWindow);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		HostGameGraphics graphics = new HostGameGraphics(getMainWindow());
		graphics.initClient(getMainWindow());
		changePanel(graphics);
	}

}
