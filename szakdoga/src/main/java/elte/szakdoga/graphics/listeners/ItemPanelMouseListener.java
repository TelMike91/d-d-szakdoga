package elte.szakdoga.graphics.listeners;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.BorderFactory;
import javax.swing.JButton;

import elte.szakdoga.items.controller.ItemControllerInterface;
import elte.szakdoga.items.graphics.ItemPanel.ItemImageLabel;
import elte.szakdoga.logging.graphics.LoggingFieldLogger;

/**
 * Az a listener, ami kiírja nekünk hogy milyen tárgyat használnánk, és aktiválja a fölvétel/használat gombot.
 * @author Teleki Miklós
 */
public class ItemPanelMouseListener extends MouseAdapter {
    private ItemImageLabel itemImageLabel;
    private static ItemImageLabel previous;
    private ItemControllerInterface itemController;
    private JButton buttonControll;

    public ItemPanelMouseListener(ItemImageLabel itemImageLabel, ItemControllerInterface itemController, JButton button) {
        this.itemImageLabel = itemImageLabel;
        this.itemController = itemController;
        this.buttonControll = button;
    }

    @Override
    public void mouseClicked(MouseEvent event) {
        if (event.getButton() == MouseEvent.BUTTON3) {
            LoggingFieldLogger.addText(itemImageLabel.getItem().toString());
        } else if (event.getButton() == MouseEvent.BUTTON1) {
            buttonControll.setEnabled(true);
            if (previous != null) {
                previous.setBorder(null);
            }
            itemController.setCurrentItem(itemImageLabel.getItem());
            itemImageLabel.setBorder(BorderFactory.createLineBorder(Color.RED));
            if (itemController.getCurrentItem().isEquipable()) {
                buttonControll.setText("Equip");
            } else {
                buttonControll.setText("Use");
            }
            previous = itemImageLabel;
            itemController.setCurrentItemId(itemImageLabel.getId());
        }
    }
}
