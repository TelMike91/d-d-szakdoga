package elte.szakdoga.graphics.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import elte.szakdoga.game.controllers.GameControllerInterface;

public class PlayerQuitsShopListener implements ActionListener {
	private GameControllerInterface gameController;	

	public PlayerQuitsShopListener(GameControllerInterface gameController) {
		this.gameController = gameController;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		gameController.playerLeavesShop();
	}

}
