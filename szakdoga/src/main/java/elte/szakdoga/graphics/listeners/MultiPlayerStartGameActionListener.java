package elte.szakdoga.graphics.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.esotericsoftware.kryonet.rmi.ObjectSpace;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.actors.models.player.NarratorPlayer;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.game.controllers.GameControllerForNarratorWithNetwork;
import elte.szakdoga.game.controllers.GameControllerInterface;
import elte.szakdoga.game.controllers.GameControllerWithNetwork;
import elte.szakdoga.game.graphics.GamePanel;
import elte.szakdoga.game.graphics.MainWindow;
import elte.szakdoga.game.graphics.PlayerTypeGraphics;
import elte.szakdoga.game.graphics.PlayerTypeGraphics.PlayerType;
import elte.szakdoga.game.graphics.menus.Configuration;
import elte.szakdoga.narrator.models.playercontrolled.NarratorWithNetwork;
import elte.szakdoga.network.MyClient;
import elte.szakdoga.network.rmi.ServerDataInterface;

public class MultiPlayerStartGameActionListener implements ActionListener {
	private MainWindow window;
	private GameControllerInterface gameController;
	private MyClient client;
	private PlayerTypeGraphics playerType;
	private Configuration configuration;
	private static Logger logger = LoggerFactory.getLogger(MultiPlayerStartGameActionListener.class);

	public MultiPlayerStartGameActionListener(MainWindow mainWindow, MyClient client, PlayerTypeGraphics playerType,
			Configuration configuration) {
		this.window = mainWindow;
		this.client = client;
		this.playerType = playerType;
		this.configuration = configuration;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		doAction();
	}

	public void doAction() {
		boolean startable = true;		
		if(configuration.isMapSelectable())
			startable = configuration.getMapName() != null && configuration.getMapName().length() > 0;
		if (startable) {
			int connectionId = client.getConnection().getID();
			logger.info("Getting server data");
			ServerDataInterface serverData = ObjectSpace.getRemoteObject(client.getConnection(), 1,
					ServerDataInterface.class);
			logger.info("Server data acquired");
			Player player = null;
			if (playerType.getSelectedOption() == PlayerType.NARRATOR)
				player = NarratorPlayer.createNarratorPlayer(new Position(1, 1));
			else
				player = configuration.buildPlayer();
			player.setPosition(new Position(1, 2));
			player.setId(connectionId);
			logger.info("Updating player");
			serverData.updatePlayer(player);
			logger.info("Player updated");
			GamePanel gamePanel;
			if (playerType.getSelectedOption() == PlayerType.NARRATOR) {
				logger.info("Narrator game initiating");
				gameController = new GameControllerForNarratorWithNetwork(new NarratorWithNetwork(client),
						serverData.getSmallMap(new Position(1, 1)), window, client);
				gameController.setDefaultPlayer(player);
				gamePanel = null;
//				gamePanel = GamePanel.createNarratorGamePanel(gameController, client);
			} else {
				logger.info("Player game initating");
				gameController = new GameControllerWithNetwork(serverData.getSmallMap(new Position(1, 1)), window,
						client);
				gameController.setDefaultPlayer(player);
				gamePanel = GamePanel.createGamePanel(gameController);
			}

			gameController.setGamePanel(gamePanel);
			gameController.changePanel(gamePanel);
			logger.info("Game started!");
			gameController.pack();
		}
	}
}
