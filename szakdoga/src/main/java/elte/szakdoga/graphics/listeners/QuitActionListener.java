package elte.szakdoga.graphics.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import elte.szakdoga.game.graphics.MainWindow;

public class QuitActionListener implements ActionListener {
	private static Logger logger = LoggerFactory.getLogger(QuitActionListener.class); 
	private MainWindow mainWindow;

	public QuitActionListener(MainWindow mainWindow) {
		this.mainWindow = mainWindow;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		logger.info("Game is being shutdown");
		mainWindow.dispose();		
		System.exit(0);
	}

}
