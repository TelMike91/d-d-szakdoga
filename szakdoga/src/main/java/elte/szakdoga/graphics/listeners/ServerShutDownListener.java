package elte.szakdoga.graphics.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import elte.szakdoga.network.MyServer;

public class ServerShutDownListener implements ActionListener {
	private MyServer server;
	private static Logger logger = LoggerFactory.getLogger(ServerShutDownListener.class);

	public ServerShutDownListener(MyServer server) {
		this.server = server;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		logger.info("Server shutdown initiated");
		server.shutDown();
		logger.info("Server shutdown completed");
	}

}
