package elte.szakdoga.graphics.listeners;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import elte.szakdoga.actors.graphics.PlayerPanel;
import elte.szakdoga.actors.models.Position.Direction;
import elte.szakdoga.game.controllers.GameControllerInterface;

/**
 * Ez az osztály felel a fontos gomblenyomásokért
 */
public class KeyBindings extends KeyAdapter {
	private final GameControllerInterface gameController;
	private PlayerPanel playerPanel;

	public KeyBindings(GameControllerInterface gameController, PlayerPanel playerPanel) {
		this.gameController = gameController;
		this.playerPanel = playerPanel;
	}

	public KeyBindings(GameControllerInterface gameController) {
		this.gameController = gameController;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		Direction direction = null;
		switch (e.getKeyCode()) {
		case KeyEvent.VK_UP:
			direction = Direction.UP;
			break;
		case KeyEvent.VK_DOWN:
			direction = Direction.DOWN;
			break;
		case KeyEvent.VK_LEFT:
			direction = Direction.LEFT;
			break;
		case KeyEvent.VK_RIGHT:
			direction = Direction.RIGHT;
			break;
		}
		if (direction != null && gameController.checkIfMoveIsValid(direction))
			gameController.movePlayer(direction);
		if (!gameController.checkIfGameOver()) {
			gameController.updatePanelIfPlayerIsInBattle();
			gameController.updatePanelIfPlayerIsInShop();
			gameController.update();
			if (playerPanel != null)
				playerPanel.updatePlayerPanel();
		} else {
			gameController.gameOver();
		}
	}
}
