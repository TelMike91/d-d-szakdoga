package elte.szakdoga.graphics.listeners;

import java.awt.event.ActionListener;

import javax.swing.JPanel;

import elte.szakdoga.game.graphics.MainWindow;

public abstract class ChangePanelButtonListener implements ActionListener {
	
	private MainWindow mainWindow;

	public ChangePanelButtonListener(MainWindow mainWindow) {
		this.setMainWindow(mainWindow);
	}

	public MainWindow getMainWindow() {
		return mainWindow;
	}

	public void setMainWindow(MainWindow mainWindow) {
		this.mainWindow = mainWindow;
	}
	

	protected void changePanel(JPanel otherPanel) {
		mainWindow.changePanel(otherPanel);
	}
}
