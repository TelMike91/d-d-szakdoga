package elte.szakdoga.graphics.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JTextField;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

import elte.szakdoga.actors.models.Position;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.game.graphics.MainWindow;
import elte.szakdoga.game.graphics.PlayerTypeGraphics;
import elte.szakdoga.game.graphics.menus.Configuration;
import elte.szakdoga.logging.graphics.LoggingFieldLogger;
import elte.szakdoga.network.MyClient;
import elte.szakdoga.network.Ports.Builder;
import elte.szakdoga.network.messages.PlayerDetailsMessage;
import elte.szakdoga.network.messages.StartGameMessage;

public class LocalGameJoinActionListener implements ActionListener {

	private static final String IP_REGEX = "\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}";
	private MainWindow mainWindow;
	private JButton connect;
	private PlayerTypeGraphics playerType;
	private JTextField connectToIp;
	private Configuration configuration;
	private JButton back;
	private static Logger logger = LoggerFactory.getLogger(LocalGameJoinActionListener.class);

	public LocalGameJoinActionListener(MainWindow mainWindow, JButton back, JTextField connectToIp, JButton connect,
			PlayerTypeGraphics playerType, Configuration configuration) {
		this.back = back;
		this.mainWindow = mainWindow;
		this.connectToIp = connectToIp;
		this.connect = connect;
		this.playerType = playerType;
		this.configuration = configuration;
	}

	private class Encapsulator {
		private MyClient client;

		public MyClient getClient() {
			return client;
		}

		public void setClient(MyClient client) {
			this.client = client;
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (configuration.everythingFilled()) {
			Encapsulator encapsulator = new Encapsulator();
			MyClient client = null;
			if (connectToIp.getText().matches(IP_REGEX)) {
				client = new MyClient(connectToIp.getText(), new Builder().buildDefault());
			} else {
				logger.debug("IP address format is not acceptable. Trying to connect 127.0.0.1(localhost)");
				LoggingFieldLogger.addText("IP address format is not good. Connecting to localhost");
				client = new MyClient(MyClient.LOCAL_ADDRESS, new Builder().buildDefault());
			}

			encapsulator.setClient(client);
			back.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {
					encapsulator.getClient().disconnect();
				}
			});

			Player player = configuration.buildPlayer();
			player.setPosition(new Position(1, 2));
			PlayerDetailsMessage message = new PlayerDetailsMessage();
			message.setPlayer(player);
			try {
				LoggingFieldLogger.addText("Connecting...");
				client.connect();
				LoggingFieldLogger.addText("Connection completed!");
				client.sendMessage(message);
				while (connect.getActionListeners().length > 0) {
					connect.removeActionListener(connect.getActionListeners()[0]);
				}
				client.addListener(new Listener() {
					@Override
					public void received(Connection connection, Object object) {
						if (object instanceof StartGameMessage) {
							connect.setText("Start");
							while (connect.getActionListeners().length > 0) {
								connect.removeActionListener(connect.getActionListeners()[0]);
							}
							connect.addActionListener(new MultiPlayerStartGameActionListener(mainWindow,
									encapsulator.getClient(), playerType, configuration));
						}

					}
				});
			} catch (IOException e1) {
				LoggingFieldLogger.addText("Error joining. Is the server running? Good ip address?");
				logger.debug(e1.getMessage());
			}
		} else {
			LoggingFieldLogger.addText("Create your character first!");
		}
	}

}
