package elte.szakdoga.graphics.listeners.changepanel;

import java.awt.event.ActionEvent;

import elte.szakdoga.game.graphics.MainWindow;
import elte.szakdoga.graphics.listeners.ChangePanelButtonListener;

public class ChangeToGamePanel extends ChangePanelButtonListener {
	public ChangeToGamePanel(MainWindow mainFrame) {
		super(mainFrame);
	}

	@Override
	public void actionPerformed(ActionEvent event) {		
		getMainWindow().revertPanel();
	}

}
