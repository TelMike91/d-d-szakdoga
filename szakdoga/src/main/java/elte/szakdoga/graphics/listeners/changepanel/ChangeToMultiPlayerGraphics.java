package elte.szakdoga.graphics.listeners.changepanel;

import java.awt.event.ActionEvent;

import elte.szakdoga.game.graphics.MainWindow;
import elte.szakdoga.game.graphics.menus.MultiPlayerGraphics;
import elte.szakdoga.graphics.listeners.ChangePanelButtonListener;

public class ChangeToMultiPlayerGraphics extends ChangePanelButtonListener {

	public ChangeToMultiPlayerGraphics(MainWindow mainWindow) {
		super(mainWindow);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		changePanel(new MultiPlayerGraphics(getMainWindow()));
	}

}
