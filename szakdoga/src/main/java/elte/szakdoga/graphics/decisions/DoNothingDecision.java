package elte.szakdoga.graphics.decisions;

import elte.szakdoga.messages.graphics.DecisionVisitor;
import elte.szakdoga.narrator.models.ai.NarratorInterface;

public class DoNothingDecision implements Decision {
	public DoNothingDecision() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String toString() {	
		return "Doing nothing";
	}

	@Override
	public void visit(DecisionVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public void doDecision(NarratorInterface narrator) {
		// TODO Auto-generated method stub
		
	}

}
