package elte.szakdoga.graphics.decisions;

import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.messages.graphics.DecisionVisitor;
import elte.szakdoga.narrator.models.ai.NarratorInterface;

public class BattleDecision implements Decision {
	private BattleField battleField;	
	
	public BattleDecision() { }
	
	public BattleDecision(BattleField battleField) {
		this.battleField = battleField;		
	}

	@Override
	public void doDecision(NarratorInterface narrator) {		
		narrator.putIntoBattle(battleField);
	}
	
	@Override
	public String toString() {	
		return "Battle";
	}

	@Override
	public void visit(DecisionVisitor visitor) {
		visitor.visit(this);
	}

}
