package elte.szakdoga.graphics.decisions;

import elte.szakdoga.messages.graphics.DecisionVisitor;
import elte.szakdoga.narrator.models.ai.NarratorInterface;

public interface Decision {
	public void doDecision(NarratorInterface narrator);

	public void visit(DecisionVisitor visitor);
}
