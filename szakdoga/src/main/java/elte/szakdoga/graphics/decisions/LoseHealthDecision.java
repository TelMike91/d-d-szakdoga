package elte.szakdoga.graphics.decisions;

import elte.szakdoga.messages.graphics.DecisionVisitor;
import elte.szakdoga.narrator.models.ai.NarratorInterface;

public class LoseHealthDecision implements Decision {	
	private int amount;
	
	public LoseHealthDecision() {
		// TODO Auto-generated constructor stub
	}
	
	public LoseHealthDecision(int amount) {		
		this.amount = amount;
	}

	@Override
	public void doDecision(NarratorInterface narrator) {
		narrator.removeHealth(amount);
	}

	@Override
	public void visit(DecisionVisitor visitor) {
		visitor.visit(this);
	}

}
