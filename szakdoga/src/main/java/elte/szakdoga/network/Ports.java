package elte.szakdoga.network;

public class Ports {
	private final int tcpPort;
	private final int udpPort;

	public static class Builder {
		private int tcpPort;
		private int udpPort;

		public Ports build() {
			return new Ports(this);
		}

		public Ports buildDefault() {
			tcpPort = 12000;
			udpPort = 11000;
			return new Ports(this);
		}

		public void setUdpPort(int udpPort) {
			this.udpPort = udpPort;
		}

		public void setTcpPort(int tcpPort) {
			this.tcpPort = tcpPort;
		}

	}

	public Ports(Builder builder) {
		this.tcpPort = builder.tcpPort;
		this.udpPort = builder.udpPort;
	}

	public int getTcpPort() {
		return tcpPort;
	}

	public int getUdpPort() {
		return udpPort;
	}

}
