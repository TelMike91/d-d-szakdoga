package elte.szakdoga.network;

import java.awt.List;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.rmi.ObjectSpace;

import elte.szakdoga.MyObserver;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.map.models.MyMap;
import elte.szakdoga.network.rmi.BattleInterface;
import elte.szakdoga.network.rmi.ServerData;
import elte.szakdoga.network.rmi.ServerDataInterface;

public class KryoRegister {
	public static void registerAll(Kryo kryo) {
		ObjectSpace.registerClasses(kryo);

		kryo.register(List.class);
		kryo.register(MyMap.class);
		kryo.register(Player.class);
		kryo.register(ServerData.class);
		kryo.register(Map.class);
		kryo.register(TreeMap.class);
		kryo.register(Collection.class);
		kryo.register(ServerDataInterface.class);
		kryo.register(BattleInterface.class);
		kryo.register( MyObserver.class);
	}
}
