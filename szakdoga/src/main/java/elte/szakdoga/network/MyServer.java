package elte.szakdoga.network;

import java.io.IOException;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Server;

import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.game.graphics.PlayerListModel;
import elte.szakdoga.map.models.MyMap;
import elte.szakdoga.network.listeners.ServerListener;
import elte.szakdoga.network.messages.LoggingMessage;
import elte.szakdoga.network.messages.StartGameMessage;
import elte.szakdoga.network.messages.UpdateBattle;
import elte.szakdoga.network.messages.UpdateMessage;
import elte.szakdoga.network.messages.responses.NarratorMessageResponse;

public class MyServer extends Connection implements Runnable {
	
	private Ports ports;
	private Server server;
	private ServerListener listener;
	public static final int BATTLE_FIELD_ID = 1;
	private static final int WRITE_BUFFER_SIZE = 80000;
	private static final int OBJECT_BUFFER_SIZE = 40000;

	public MyServer(Ports ports) {
		this.ports = ports;
		listener = new ServerListener(this);
	}

//	public void setServerData(ServerData serverData) {
//		listener.setServerData(serverData);
//	}

	/**
	 * Elindítja a szervert a
	 * 
	 * @throws IOException
	 */
	public void start() throws IOException {
		server = new Server(WRITE_BUFFER_SIZE, OBJECT_BUFFER_SIZE) {
			@Override
			protected Connection newConnection() {
				ConnectionManager manager = new ConnectionManager();
				listener.addConnection(manager);
				return manager;
			}
		};

		server.bind(ports.getTcpPort(), ports.getUdpPort());
		server.start();

		server.addListener(listener);

		Kryo kryo = server.getKryo();

		KryoRegister.registerAll(kryo);

		kryo.setReferences(true);
		kryo.setRegistrationRequired(false);
	}

	/**
	 * Elküldi a játék megkezdéséhez való üzenetet. A kliensek ezt megkapják, és
	 * elindíthajták a játékot, ha igaz az érték.
	 * 
	 * @param value Az érték, amit elküldünk az üzenetben.
	 */
	public void sendStartMessage(boolean value) {
		server.sendToAllTCP(new StartGameMessage(value));
	}

	@Override
	public void run() {
		try {
			start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Leállítja a szervert, és fölszabadítja az erőforrásokat.
	 */
	public void shutDown() {
		server.stop();
		try {
			server.dispose();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Elküldi egy narrárotr üzenetet mindenkinek, aki nem a narrátor.
	 * 
	 * @param connection A narrátor Connection-je.
	 * @param response   A narrátori üzenetet tartalmazza.
	 */
	public void sendNarratorMessageToEveryoneExceptNarrator(Connection connection, NarratorMessageResponse response) {
		server.sendToAllExceptTCP(connection.getID(), response);
	}

	public void sendLoggingMessage(int id, LoggingMessage request) {
		server.sendToTCP(id, request);
	}

	public void sendUpdateMessage(int id, MyMap map, Player[] players) {
		server.sendToTCP(id, new UpdateMessage(map, players));
	}

	public void setJoinListener(PlayerListModel playerModel) {
		listener.setJoinListener(playerModel);
	}

	public void setMapData(MyMap map) {
		listener.setMapData(map);
	}

	public void sendUpdateBattleMessage(UpdateBattle updateBattle) {
		server.sendToAllTCP(updateBattle);
	}
}
