package elte.szakdoga.network;

public class DisconnectMessage {
	private int id;
	public DisconnectMessage() {
	
	}
	
	public DisconnectMessage(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
}
