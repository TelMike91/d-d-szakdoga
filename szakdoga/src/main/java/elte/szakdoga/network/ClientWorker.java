package elte.szakdoga.network;

import com.esotericsoftware.kryonet.Connection;

import elte.szakdoga.network.listeners.ClientWorkerListener;
import elte.szakdoga.network.messages.PlayerDetailsMessage;
import elte.szakdoga.network.rmi.ServerDataInterface;

/**
 * A különböző klienseknek a kéréseit ez az osztály fogja földolgozni
 * @author Miki
 *
 */
public class ClientWorker {
	private int playerId;

	public ClientWorker(Connection connection, ServerDataInterface serverData) {
		connection.addListener(new ClientWorkerListener(serverData));
	}
	
	public void processInitialMessage(Connection connection, Object object) {
		if(object instanceof PlayerDetailsMessage) {
			PlayerDetailsMessage communication = (PlayerDetailsMessage) object;
			communication.getPlayer().setId(connection.getID());
			setPlayerId(connection.getID());
		}
	}

	public int getPlayerId() {
		return playerId;
	}

	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}
}
