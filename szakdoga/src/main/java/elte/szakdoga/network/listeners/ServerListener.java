package elte.szakdoga.network.listeners;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.rmi.ObjectSpace;

import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.game.graphics.PlayerListModel;
import elte.szakdoga.map.models.MyMap;
import elte.szakdoga.network.ClientWorker;
import elte.szakdoga.network.ConnectionManager;
import elte.szakdoga.network.DeleteEventMessage;
import elte.szakdoga.network.DisconnectMessage;
import elte.szakdoga.network.MyServer;
import elte.szakdoga.network.messages.AddFieldMessage;
import elte.szakdoga.network.messages.LoggingMessage;
import elte.szakdoga.network.messages.PlayerDetailsMessage;
import elte.szakdoga.network.messages.UpdatePlayer;
import elte.szakdoga.network.messages.requests.NarratorMessageRequest;
import elte.szakdoga.network.messages.responses.NarratorMessageResponse;
import elte.szakdoga.network.rmi.BattleInterface;
import elte.szakdoga.network.rmi.RemoteBattle;
import elte.szakdoga.network.rmi.ServerData;

public class ServerListener extends Listener {
	private Map<Connection, ClientWorker> workers;
	private PlayerListModel playerModel;
	private MyServer server;
	private ServerData serverData;	
	private ObjectSpace objectSpace;
	
	public ServerListener(MyServer server) {
		workers = new HashMap<>();
		objectSpace = new ObjectSpace();
		serverData = new ServerData(server);
		objectSpace.register(1, serverData);	
		BattleInterface remoteBattle = new RemoteBattle(server, serverData);
		objectSpace.register(3, remoteBattle );
		this.server = server;	
	}
	
	public void connected(Connection connection) {
		objectSpace.addConnection(connection);
		if(playerModel != null)
			playerModel.addPlayerName("" + connection.getID());
	}

	public void received(Connection connection, Object object) {
		if(object instanceof NarratorMessageRequest) {
			received(connection, (NarratorMessageRequest) object);		
		}
		if(object instanceof PlayerDetailsMessage) { 
			received(connection, (PlayerDetailsMessage) object);
		}	
		if(object instanceof LoggingMessage) {
			received(connection, (LoggingMessage) object);
		}
		if(object instanceof AddFieldMessage) {
			received(connection, (AddFieldMessage) object);
		}
		if(object instanceof DisconnectMessage) {
			received(connection, (DisconnectMessage) object);
		}
		if(object instanceof UpdatePlayer) {
			received(connection, (UpdatePlayer) object);
		}
		if(object instanceof DeleteEventMessage) {
			received(connection, (DeleteEventMessage) object);
		}				
	}
	
	public void received(Connection connection, DeleteEventMessage message) {
		serverData.deleteEventField(message.getPosition());
	}
	
	public void received(Connection connection, UpdatePlayer message) {
		serverData.updatePlayer(message.getPlayer());
	}
	
	public void received(Connection connection, DisconnectMessage message) {
		serverData.removePlayer(message.getId());
		if(serverData.getPlayerInfos().length == 0) {
			server.shutDown();
		}
	}
	
	public void received(Connection connection, AddFieldMessage request) {
		serverData.addField(request.getField());
	}
	
	
	public void received(Connection connection, LoggingMessage request) {		
		for(Player player : serverData.getPlayerInfos()) {
			server.sendLoggingMessage(player.getId(), request);	
		}		
	}
	
	public void received(Connection connection, PlayerDetailsMessage request) {
		if(!workers.containsKey(connection)) { 
			workers.put(connection, new ClientWorker(connection, serverData));
			workers.get(connection).processInitialMessage(connection, request);	
		}		
	}
		
	public void received(Connection connection, NarratorMessageRequest request) {
		NarratorMessageResponse response = new NarratorMessageResponse(request.getMessage());
		server.sendNarratorMessageToEveryoneExceptNarrator(connection, response);
	}
	
	public void disconnected (Connection connection) {
		ClientWorker worker = workers.get(connection);
		if(worker != null)
			serverData.removePlayer(worker.getPlayerId());
		workers.remove(connection);
	}

	public void setServerData(ServerData serverData) {
		Objects.requireNonNull(serverData);
		if(this.serverData == null)
			this.serverData = serverData;
		else {
			this.serverData.setMap(serverData.getMap());
			for(Player player : serverData.getPlayerInfos()) {
				this.serverData.updatePlayer(player);
			}
		}
	}

	public void setJoinListener(PlayerListModel playerModel) {
		this.playerModel = playerModel;
	}

	public void setMapData(MyMap map) {
		serverData.setMap(map);
	}

	public void addConnection(ConnectionManager manager) {
		serverData.addConnection(manager);
	}
}
