package elte.szakdoga.network.listeners;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

import elte.szakdoga.network.messages.PlayerDetailsMessage;
import elte.szakdoga.network.rmi.ServerDataInterface;

public class ClientWorkerListener extends Listener {
	private ServerDataInterface serverData;
	
	public ClientWorkerListener(ServerDataInterface serverData) {
		this.serverData = serverData;
	}
	
	@Override
	public void received(Connection connection, Object object) {
		if(object instanceof PlayerDetailsMessage) {
			received(connection, (PlayerDetailsMessage) object);					
		}		
	}
	
	public void received(Connection connection, PlayerDetailsMessage communication) {		
		if(serverData.isInOfBounds(communication.getPlayer())) {
			serverData.updatePlayer(communication.getPlayer());					
		}		
	}
}
