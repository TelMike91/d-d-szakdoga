package elte.szakdoga.network.listeners;

import java.util.LinkedList;
import java.util.List;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

import elte.szakdoga.MyObserver;
import elte.szakdoga.battle.models.observer.BattleObserver;
import elte.szakdoga.logging.graphics.LoggingFieldLogger;
import elte.szakdoga.network.MyClient;
import elte.szakdoga.network.messages.LoggingMessage;
import elte.szakdoga.network.messages.StartGameMessage;
import elte.szakdoga.network.messages.UpdateBattle;
import elte.szakdoga.network.messages.UpdateMessage;
import elte.szakdoga.network.messages.responses.NarratorMessageResponse;

public class MyClientListener extends Listener {
	private MyClient client;
	private List<MyObserver> observers = new LinkedList<>();
	private List<BattleObserver> battleObservers = new LinkedList<>();

	public MyClientListener(MyClient client) {
		this.client = client;
	}

	public void received(Connection connection, Object object) {
		if (object instanceof LoggingMessage)
			received(connection, (LoggingMessage) object);
		if (object instanceof StartGameMessage)
			received(connection, (StartGameMessage) object);
		if (object instanceof NarratorMessageResponse)
			received(connection, (NarratorMessageResponse) object);
		if (object instanceof UpdateMessage)
			received(connection, (UpdateMessage) object);
		if(object instanceof UpdateBattle) {
			received(connection, (UpdateBattle) object);
		}
	}
	
	public void received(Connection connection, UpdateBattle communication) {
		for(BattleObserver observer : battleObservers) {
			observer.update(communication);
		}
	}


	public void received(Connection connection, UpdateMessage communication) {
		for(MyObserver observer : observers) {
			observer.update(communication);
		}
	}

	public void received(Connection connection, NarratorMessageResponse communication) {
		client.addMessage(communication.getMessage());
	}

	public void received(Connection connection, LoggingMessage communication) {
		LoggingFieldLogger.addText(communication.getWho() + ": " + communication.getMessage());
	}

	public void received(Connection connection, StartGameMessage idMessage) {
		
	}

	public void addObserver(MyObserver observer) {
		this.observers.add(observer);
	}

}
