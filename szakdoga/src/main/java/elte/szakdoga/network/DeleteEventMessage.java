package elte.szakdoga.network;

import elte.szakdoga.actors.models.Position;

public class DeleteEventMessage {
	private Position position;
	
	
	
	public DeleteEventMessage(Position position) {
		super();
		this.position = position;
	}

	public DeleteEventMessage() {
		// TODO Auto-generated constructor stub
	}	

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}
	
	
}
