package elte.szakdoga.network.messages;

import elte.szakdoga.map.models.fields.Field;

public class AddFieldMessage {
	private Field field;
	
	public AddFieldMessage() {
		// TODO Auto-generated constructor stub
	}

	public AddFieldMessage(Field field) {
		super();
		this.field = field;
	}

	public Field getField() {
		return field;
	}

	public void setField(Field field) {
		this.field = field;
	}
	
	
	
}
