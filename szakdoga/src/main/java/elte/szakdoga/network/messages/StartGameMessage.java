package elte.szakdoga.network.messages;

public class StartGameMessage {
	private boolean start;
	
	public StartGameMessage() {	}
	
	public StartGameMessage(boolean value) {
		this.start = value;
	}

	public boolean isStart() {
		return start;
	}

	public void setStart(boolean start) {
		this.start = start;
	}
	
}
