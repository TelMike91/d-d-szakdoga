package elte.szakdoga.network.messages;

public class LoggingMessage {
	private String message;
	private String who;
	
	public LoggingMessage() {
		// TODO Auto-generated constructor stub
	}

	public LoggingMessage(String message, String who) {
		super();
		this.message = message;
		this.who = who;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getWho() {
		return who;
	}

	public void setWho(String who) {
		this.who = who;
	}
	
	
}
