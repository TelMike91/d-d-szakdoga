package elte.szakdoga.network.messages;

public class PlayerIdMessage {
	private int id;
	private static int idGeneration = 1;
	
	public PlayerIdMessage() {
		setId(idGeneration++);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
