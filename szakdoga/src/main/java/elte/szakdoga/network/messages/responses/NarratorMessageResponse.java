package elte.szakdoga.network.messages.responses;

import elte.szakdoga.messages.models.MyMessage;

public class NarratorMessageResponse {
	private MyMessage message;
	
	public NarratorMessageResponse() {
	}
	
	public NarratorMessageResponse(MyMessage message) {		
		this.message = message;
	}



	public MyMessage getMessage() {
		return message;
	}

	public void setMessage(MyMessage message) {
		this.message = message;
	}
	
	
}
