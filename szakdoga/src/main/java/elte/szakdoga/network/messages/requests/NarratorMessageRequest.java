package elte.szakdoga.network.messages.requests;

import elte.szakdoga.messages.models.MyMessage;

public class NarratorMessageRequest {
	private MyMessage message;

	public NarratorMessageRequest() {

	}

	public MyMessage getMessage() {
		return message;
	}

	public void setMessage(MyMessage message) {
		this.message = message;
	}

}
