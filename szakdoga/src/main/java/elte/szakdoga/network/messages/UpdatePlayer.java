package elte.szakdoga.network.messages;

import elte.szakdoga.actors.models.player.Player;

public class UpdatePlayer {
	private Player player;
	
	public UpdatePlayer() {
		// TODO Auto-generated constructor stub
	}

	public UpdatePlayer(Player player) {
		this.player = player;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}
	
	

}
