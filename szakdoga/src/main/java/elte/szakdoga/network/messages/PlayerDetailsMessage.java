package elte.szakdoga.network.messages;

import elte.szakdoga.actors.models.player.Player;

public class PlayerDetailsMessage {
	private Player player;

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}
}
