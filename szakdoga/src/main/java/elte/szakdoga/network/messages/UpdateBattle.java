package elte.szakdoga.network.messages;

import elte.szakdoga.battle.models.BattleField;

public class UpdateBattle {
	private BattleField battleField;
	
	public UpdateBattle() {
		// TODO Auto-generated constructor stub
	}

	public UpdateBattle(BattleField battleField) {
		super();
		this.battleField = battleField;
	}

	public BattleField getBattleField() {
		return battleField;
	}

	public void setBattleField(BattleField battleField) {
		this.battleField = battleField;
	}
	
	
}
