package elte.szakdoga.network.messages;

import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.map.models.MyMap;

public class UpdateMessage {
	
	private Player[] playerInfos;
	private MyMap map;

	public UpdateMessage() {
		// TODO Auto-generated constructor stub
	}

	public UpdateMessage(MyMap map, Player[] playerInfos) {
		this.map = map;
		this.playerInfos = playerInfos;
	}

	public Player[] getPlayerInfos() {
		return playerInfos;
	}

	public MyMap getMap() {
		return map;
	}

	public void setMap(MyMap map) {
		this.map = map;
	}
	
	

}
