package elte.szakdoga.network;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.List;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Listener.ThreadedListener;
import com.esotericsoftware.kryonet.rmi.ObjectSpace;

import elte.szakdoga.MyObserver;
import elte.szakdoga.actors.models.Position;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.map.models.fields.EventField;
import elte.szakdoga.messages.models.MyMessage;
import elte.szakdoga.network.listeners.MyClientListener;
import elte.szakdoga.network.messages.AddFieldMessage;
import elte.szakdoga.network.messages.LoggingMessage;
import elte.szakdoga.network.messages.PlayerDetailsMessage;
import elte.szakdoga.network.messages.UpdatePlayer;
import elte.szakdoga.network.messages.requests.NarratorMessageRequest;

/**
 * A kliens. 
 *
 */
public class MyClient implements Runnable {
	public static final String LOCAL_ADDRESS = "127.0.0.1";
	private Client client;
	private String address;
	private int tcpPort;
	private int udpPort;
	private MyClientListener listener;
	private List<MyMessage> messageList;
	private ObjectSpace objectSpace;

	public MyClient(String address, Ports ports) {
		client = new Client(80000, 20000);
		this.address = address;
		messageList = new LinkedList<>();
		this.tcpPort = ports.getTcpPort();
		this.udpPort = ports.getUdpPort();
		Kryo kryo = client.getKryo();
		
		KryoRegister.registerAll(kryo);
		
		kryo.setRegistrationRequired(false);
		kryo.setReferences(true);

		objectSpace = new ObjectSpace(client);
	}
	
	public void register(int id, Object object) {
		objectSpace.register(id, object);
	}

	public boolean connect() throws UnknownHostException, IOException {
		client.start();
		client.connect(1000, address, tcpPort, udpPort);
		listener = new MyClientListener(this);
		client.addListener(new ThreadedListener(listener));		
		return client.isConnected();
	}

	public void sendMessage(PlayerDetailsMessage message) {
		client.sendTCP(message);
	}

	@Override
	public void run() {

	}

	public void sendPlayerDetailsMessage(Player player) {
		PlayerDetailsMessage comm = new PlayerDetailsMessage();
		Player copyPlayer = new Player(player);
		comm.setPlayer(copyPlayer);
		sendMessage(comm);
	}

	public void addListener(Listener listener) {	
		client.addListener(new ThreadedListener(listener));
	}
	
	public void sendLoggingMessage(LoggingMessage loggingMessage) {
		client.sendTCP(loggingMessage);
	}

	public void sendNarratorMessage(MyMessage message) {
		NarratorMessageRequest request = new NarratorMessageRequest();
		request.setMessage(message);
		client.sendTCP(request);
	}

	public void addMessage(MyMessage message) {
		messageList.add(message);
	}

	public List<MyMessage> getMessages() {
		return messageList;
	}

	public void removeAllMessages() {
		while (!messageList.isEmpty()) {
			messageList.remove(0);
		}
	}

	public Connection getConnection() {
		return client;
	}

	public void addObserver(MyObserver observer) {
		listener.addObserver(observer);
	}

	public void sendAddEventMessage(EventField eventField) {
		client.sendTCP(new AddFieldMessage(eventField));
	}

	public void disconnect(int id) {
		client.sendTCP(new DisconnectMessage(id));
		client.stop();
		client.close();
	}

	public boolean isConnected() {
		return client.isConnected();
	}

	public void sendUpdatePlayerMessage(Player player) {
		client.sendTCP(new UpdatePlayer(player));
	}

	public void disconnect() {
		client.stop();
		client.close();
	}

	public void sendDeleteEventMessage(Position position) {
		client.sendTCP(new DeleteEventMessage(position));
	}
}
