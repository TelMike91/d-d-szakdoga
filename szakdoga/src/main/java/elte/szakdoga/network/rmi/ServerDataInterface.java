package elte.szakdoga.network.rmi;

import java.rmi.Remote;
import java.util.List;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.actors.models.Position;
import elte.szakdoga.actors.models.Position.Direction;
import elte.szakdoga.actors.models.enemy.Enemy;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.map.models.MyMap;

public interface ServerDataInterface extends Remote {

	MyMap getMap();

	void setMap(MyMap map);
	
	void move(int id, Direction direction);

	Player[] getPlayerInfos();

	void removePlayer(int playerId);

	void updatePlayer(Player player);

	boolean isInOfBounds(Player player);

	int getId();

	Player getPlayer(Player currentPlayer);

	void useItem(Player currentPlayer, Item currentItem);

	void updateBattleField(int id, List<Player> list, List<Enemy> enemyData);

	BattleField getBattleField(int id);
	
	void initBattleField(int id);

	Actor getCurrentActor(int id);

	void playerLeavesShop(int id);

	MyMap getSmallMap(Position position);

	void moveNarrator(int id, Direction direction);

	boolean buyItem(int id, Item item);
}