package elte.szakdoga.network.rmi;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.rmi.ObjectSpace;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.actors.models.Position;
import elte.szakdoga.actors.models.Position.Direction;
import elte.szakdoga.actors.models.enemy.Enemy;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.map.models.MyMap;
import elte.szakdoga.map.models.fields.Field;
import elte.szakdoga.map.models.fields.NoEventField;
import elte.szakdoga.network.ConnectionManager;
import elte.szakdoga.network.MyServer;

public class ServerData extends Connection implements ServerDataInterface {
	
	private transient static ServerGameController gameController = new ServerGameController();
	private transient static Map<Integer, Player> playerInfos = new TreeMap<>();
	private int id;
	private transient MyServer server;
	private transient List<ConnectionManager> connections;
	private final static transient Logger LOGGER = LoggerFactory.getLogger(ServerData.class);

	public ServerData(MyServer server) {
		ObjectSpace objectSpace = new ObjectSpace(server);		
		objectSpace.register(1, this);
		this.server = server;		
		connections = new LinkedList<>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.network.ServerDataInterface#getMap()
	 */
	@Override
	public MyMap getMap() {
		return gameController.getMap();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * elte.szakdoga.network.ServerDataInterface#setMap(elte.szakdoga.map.models.
	 * MyMap)
	 */
	@Override
	public void setMap(MyMap map) {
		gameController.setMap(map);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.network.ServerDataInterface#getPlayerInfos()
	 */
	@Override
	public Player[] getPlayerInfos() {
		return playerInfos.values().toArray(new Player[playerInfos.values().size()]);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.network.ServerDataInterface#removePlayer(int)
	 */
	@Override
	public void removePlayer(int playerId) {
		if (playerInfos.size() > 0) {
			Player player = playerInfos.remove(playerId);
			LOGGER.debug("Removed player with id : {}",playerId);
			LOGGER.debug("Remaining players: {}", Arrays.toString(getPlayerInfos()));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * elte.szakdoga.network.ServerDataInterface#updatePlayer(elte.szakdoga.actors.
	 * models.player.Player)
	 */
	@Override
	public void updatePlayer(Player player) {
		playerInfos.put(player.getId(), player);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * elte.szakdoga.network.ServerDataInterface#isInOfBounds(elte.szakdoga.actors.
	 * models.player.Player)
	 */
	@Override
	public boolean isInOfBounds(Player player) {
		return gameController.checkIfMoveIsLegal(player.getPosition());
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public void move(int id, Direction direction) {
		try {
			if (getBattleField(id) != null) {
				initBattleField(id);
			} else {
				Position newPosition = direction.move(playerInfos.get(id).getPosition());
				gameController.setDefaultPlayer(playerInfos.get(id));
				if (gameController.checkIfMoveIsLegal(newPosition)) {
					playerInfos.get(id).setPosition(newPosition);
				}			
				gameController.updatePlayerPositions(playerInfos.get(id), getPlayerInfos());			
				for (Integer player : playerInfos.keySet()) {
					server.sendUpdateMessage(player,
							gameController.getSmallMapFromPlayerPositon(getPlayer(player).getPosition()), getPlayerInfos());
				}
			}
		} catch(Exception exception) {
			LOGGER.debug("Move threw exception with parameters: {}, {}. Stack trace: ", id, direction, exception);
		}
	}

	@Override
	public Player getPlayer(Player currentPlayer) {
		return playerInfos.get(currentPlayer.getId());
	}

	@Override
	public void useItem(Player currentPlayer, Item currentItem) {
//		playerInfos.get(currentPlayer.getId()).useItem(currentItem);
	}

	@Override
	public void updateBattleField(int id, List<Player> list, List<Enemy> enemyData) {
//		playerInfos.get(id).getBattleField().setPlayers(list);
//		playerInfos.get(id).getBattleField().setEnemies(enemyData);
	}

	@Override
	public BattleField getBattleField(int id) {
//		return playerInfos.get(id).getBattleField();
		return null;
	}

	@Override
	public void initBattleField(int id) {
		BattleField battle = getBattleField(id);
		battle.init();
	}

	@Override
	public Actor getCurrentActor(int id) {
		BattleField battle = getBattleField(id);
//		battle.takeAction();
		return battle.getCurrentActor();
	}

	public Player getPlayer(int id) {
		return playerInfos.get(id);
	}

	public void addConnection(ConnectionManager manager) {
		connections.add(manager);
	}

	@Override
	public void playerLeavesShop(int id) {
		getPlayer(id).setShop(null);
	}

	public void addField(Field field) {
		gameController.setField(field);
	}

	@Override
	public MyMap getSmallMap(Position position) {
		return gameController.getSmallMapFromPlayerPositon(position);
	}

	@Override
	public void moveNarrator(int id, Direction direction) {
		Position newPosition = direction.move(playerInfos.get(id).getPosition());
		if (gameController.checkIfMoveIsLegal(newPosition))
			playerInfos.get(id).setPosition(newPosition);
		for (Integer player : playerInfos.keySet()) {
			server.sendUpdateMessage(player, gameController.getSmallMapFromPlayerPositon(playerInfos.get(player).getPosition()),
					getPlayerInfos());
		}
	}

	@Override
	public boolean buyItem(int id, Item item) {
		return playerInfos.get(id).buy(item);
	}

	public void deleteEventField(Position position) {
		gameController.setField(new NoEventField(position));
	}
}
