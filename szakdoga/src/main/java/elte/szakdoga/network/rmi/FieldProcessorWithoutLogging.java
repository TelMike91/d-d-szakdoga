package elte.szakdoga.network.rmi;

import elte.szakdoga.actors.controllers.PlayerControllerInterface;
import elte.szakdoga.game.controllers.GameControllerInterface;
import elte.szakdoga.map.models.fields.BlockField;
import elte.szakdoga.map.models.fields.EndGameField;
import elte.szakdoga.map.models.fields.EventField;
import elte.szakdoga.map.models.fields.FieldProcessorInterface;
import elte.szakdoga.map.models.fields.NoEventField;
import elte.szakdoga.map.models.fields.ShopField;
import elte.szakdoga.map.models.fields.events.EventProcessorInterface;

public class FieldProcessorWithoutLogging implements FieldProcessorInterface {

	private EventProcessorInterface eventProcessor;

	public FieldProcessorWithoutLogging(GameControllerInterface serverGameController,
			PlayerControllerInterface playerController) {
		this.eventProcessor = new EventProcesorWithoutLogging(playerController);
	}

	@Override
	public void processField(EventField eventField) {
		// TODO Auto-generated method stub

	}

	@Override
	public void processField(EndGameField endGameField) {
		// TODO Auto-generated method stub

	}

	@Override
	public void processField(NoEventField noEventField) {
		// TODO Auto-generated method stub

	}

	@Override
	public void processField(BlockField blockField) {
		// TODO Auto-generated method stub

	}

	@Override
	public void processField(ShopField shopField) {
		// TODO Auto-generated method stub

	}

}
