package elte.szakdoga.network.rmi;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.battle.controller.BattleSystem;
import elte.szakdoga.battle.controller.commands.AttackCommand;
import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.network.MyServer;
import elte.szakdoga.network.messages.LoggingMessage;
import elte.szakdoga.network.messages.UpdateBattle;

public class RemoteBattle implements BattleInterface {
	private ServerData serverData;
	private BattleSystem battleSystem;
	private MyServer server;

	public RemoteBattle(MyServer server, ServerData serverData) {
		super();
		this.server = server;
		this.serverData = serverData;
		battleSystem = new BattleSystem();
	}	

	@Override
	public boolean ownControl(int id) {
		Player player = serverData.getPlayer(id);
		BattleField battle = serverData.getBattleField(id);
		if(battle != null)
			return battle.getCurrentActor().equals(player);
		else
			return false;
	}

	@Override
	public void playerAttack(int id, Actor indexOfEnemy) {
		BattleField battle = serverData.getBattleField(id);
		if(battle != null) {
			for(Player player : battle.getFriends())
				server.sendLoggingMessage(player.getId(), new LoggingMessage("Attacking!", battle.getCurrentActor().getName()));
			battleSystem.executeAttackCommand(new AttackCommand(battle.getCurrentActor(), indexOfEnemy));
			
		}
	}

	@Override
	public void defend(int id) {
		BattleField battle = serverData.getBattleField(id);
		if(battle != null)
			for(Player player : battle.getFriends())
				server.sendLoggingMessage(player.getId(), new LoggingMessage("Defending!", battle.getCurrentActor().getName()));
//			battle.defend();		
	}

	@Override
	public void flee(int id) {
		BattleField battle = serverData.getBattleField(id);
		if(battle != null)
			for(Player player : battle.getFriends())
				server.sendLoggingMessage(player.getId(), new LoggingMessage("Trying to flee!", battle.getCurrentActor().getName()));
//			battle.flee();
		
	}
	
	@Override
	public void takeAction(int id) {
		BattleField battle = serverData.getBattleField(id);
		if(battle != null) {			
//			battle.takeAction();
			server.sendUpdateBattleMessage(new UpdateBattle(battle));
		}
	}

	@Override
	public void useItemOnEnemy(int id, Item currentItem, int index) {
		BattleField battle = serverData.getBattleField(id);
//		battle.useItem(battle.getEnemy(index), currentItem);
	}

	@Override
	public void useItemOnParty(int id, Item currentItem, int index) {
		BattleField battle = serverData.getBattleField(id);
//		battle.useItem(battle.getPlayer(index), currentItem);
	}	
}
