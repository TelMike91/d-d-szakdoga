package elte.szakdoga.network.rmi;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.items.models.Item;

public interface BattleInterface {
	boolean ownControl(int id);

	void playerAttack(int id, Actor enemy);

	void defend(int id);
	
	void flee(int id);
	
	void takeAction(int id);

	void useItemOnEnemy(int id, Item currentItem, int index);

	void useItemOnParty(int id, Item currentItem, int index);
}
