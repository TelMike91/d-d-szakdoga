package elte.szakdoga.network.rmi;

import elte.szakdoga.actors.controllers.PlayerController;
import elte.szakdoga.actors.models.Position;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.game.controllers.AbstractGameController;
import elte.szakdoga.map.controller.MapController;
import elte.szakdoga.map.models.MyMap;
import elte.szakdoga.map.models.fields.Field;

public class ServerGameController extends AbstractGameController {
	private static MyMap map;	
	
	public ServerGameController() {		
		super(null);
		playerController = new PlayerController();
		fieldProcessor = new FieldProcessorWithoutLogging(this, playerController);
		mapController = new MapController(ServerGameController.map, null);	
	}

	public MyMap getMap() {		
		return ServerGameController.map;
	}

	public void setMap(MyMap map) {
		ServerGameController.map = map;
		mapController.setMap(ServerGameController.map);
	}

	public boolean checkIfMoveIsLegal(Position position) {
		return ServerGameController.map.checkIfMoveIsLegal(position);
	}

	public void updatePlayerPositions(Player player, Player[] playerInfos) {
		ServerGameController.map.updatePlayerPositions(player, playerInfos);
		
	}

	public MyMap getSmallMapFromPlayerPositon(Position position) {
		return map.getSmallMapFromPlayerPositon(position);
	}

	public void setField(Field field) {
		ServerGameController.map.setField(field);
	}

	@Override
	public void gameOver() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updatePanelIfPlayerIsInBattle() {
		// TODO Auto-generated method stub
		
	}	

}
