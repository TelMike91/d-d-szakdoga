package elte.szakdoga.game.controllers;

import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.util.List;

import javax.swing.JPanel;

import elte.szakdoga.actors.controllers.NarratorPlayerController;
import elte.szakdoga.actors.graphics.PlayerPanel;
import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.actors.models.Position.Direction;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.game.graphics.GamePanel;
import elte.szakdoga.game.graphics.MainWindow;
import elte.szakdoga.game.graphics.NarratorTypeGraphic.NarratorType;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.map.controller.MapController;
import elte.szakdoga.map.graphics.MapGraphics;
import elte.szakdoga.map.models.MyMap;
import elte.szakdoga.map.models.fields.EventField;
import elte.szakdoga.narrator.controllers.NarratorController;
import elte.szakdoga.narrator.models.ai.NarratorAI;
import elte.szakdoga.narrator.models.ai.NarratorAIWithNetwork;
import elte.szakdoga.narrator.models.ai.NarratorInterface;
import elte.szakdoga.narrator.models.playercontrolled.Narrator;
import elte.szakdoga.network.MyClient;

public class WindowlessGameController extends AbstractGameController {
    private MyClient client;   
    
    public WindowlessGameController() {
    	super(null);
	}

	public WindowlessGameController(MyMap map, NarratorType narratorType, MyClient narratorAiClient) {
    	super(null);
    	this.client = narratorAiClient;
        this.playerController = new NarratorPlayerController(narratorAiClient);        
        mapController = new MapController(map, MapGraphics.createMapGraphics(map.getRows(), map.getColumns()));
        NarratorAI ai = new NarratorAI(map);
    	narratorController = new NarratorController(this, new NarratorAIWithNetwork(ai, client));
    	ai.setIntelligence(narratorType.getIntelligence().getAction(narratorController.getNarrator()));            
        narratorController.start(); 
	}


	public NarratorController getNarratorController() {
    	return narratorController;
    }
    
    public void gameOver() {
    	
    }
    
    public MainWindow getMainFrame() {
    	return mainFrame;
    }
    

    /**
     * Frissíti a térképet a játékosok pozíciójának függvényében és a térkép helyzetében.
     */
    public void update() {    
//    	ServerDataInterface test = ObjectSpace.getRemoteObject(client.getConnection(), 1, ServerDataInterface.class);
//        mapController.updateMap(playerController.getCurrentPlayer(), test.getPlayerInfos());          
    }

    /**
     * Hozzáadjuk a játékosokat a pozíciók listájából.
     * @param positionList A játékosok pozícióinak a listája.
     */
    public void addPlayer(Player player) {
      
    }   

    /**
     * Ellenőrizzük hogy az adott írányba érvényes lépést tudunk tenni. 
     * @param direction Az irány amerre menjen a játékos.
     * @return Igaz ha érvényes, hamis különben.
     */
    public boolean checkIfMoveIsValid(Direction direction) {
        return mapController.checkIfMoveIsValid(direction.move(playerController.getCurrentPlayerPosition()));
    }

    /**
     * Mozgatjuk a játékost a térképen.
     * @param direction Az irány amerre mozog a játékos.
     */
    public void movePlayer(Direction direction) {  
        playerController.movePlayer(direction);        
        client.sendPlayerDetailsMessage(playerController.getCurrentPlayer());
    }
    
    public void addItem(Item item) {
        playerController.addItem(item);
    }
    
    public void useItem(Item item) {
        playerController.useItem(item);
    }
    
    public void useItem(Item item, Actor actor) {
        playerController.useItem(item, actor);
    }                   
    
    public Player getCurrentPlayer() {
        return playerController.getCurrentPlayer();
    }

	public JPanel getMapView() {		
		return mapController.getMapView();
	}

	public void addKeyListenerForButtons(KeyListener keyListener) {
		mapController.addKeyListenerForButtons(keyListener);
	}

	/**
	 * Beállítjuk a láthatóságot a JFrame-nek
	 * @param visibility Logikai érték hogy látható-e a főablak, vagy nem.
	 */
	public void setVisible(boolean visibility) {
//		this.mainFrame.setVisible(visibility);
	}

	/**
	 * Meghívja a pack() függvényt a JFrame-nek.
	 */
	public void pack() {
//		this.mainFrame.pack();
	}

	/**
	 * Változtatjuk a főablak paneljét, hogy például harcot mutassunk, vagy térképet.
	 * @param otherPanel Egy másik JPanle amit szeretnénk megjeleníteni a főablakban.
	 */
	public void changePanel(JPanel otherPanel) {
		this.mainFrame.changePanel(otherPanel);
	}

	/**
	 * Frissítjük a fő panelt az ablakban azoknak a játékosoknak, akik éppen harcban vannak.
	 */
	public void updatePanelIfPlayerIsInBattle() {
	
	}

	public void changeToMapView() {
//		mainFrame.revertPanel();
	}

	public void setGamePanel(GamePanel gamePanel) {	
//		mainFrame.setGamePanel(gamePanel);
	}

	public void addMouseListenerForButtons(MouseListener mouseListener) {
		mapController.addMouseListenerForButton(mouseListener);
	}

	public void addNarratorMouseListenerForMap(Narrator narrator) {
		mapController.addNarratorMouseListenerForMap(narrator);
		
	}


	@Override
	public void addNarratorMouseListenerForMap(NarratorInterface narrator) { }


	@Override
	public void setDefaultPlayer(Player player) { }


	@Override
	public PlayerPanel getPlayerView() {
		return null;
	}


	@Override
	public void showShop(List<Item> purchasableItems) {	}


	@Override
	public void updatePanelIfPlayerIsInShop() {	}


	@Override
	public void playerLeavesShop() {}


	@Override
	public void addNewEventFieldListener(NarratorInterface narrator, EventField eventField) {}


	@Override
	public void buy(Item item) {
	
	}
}
