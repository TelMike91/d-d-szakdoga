package elte.szakdoga.game.controllers;

import elte.szakdoga.actors.controllers.PlayerController;
import elte.szakdoga.battle.controller.BattleController;
import elte.szakdoga.battle.controller.BattleSystem;
import elte.szakdoga.database.DatabaseConnection;
import elte.szakdoga.game.graphics.MainWindow;
import elte.szakdoga.game.graphics.NarratorTypeGraphic.NarratorType;
import elte.szakdoga.game.graphics.menus.MainMenuGraphics;
import elte.szakdoga.logging.graphics.LoggingFieldLogger;
import elte.szakdoga.map.controller.MapController;
import elte.szakdoga.map.graphics.MapGraphics;
import elte.szakdoga.map.models.MyMap;
import elte.szakdoga.map.models.fields.FieldProcessor;
import elte.szakdoga.narrator.controllers.NarratorController;
import elte.szakdoga.narrator.models.ai.NarratorAI;
import elte.szakdoga.narrator.models.ai.NarratorInterface;

/**
 * Összecsatlakoztatja a kontrolleket, és így alakítja a játékot.
 */
public class GameService extends AbstractGameController {    

    public GameService(MyMap myMap, MainWindow window, NarratorType narratorType) {
    	super(window);
        this.playerController = new PlayerController();
		fieldProcessor = new FieldProcessor(this);
        mapController = new MapController(myMap, MapGraphics.createMapGraphics(myMap.getRows(), myMap.getColumns()));        
    	NarratorAI ai = new NarratorAI(myMap);
    	ai.setIntelligence(narratorType.getIntelligence().getAction(ai));
    	narratorController = new NarratorController(this, ai);        	        
        messageListener = new MyMessageListener(narratorController, this, window);
        this.battleController = new BattleController(new BattleSystem(), this);
	}
    
    /* (non-Javadoc)
	 * @see elte.szakdoga.controllers.GameControllerInterface#gameOver()
	 */
    @Override
	public void gameOver() {
    	messageListener.setGameOver(true);
    	LoggingFieldLogger.addText("Game over");
    	DatabaseConnection.getInstance().writePlayer(playerController.getCurrentPlayer());
    	mainFrame.changePanel(new MainMenuGraphics(mainFrame));
    }
    
    /* (non-Javadoc)
	 * @see elte.szakdoga.controllers.GameControllerInterface#updateMap()
	 */
    @Override
	public void update() {
        mapController.updateMap(playerController.getCurrentPlayer());        
    }
      
	/* (non-Javadoc)
	 * @see elte.szakdoga.controllers.GameControllerInterface#updatePanelIfPlayerIsInBattle()
	 */
	@Override
	public void updatePanelIfPlayerIsInBattle() {
		if(battleController.isBattleInitiated()) {
			battleController.initBattleField(battleController.getBattle());
			battleController.getBattle().addPlayer(playerController.getCurrentPlayer());
			mainFrame.changePanel(battleController.getView());
			battleController.update();
		}		
	}


	/* (non-Javadoc)
	 * @see elte.szakdoga.controllers.GameControllerInterface#addNarratorMouseListenerForMap(elte.szakdoga.narrator.models.ai.NarratorInterface)
	 */
	@Override
	public void addNarratorMouseListenerForMap(NarratorInterface narrator) {
		mapController.addNarratorMouseListenerForMap(narrator);
		
	}
}
