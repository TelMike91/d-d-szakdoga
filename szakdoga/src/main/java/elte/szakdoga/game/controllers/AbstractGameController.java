package elte.szakdoga.game.controllers;

import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.util.List;

import javax.swing.JPanel;

import elte.szakdoga.actors.controllers.PlayerControllerInterface;
import elte.szakdoga.actors.graphics.PlayerPanel;
import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.actors.models.Position.Direction;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.battle.controller.BattleControllerInterface;
import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.game.graphics.GamePanel;
import elte.szakdoga.game.graphics.MainWindow;
import elte.szakdoga.game.graphics.NarratorTypeGraphic.NarratorType;
import elte.szakdoga.game.graphics.ShopPanel;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.logging.graphics.LoggingFieldLogger;
import elte.szakdoga.map.controller.MapController;
import elte.szakdoga.map.models.MyMap;
import elte.szakdoga.map.models.fields.EventField;
import elte.szakdoga.map.models.fields.FieldProcessorInterface;
import elte.szakdoga.narrator.controllers.NarratorController;
import elte.szakdoga.narrator.models.ai.NarratorInterface;

public abstract class AbstractGameController implements GameControllerInterface {
	protected MapController mapController;
	protected PlayerControllerInterface playerController;
	protected NarratorController narratorController;
	protected MyMessageListener messageListener;
	protected MainWindow mainFrame;
	protected FieldProcessorInterface fieldProcessor;
	protected BattleControllerInterface battleController;
	
	@Override
	public void addDeleteEventListener(NarratorInterface narrator) {
		
	}
	
	public AbstractGameController(MainWindow window) {
		mainFrame = window;
		if(mainFrame != null)
			mainFrame.setTitle("Szakdoga");
	}

	@Override
	public boolean checkIfGameOver() {
		Player player  = playerController.getCurrentPlayer();
		boolean gameOver = player.getHP() <= 0;			
		return gameOver;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * elte.szakdoga.controllers.GameControllerInterface#getNarratorController()
	 */
	@Override
	public NarratorController getNarratorController() {
		return narratorController;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.controllers.GameControllerInterface#getMainFrame()
	 */
	@Override
	public MainWindow getMainFrame() {
		return mainFrame;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * elte.szakdoga.controllers.GameControllerInterface#addPlayer(java.util.List)
	 */
	@Override
	public void addPlayer(Player player) {
		playerController.setPlayer(player);
		narratorController.setPlayer(player);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * elte.szakdoga.controllers.GameControllerInterface#checkIfMoveIsValid(elte.
	 * szakdoga.actors.models.Position.Direction)
	 */
	@Override
	public boolean checkIfMoveIsValid(Direction direction) {
		return mapController.checkIfMoveIsValid(direction.move(playerController.getCurrentPlayerPosition()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * elte.szakdoga.controllers.GameControllerInterface#movePlayer(elte.szakdoga.
	 * actors.models.Position.Direction)
	 */
	@Override
	public void movePlayer(Direction direction) {
		playerController.movePlayer(direction);
		mapController.getField(playerController.getCurrentPlayerPosition()).visit(fieldProcessor);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * elte.szakdoga.controllers.GameControllerInterface#addItem(elte.szakdoga.items
	 * .models.Item)
	 */
	@Override
	public void addItem(Item item) {
		playerController.addItem(item);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * elte.szakdoga.controllers.GameControllerInterface#useItem(elte.szakdoga.items
	 * .models.Item)
	 */
	@Override
	public void useItem(Item item) {
		playerController.useItem(item);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.controllers.GameControllerInterface#getMapView()
	 */
	@Override
	public JPanel getMapView() {
		return mapController.getMapView();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.controllers.GameControllerInterface#setVisible(boolean)
	 */
	@Override
	public void setVisible(boolean visibility) {
		this.mainFrame.setVisible(visibility);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * elte.szakdoga.controllers.GameControllerInterface#useItem(elte.szakdoga.items
	 * .models.Item, elte.szakdoga.actors.models.Actor)
	 */
	@Override
	public void useItem(Item item, Actor actor) {
		playerController.useItem(item, actor);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.controllers.GameControllerInterface#getCurrentPlayer()
	 */
	@Override
	public Player getCurrentPlayer() {
		return playerController.getCurrentPlayer();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * elte.szakdoga.controllers.GameControllerInterface#addKeyListenerForButtons(
	 * java.awt.event.KeyListener)
	 */
	@Override
	public void addKeyListenerForButtons(KeyListener keyListener) {
		mapController.addKeyListenerForButtons(keyListener);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.controllers.GameControllerInterface#pack()
	 */
	@Override
	public void pack() {
		this.mainFrame.pack();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * elte.szakdoga.controllers.GameControllerInterface#changePanel(javax.swing.
	 * JPanel)
	 */
	@Override
	public void changePanel(JPanel otherPanel) {
		this.mainFrame.changePanel(otherPanel);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.controllers.GameControllerInterface#changeToMapView()
	 */
	@Override
	public void changeToMapView() {
		mainFrame.revertPanel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * elte.szakdoga.controllers.GameControllerInterface#setGamePanel(elte.szakdoga.
	 * graphics.GamePanel)
	 */
	@Override
	public void setGamePanel(GamePanel gamePanel) {
		mainFrame.setGamePanel(gamePanel);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * elte.szakdoga.controllers.GameControllerInterface#addMouseListenerForButtons(
	 * java.awt.event.MouseListener)
	 */
	@Override
	public void addMouseListenerForButtons(MouseListener mouseListener) {
		mapController.addMouseListenerForButton(mouseListener);
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.controllers.GameControllerInterface#setDefaultPlayer(elte.
	 * szakdoga.actors.models.player.Player)
	 */
	@Override
	public void setDefaultPlayer(Player player) {
		playerController.setPlayer(player);
	}

	@Override
	public void addNewEventFieldListener(NarratorInterface narrator, EventField eventField) {
		mapController.addNewEventFieldListener(narrator, eventField);
	}

	@Override
	public void updatePanelIfPlayerIsInShop() {
		Player player = playerController.getPlayerInShop();
		if(player != null) {
			showShop(player.getShop().getPurchasableItems());
		}
	}

	@Override
	public void playerLeavesShop() {
		playerController.playerLeavesShop();
	}
	
	@Override
	public void showShop(List<Item> purchasableItems) {
		mainFrame.changePanel(new ShopPanel(purchasableItems, this));
	}
	
	/* (non-Javadoc)
	 * @see elte.szakdoga.controllers.GameControllerInterface#getPlayerView()
	 */
	@Override
	public PlayerPanel getPlayerView() {		
		return playerController.getPlayerView();
	}
	
	@Override
	public void addNarratorMouseListenerForMap(NarratorInterface narrator) {
		mapController.addNarratorMouseListenerForMap(narrator);
	}
	
	public static AbstractGameController createSimpleGameController(MyMap map, MainWindow mainWindow, NarratorType narratorType) {
		AbstractGameController gameController = new GameService(map, mainWindow, narratorType);
		gameController.narratorController.start();
		new Thread(gameController.messageListener).start(); 
		return gameController;
	}
	
	@Override
	public void buy(Item item) {
		boolean success = playerController.buyItem(item);
		if(success) {
			LoggingFieldLogger.addText("Purchase successful");
		} else {
			LoggingFieldLogger.addText("Purchase failed! Not enough gold");
		}
		
	}	
	
	@Override
	public void setBattle(BattleField battleField) {
		battleController.initBattleField(battleField);
	}
	
	@Override
	public void damage(int damage) {
		playerController.damage(damage);
	}
}
