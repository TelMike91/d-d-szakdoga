package elte.szakdoga.game.controllers;

import elte.szakdoga.game.graphics.MainWindow;
import elte.szakdoga.messages.graphics.MessageVisitor;
import elte.szakdoga.narrator.controllers.NarratorController;

/**
 * Ez az osztály figyeli hogy kaptak-e a játékosok üzenetet. Ha igen akkor megjeleníti őket.
 * Egy külön szálon fut, úgyhogy Thread-safety-re figyelni kell.
 * @author Miki
 *
 */
public class MyMessageListener implements Runnable {
	private GameControllerInterface playerController;
	private MessageVisitor messageVisitor;
	private boolean gameOver = false;

	public MyMessageListener(NarratorController controller, GameControllerInterface playerController, MainWindow window) {
		this.playerController = playerController;
		this.messageVisitor = new MessageVisitor(controller, window);
	}

	@Override
	public void run() {
		while(!isGameOver()) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
//			if(playerController.gotMessages()) {	
//				for(MyMessage message : playerController.getMessages()) {
//					if(playerController.getPlayerInBattle() != null) {
//						if(message.isVisibleInBattle())
//							message.visit(messageVisitor);
//					} else {
//						message.visit(messageVisitor);
//					}
//				}				
//			}
		}
	}

	public boolean isGameOver() {
		return gameOver;
	}

	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}
	
}
