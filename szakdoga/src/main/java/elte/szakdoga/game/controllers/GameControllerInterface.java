package elte.szakdoga.game.controllers;

import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.util.List;

import javax.swing.JPanel;

import elte.szakdoga.actors.graphics.PlayerPanel;
import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.actors.models.Position.Direction;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.game.graphics.GamePanel;
import elte.szakdoga.game.graphics.MainWindow;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.map.models.fields.EventField;
import elte.szakdoga.narrator.controllers.NarratorController;
import elte.szakdoga.narrator.models.ai.NarratorInterface;

public interface GameControllerInterface {
	
	boolean checkIfGameOver();

	NarratorController getNarratorController();

	void gameOver();

	MainWindow getMainFrame();

	/**
	 * Frissíti a térképet a játékosok pozíciójának függvényében és a térkép helyzetében.
	 */
	void update();

	/**
	 * Hozzáadjuk a játékosokat a pozíciók listájából.
	 * @param positionList A játékosok pozícióinak a listája.
	 */
	void addPlayer(Player player);

	/**
	 * Ellenőrizzük hogy az adott írányba érvényes lépést tudunk tenni. 
	 * @param direction Az irány amerre menjen a játékos.
	 * @return Igaz ha érvényes, hamis különben.
	 */
	boolean checkIfMoveIsValid(Direction direction);

	/**
	 * Mozgatjuk a játékost a térképen.
	 * @param direction Az irány amerre mozog a játékos.
	 */
	void movePlayer(Direction direction);

	void addItem(Item item);

	void useItem(Item item);

	void useItem(Item item, Actor actor);

	Player getCurrentPlayer();

	JPanel getMapView();

	void addKeyListenerForButtons(KeyListener keyListener);

	/**
	 * Beállítjuk a láthatóságot a JFrame-nek
	 * @param visibility Logikai érték hogy látható-e a főablak, vagy nem.
	 */
	void setVisible(boolean visibility);

	/**
	 * Meghívja a pack() függvényt a JFrame-nek.
	 */
	void pack();

	/**
	 * Változtatjuk a főablak paneljét, hogy például harcot mutassunk, vagy térképet.
	 * @param otherPanel Egy másik JPanle amit szeretnénk megjeleníteni a főablakban.
	 */
	void changePanel(JPanel otherPanel);

	/**
	 * Frissítjük a fő panelt az ablakban azoknak a játékosoknak, akik éppen harcban vannak.
	 */
	void updatePanelIfPlayerIsInBattle();

	void changeToMapView();

	void setGamePanel(GamePanel gamePanel);

	void addMouseListenerForButtons(MouseListener mouseListener);

	void addNarratorMouseListenerForMap(NarratorInterface narrator);

	void setDefaultPlayer(Player player);

	PlayerPanel getPlayerView();

	void showShop(List<Item> purchasableItems);

	void updatePanelIfPlayerIsInShop();

	void playerLeavesShop();

	void addNewEventFieldListener(NarratorInterface narrator, EventField eventField);

	void buy(Item item);

	void addDeleteEventListener(NarratorInterface narratorController);

	void setBattle(BattleField field);

	void damage(int damage);
}