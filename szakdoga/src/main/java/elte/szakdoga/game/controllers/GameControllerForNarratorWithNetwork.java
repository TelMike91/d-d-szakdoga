package elte.szakdoga.game.controllers;

import elte.szakdoga.MyObserver;
import elte.szakdoga.actors.controllers.MultiPlayerController;
import elte.szakdoga.game.graphics.MainWindow;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.map.controller.MapController;
import elte.szakdoga.map.graphics.MapGraphics;
import elte.szakdoga.map.models.MyMap;
import elte.szakdoga.narrator.controllers.NarratorController;
import elte.szakdoga.narrator.models.ai.NarratorInterface;
import elte.szakdoga.network.MyClient;
import elte.szakdoga.network.messages.UpdateMessage;

public class GameControllerForNarratorWithNetwork extends AbstractGameController implements MyObserver {
	public GameControllerForNarratorWithNetwork(NarratorInterface narrator, MyMap map, MainWindow window, MyClient client) {
		super(window);
		this.playerController = new MultiPlayerController(client);		
		mapController = new MapController(map, MapGraphics.createMapGraphics(map.getRows(), map.getColumns()));
		narratorController = new NarratorController(this, narrator);
		messageListener = new MyMessageListener(narratorController, this, window);
		new Thread(messageListener).start();		
		client.addObserver(this);
		update();
	}

	public void gameOver() {

	}
	
	@Override
	public void addDeleteEventListener(NarratorInterface narrator) {
		mapController.addDeleteEventListener(narrator);
	}

	@Override
	public void updatePanelIfPlayerIsInBattle() {}

	@Override
	public void update(UpdateMessage communication) {
		mapController.setMap(communication.getMap());
		narratorController.setMap(communication.getMap());
		mapController.updateMap(getCurrentPlayer(), communication.getPlayerInfos());
	}

	@Override
	public void update() {
		
	}

	@Override
	public void buy(Item item) {

	}	
}
