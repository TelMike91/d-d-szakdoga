package elte.szakdoga.game.controllers;

import com.esotericsoftware.kryonet.rmi.ObjectSpace;

import elte.szakdoga.MyObserver;
import elte.szakdoga.actors.controllers.MultiPlayerController;
import elte.szakdoga.actors.models.Position.Direction;
import elte.szakdoga.battle.controller.BattleController;
import elte.szakdoga.battle.controller.BattleControllerInterface;
import elte.szakdoga.battle.controller.BattleControllerWithNetwork;
import elte.szakdoga.battle.controller.BattleSystem;
import elte.szakdoga.game.graphics.MainWindow;
import elte.szakdoga.game.graphics.menus.MainMenuGraphics;
import elte.szakdoga.logging.graphics.LoggingFieldLogger;
import elte.szakdoga.map.controller.MapController;
import elte.szakdoga.map.graphics.MapGraphics;
import elte.szakdoga.map.models.MyMap;
import elte.szakdoga.map.models.fields.FieldProcessor;
import elte.szakdoga.narrator.controllers.NarratorController;
import elte.szakdoga.narrator.models.ai.NarratorAI;
import elte.szakdoga.narrator.models.ai.NarratorAI.NarratorIntelligence;
import elte.szakdoga.narrator.models.ai.NarratorAIWithNetwork;
import elte.szakdoga.network.MyClient;
import elte.szakdoga.network.messages.UpdateMessage;
import elte.szakdoga.network.rmi.ServerDataInterface;

public class GameControllerWithNetwork extends AbstractGameController implements MyObserver {
	private transient MyClient client;

	public GameControllerWithNetwork(MyMap map, MainWindow window, MyClient client) {
		super(window);
		this.client = client;
		this.playerController = new MultiPlayerController(client);
		this.fieldProcessor = new FieldProcessor(this);
		battleController = new BattleController(new BattleSystem(), this);
		mapController = new MapController(map, MapGraphics.createMapGraphics(map.getRows(), map.getColumns()));
		NarratorAI ai = new NarratorAI(map);
		narratorController = new NarratorController(this, new NarratorAIWithNetwork(ai, client));
		ai.setIntelligence(NarratorIntelligence.LISTENER_NARRATOR.getAction(narratorController.getNarrator()));
		narratorController.start();
		messageListener = new MyMessageListener(narratorController, this, window);
		new Thread(messageListener).start();		
		client.addObserver(this);
		update();
	}	

	public void gameOver() {
		narratorController.shutDown();
    	messageListener.setGameOver(true);
    	LoggingFieldLogger.addText("Game over");
		client.disconnect(playerController.getCurrentPlayer().getId());
    	mainFrame.changePanel(new MainMenuGraphics(mainFrame));
	}
	
	@Override
	public void movePlayer(Direction direction) {
		if(client.isConnected()) {
			playerController.movePlayer(direction);
			mapController.getField(playerController.getCurrentPlayerPosition()).visit(fieldProcessor);
			narratorController.setPlayer(getCurrentPlayer());
		} else {
			gameOver();
		}
	}

	/**
	 * Frissíti a térképet a játékosok pozíciójának függvényében és a térkép
	 * helyzetében.
	 */
	public void update() {

	}	

	/**
	 * Frissítjük a fő panelt az ablakban azoknak a játékosoknak, akik éppen harcban
	 * vannak.
	 */
	public void updatePanelIfPlayerIsInBattle() {
		if(client.isConnected()) {
			ServerDataInterface test = ObjectSpace.getRemoteObject(client.getConnection(), 1, ServerDataInterface.class);
			if (battleController.isBattleInitiated()) {
				BattleControllerInterface battle = new BattleControllerWithNetwork(getCurrentPlayer().getId(), client, test,
						this);
				mainFrame.changePanel(battle.getView());
				battle.update();
			}
		} else {
			gameOver();
		}
	}

	@Override
	public void update(UpdateMessage communication) {
		mapController.setMap(communication.getMap());
		mapController.updateMap(getCurrentPlayer(), communication.getPlayerInfos());
	}

}
