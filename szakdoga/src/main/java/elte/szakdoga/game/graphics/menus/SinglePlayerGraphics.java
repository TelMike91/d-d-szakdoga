package elte.szakdoga.game.graphics.menus;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JPanel;

import elte.szakdoga.game.graphics.MainWindow;
import elte.szakdoga.graphics.listeners.StartGameActionListener;
import elte.szakdoga.graphics.listeners.changepanel.ChangeToMainMenuGraphics;

@SuppressWarnings("serial")
public class SinglePlayerGraphics extends JPanel {

	private ConfigurationMenu configuration;
	private JButton startGame;
	private JButton back;

	
	public SinglePlayerGraphics(MainWindow mainWindow) {
		this.configuration = new ConfigurationMenu();
		this.startGame = new JButton("Start game");	
		back = new JButton("Back");
		back.addActionListener(new ChangeToMainMenuGraphics(mainWindow));
		startGame.addActionListener(new StartGameActionListener(mainWindow, configuration));
		addComponents();
	}
	
	private void addComponents() {
		GridBagConstraints gc = new GridBagConstraints();
		gc.gridx = 0;
		gc.gridy = 0;
		gc.insets = new Insets(0, 0, 32, 0);
		gc.fill = GridBagConstraints.BOTH;
		gc.anchor = GridBagConstraints.CENTER;
		this.add(configuration, gc);
		gc.gridx = 1;
		this.add(startGame, gc);
		gc.gridx = 2;
		this.add(back, gc);
	}
}
