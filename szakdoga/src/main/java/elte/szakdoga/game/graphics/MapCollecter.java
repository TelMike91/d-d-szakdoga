package elte.szakdoga.game.graphics;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

public class MapCollecter {
	public MapCollecter() {
		// TODO Auto-generated constructor stub
	}
	
	public static List<String> mapNames() {
		List<String> names = new LinkedList<>();
		
		File file = new File("maps");
		if(file.listFiles() != null) {
			for(File fileElement : file.listFiles()) {
				if(!fileElement.isDirectory())
					names.add(getName(fileElement.getName()));
			}
		}
		return names;		
	}

	private static String getName(String name) {
		return name.substring(0, name.indexOf("."));
	}
}
