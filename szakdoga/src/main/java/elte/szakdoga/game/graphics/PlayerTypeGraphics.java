package elte.szakdoga.game.graphics;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import elte.szakdoga.game.graphics.menus.HostGameGraphics;
import elte.szakdoga.game.graphics.menus.JoinGameGraphics;

@SuppressWarnings("serial")
public class PlayerTypeGraphics extends JPanel {
	private JRadioButton narrator;
	private JRadioButton player;
	private ButtonGroup playerType;
	
	public enum PlayerType {
		PLAYER,NARRATOR;
	}

	public PlayerTypeGraphics() {
		narrator = new JRadioButton("Narrator");
		player = new JRadioButton("Player");
		playerType = new ButtonGroup();
		playerType.add(player);
		playerType.add(narrator);
		
		player.setSelected(true);
		
		this.add(narrator);
		this.add(player);
	}

	public PlayerType getSelectedOption() {
		if(player.isSelected())
			return PlayerType.PLAYER;
		if(narrator.isSelected())
			return PlayerType.NARRATOR;
		return null; 
	}

	public void addChangeListener(HostGameGraphics hostGameGraphics) {
		narrator.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				hostGameGraphics.removeConfiguration();
			}
		});
		
		player.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				hostGameGraphics.addConfiguration();
				
			}
		});
	}

	public void addChangeListener(JoinGameGraphics joinGameGraphics) {
		narrator.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				joinGameGraphics.removeConfiguration();
			}
		});
		
		player.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				joinGameGraphics.addConfiguration();
				
			}
		});
	}
}
