package elte.szakdoga.game.graphics;

import java.awt.GridBagConstraints;

@SuppressWarnings("serial")
public class GamePanelConstraints extends GridBagConstraints {
	public GamePanelConstraints() {
	    gridx = 0;
	    gridy = 0;
	    fill = BOTH;
	}

}
