package elte.szakdoga.game.graphics;

import java.util.LinkedList;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

public class PlayerListModel implements ListModel<String> {
	private List<String> playerList;

	public PlayerListModel() {
		this.playerList = new LinkedList<>();
	}
	
	public void addPlayerName(String name) {
		playerList.add(name);
	}

	@Override
	public void addListDataListener(ListDataListener l) {
	}

	@Override
	public String getElementAt(int index) {
		return playerList.get(index);
	}

	@Override
	public int getSize() {
		return playerList.size();
	}

	@Override
	public void removeListDataListener(ListDataListener l) {
		
	}

}
