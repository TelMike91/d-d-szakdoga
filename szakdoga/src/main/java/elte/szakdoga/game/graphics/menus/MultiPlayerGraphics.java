package elte.szakdoga.game.graphics.menus;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

import elte.szakdoga.game.graphics.MainWindow;
import elte.szakdoga.graphics.listeners.changepanel.ChangeToHostGraphics;
import elte.szakdoga.graphics.listeners.changepanel.ChangeToJoinGraphics;
import elte.szakdoga.graphics.listeners.changepanel.ChangeToMainMenuGraphics;

@SuppressWarnings("serial")
public class MultiPlayerGraphics extends JPanel {
	private JButton host;
	private JButton join;
	private JButton back;

	public MultiPlayerGraphics(MainWindow mainWindow) {
		initComponents();
		addActionListeners(mainWindow);		
		addComponents();
	}

	private void addActionListeners(MainWindow mainWindow) {
		this.host.addActionListener(new ChangeToHostGraphics(mainWindow));
		this.join.addActionListener(new ChangeToJoinGraphics(mainWindow));				
		this.back.addActionListener(new ChangeToMainMenuGraphics(mainWindow));
	}

	private void initComponents() {
		this.host = new JButton("Host game");		
		this.join = new JButton("Join game");
		this.back = new JButton("Back");
	}

	private void addComponents() {
		this.setLayout(new GridLayout(3,1));
		this.add(host);
		this.add(join);
		this.add(back);
	}

}
