package elte.szakdoga.game.graphics.menus;

import java.awt.Color;
import java.awt.GridBagLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

import elte.szakdoga.game.graphics.BackgroundColorlessButton;
import elte.szakdoga.game.graphics.GridBagConstaintsWrapper;
import elte.szakdoga.game.graphics.MainWindow;
import elte.szakdoga.graphics.listeners.QuitActionListener;
import elte.szakdoga.graphics.listeners.changepanel.ChangeToMultiPlayerGraphics;
import elte.szakdoga.graphics.listeners.changepanel.ChangeToSinglePlayerGraphics;

@SuppressWarnings("serial")
public class MainMenuGraphics extends JPanel {
	private JButton newGame;
	private JButton multiplayer;
	private JButton quit;
	
	public MainMenuGraphics(MainWindow mainWindow) {
		this.newGame = new BackgroundColorlessButton("start");		
		this.multiplayer = new BackgroundColorlessButton("multiplayer");
		this.quit = new BackgroundColorlessButton("quit");
		
		this.setBackground(Color.BLACK);
		this.setLayout(new GridBagLayout());
		
		newGame.addActionListener(new ChangeToSinglePlayerGraphics(mainWindow));
		
		multiplayer.addActionListener(new ChangeToMultiPlayerGraphics(mainWindow));
		
		quit.addActionListener(new QuitActionListener(mainWindow));
		
		
		GridBagConstaintsWrapper gc = new GridBagConstaintsWrapper();
		GridBagConstaintsWrapper.InsetsBuilder insetBuilder = new GridBagConstaintsWrapper.InsetsBuilder();
		
		insetBuilder.setTop(20)
				.setLeft(20)
				.setRight(20)
				.setBottom(20);
		
		gc.setInsets(insetBuilder.build());

		this.add(newGame, gc.build());
		gc.nextRow();
		this.add(multiplayer, gc.build());
		gc.nextRow();
		this.add(quit, gc.build());
	}
}
