package elte.szakdoga.game.graphics.menus;

import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.database.DatabaseConnection;

public class PlayersFromDatabase implements ListModel<String>{
	private List<Player> players;
	
	public PlayersFromDatabase() {
		DatabaseConnection connection = DatabaseConnection.getInstance();
		players = connection.getAllPlayers();	
	}

	@Override
	public void addListDataListener(ListDataListener l) {
		
	}

	@Override
	public String getElementAt(int index) {		
		return players.get(index).getName();
	}

	@Override
	public int getSize() {
		return players.size();
	}

	@Override
	public void removeListDataListener(ListDataListener l) {
		
	}

	public Player getPlayer(int selectedIndex) {
		return players.get(selectedIndex);
	}
	
	
}
