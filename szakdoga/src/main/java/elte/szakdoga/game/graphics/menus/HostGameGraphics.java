package elte.szakdoga.game.graphics.menus;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;

import elte.szakdoga.game.graphics.MainWindow;
import elte.szakdoga.game.graphics.PlayerListModel;
import elte.szakdoga.game.graphics.PlayerTypeGraphics;
import elte.szakdoga.graphics.listeners.HostStartGameListener;
import elte.szakdoga.graphics.listeners.MultiPlayerStartGameActionListener;
import elte.szakdoga.graphics.listeners.ServerShutDownListener;
import elte.szakdoga.graphics.listeners.changepanel.ChangeToMainMenuGraphics;
import elte.szakdoga.network.MyClient;
import elte.szakdoga.network.MyServer;
import elte.szakdoga.network.Ports;
import elte.szakdoga.network.Ports.Builder;

@SuppressWarnings("serial")
public class HostGameGraphics extends JPanel {
	private JList<String> playerList;
	private JButton back;
	private JButton startGame;
	private MyServer server;	
	private MyClient currentComputer;
	private PlayerListModel playerModel;
	private Configuration configuration;
	private PlayerTypeGraphics playerType;

	public HostGameGraphics(MainWindow mainWindow) {		
		initComponents();				
		addComponents();
	}

	private void initComponents() {
		playerList = new JList<>();
		playerModel = new PlayerListModel();
		playerList.setModel(playerModel);
		back = new JButton("Back");							
		startGame = new JButton("Start game");
		playerType = new PlayerTypeGraphics();
		
		configuration = new ConfigurationMenu();
	}

	private void addActionListeners(MainWindow mainWindow) {
		back.addActionListener(new ChangeToMainMenuGraphics(mainWindow));
		back.addActionListener(new ServerShutDownListener(server));
		startGame.addActionListener(new MultiPlayerStartGameActionListener(mainWindow, currentComputer, playerType, configuration));
		startGame.addActionListener(new HostStartGameListener(configuration, server));
		playerType.addChangeListener(this);
	}

	public void addConfiguration() {
		configuration.setEnabled(true);
	}

	public void removeConfiguration() {
		configuration.setEnabled(false);		
	}

	private void addComponents() {
		this.setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();
		gc.gridx = 0;
		gc.gridy = 0;
		this.add(playerList,gc);
		gc.gridy = 1;
		this.add(playerType, gc);
		gc.gridy = 2;
		this.add(configuration.getPanel(), gc);
		gc.gridy = 3;
		this.add(startGame, gc);
		gc.gridx = 1;
		this.add(back,gc );
	}

	public void initClient(MainWindow mainWindow) {
		try {
			Ports defaultPorts = new Builder().buildDefault();
			server = new MyServer(defaultPorts);
			server.setJoinListener(playerModel);
			currentComputer = new MyClient(MyClient.LOCAL_ADDRESS, defaultPorts);
			new Thread(server).start();	
			currentComputer.connect();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		addActionListeners(mainWindow);
	}
	
}
