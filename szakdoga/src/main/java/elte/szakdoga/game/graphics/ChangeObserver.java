package elte.szakdoga.game.graphics;

public interface ChangeObserver {
	void update();
}
