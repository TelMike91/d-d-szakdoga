package elte.szakdoga.game.graphics;

import java.awt.GridLayout;
import java.awt.event.KeyListener;

import javax.swing.JPanel;

import elte.szakdoga.actors.graphics.PlayerPanel;
import elte.szakdoga.game.controllers.GameControllerInterface;
import elte.szakdoga.graphics.listeners.KeyBindings;
import elte.szakdoga.narrator.models.ai.NarratorInterface;

/**
 * Ez egy JPanel ami a játék grafikáját reprezentálja
 */
@SuppressWarnings("serial")
public class GamePanel extends JPanel {      
    private PlayerPanel playerPanel;
    private GameControllerInterface gameController;    

    private GamePanel(PlayerPanel playerPanel, GameControllerInterface gameController) {
        this.playerPanel = playerPanel; 
        this.gameController = gameController;
    }         
    
//    private void addNarratorMouseListenerForMap(NarratorInterface narrator) {
//		gameController.addNarratorMouseListenerForMap(narrator);		
//	}
//
//	private void addComponentsForNarrator() {
//		add(gameController.getMapView());
//		refresh();
//	}

	private void refresh() {
		revalidate();
		repaint();
	}

	/**
     * Factory method, amivel létrehozzuk a játékpanelt.
     * @param gameController A kontroller amivel irányítjuk a játékot
     * @return Egy inicializált játékpanel, vagy null ha valami nem sikerül.
     */
    public static GamePanel createGamePanel(GameControllerInterface gameController) {
        PlayerPanel playerPanel = gameController.getPlayerView();
        GamePanel gamePanel = new GamePanel(playerPanel, gameController);        
        gamePanel.setLayout(new GridLayout(1, 2));            
                
        gameController.update();        
        
        KeyListener keyBindings = new KeyBindings(gameController, playerPanel);
        gamePanel.addKeyListener(keyBindings);
        playerPanel.addKeyListener(keyBindings);
        gamePanel.addKeyListenerForButtons(keyBindings);
        gamePanel.addComponents(); 
        
        playerPanel.updatePlayerPanel();
        
        return gamePanel;
    }

    private void addKeyListenerForButtons(KeyListener keyListener) {
        gameController.addKeyListenerForButtons(keyListener);        
    }        

    private void addComponents() {
        add(gameController.getMapView());
        add(playerPanel);
        
        refresh();
    }        
}
