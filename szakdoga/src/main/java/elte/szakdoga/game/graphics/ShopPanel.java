package elte.szakdoga.game.graphics;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import elte.szakdoga.game.controllers.GameControllerInterface;
import elte.szakdoga.graphics.listeners.PlayerQuitsShopListener;
import elte.szakdoga.graphics.listeners.changepanel.ChangeToGamePanel;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.logging.graphics.LoggingFieldLogger;

@SuppressWarnings("serial")
public class ShopPanel extends JPanel {

	private JList<String> listOfItem;
	private JButton buy;
	private JButton back;
		
	
	class ItemListModel implements ListModel<String> {
		private List<Item> itemShop;

		public ItemListModel(List<Item> purchasableItems) {
			itemShop = purchasableItems;
		}

		@Override
		public void addListDataListener(ListDataListener l) { }

		@Override
		public String getElementAt(int index) {
			return itemShop.get(index).getName() + " " + itemShop.get(index).getCost();
		}
		
		@Override
		public int getSize() {
			return itemShop.size();
		}

		@Override
		public void removeListDataListener(ListDataListener l) {

		}
		
	}

	public ShopPanel(List<Item> purchasableItems, GameControllerInterface gameController) {
		back = new JButton("Back");
		buy = new JButton("Buy");
		
		back.addActionListener(new ChangeToGamePanel(gameController.getMainFrame()));
		back.addActionListener(new PlayerQuitsShopListener(gameController));
		
		listOfItem = new JList<>();
		listOfItem.setModel(new ItemListModel(purchasableItems));
	
		this.setLayout(new FlowLayout());
		
		this.add(listOfItem);
		listOfItem.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				LoggingFieldLogger.addText(purchasableItems.get(listOfItem.getSelectedIndex()).toString());
			}
		});
		this.add(buy);
		buy.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!listOfItem.isSelectionEmpty()) {
					gameController.buy(purchasableItems.get(listOfItem.getSelectedIndex()));
				}
				
			}
		});
		this.add(back);
	}

}
