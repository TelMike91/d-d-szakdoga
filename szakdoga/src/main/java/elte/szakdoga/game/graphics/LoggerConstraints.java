package elte.szakdoga.game.graphics;

import java.awt.GridBagConstraints;

import org.springframework.stereotype.Component;

@SuppressWarnings("serial")
@Component
public class LoggerConstraints extends GridBagConstraints{
	public LoggerConstraints() {
        gridx = 0;
        gridy = 1; 
        ipadx = 100;
        ipady = 100;
        fill = GridBagConstraints.BOTH;
	}
}
