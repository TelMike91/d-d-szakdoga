package elte.szakdoga.game.graphics;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.text.PlainDocument;

public class LabelAndTextField {
	private JLabel name;
	private JTextField textField;		
	
	public LabelAndTextField(ChangeObserver observer, String name) {
		this.name = new JLabel(name);
		this.textField = new JTextField(4);		
		
		PlainDocument document = (PlainDocument) textField.getDocument();
		document.setDocumentFilter(new IntFilter(observer));
	}

	public JLabel getName() {
		return name;
	}

	public void setName(JLabel name) {
		this.name = name;
	}

	public JTextField getTextField() {
		return textField;
	}

	public void setTextField(JTextField textField) {
		this.textField = textField;
	}
	
	public int getValue() {
		return Integer.parseInt(textField.getText());
	}

	public boolean isFilled() {
		return this.textField.getText().length() > 0;
	}

	public void setText(int value) {
		this.textField.setText(String.valueOf(value));
		
	}
	
	
}
