package elte.szakdoga.game.graphics;

import java.awt.GridBagConstraints;
import java.awt.Insets;

public class GridBagConstaintsWrapper {
	private int gridx;
	private int gridy;
	private Insets insets;
	
	public GridBagConstaintsWrapper() {
		gridx = 0;
		gridy = 0;
		insets = new Insets(0, 0, 0, 0);
	}
	
	public GridBagConstraints build() {
		GridBagConstraints gc = new GridBagConstraints();
		gc.gridx = gridx;
		gc.gridy = gridy;
		gc.insets = insets;
		return gc;
	}
	
	public void setInsets(Insets insets) {
		this.insets = insets;
	}
	
	public void resetColumn() {
		gridx = 0;		
	}
	
	public void resetRow() {
		gridy = 0;
	}
	
	public void nextColumn() {
		gridx++;
	}
	
	public void nextRow() {
		gridy++;
	}
	
	public static class InsetsBuilder {
		private int top;
		private int bottom;
		private int left;
		private int right;
		
		public InsetsBuilder setTop(int top) {
			this.top = top;
			return this;
		}
		
		public InsetsBuilder setLeft(int left) {
			this.left = left;
			return this;
		}
		
		public InsetsBuilder setBottom(int bottom) {
			this.bottom = bottom;
			return this;
		}
		
		public InsetsBuilder setRight(int right) {
			this.right = right;
			return this;
		}
		
		public Insets build() {
			return new Insets(top, left, bottom, right);
		}
	}
}
