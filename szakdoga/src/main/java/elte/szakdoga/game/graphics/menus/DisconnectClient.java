package elte.szakdoga.game.graphics.menus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import elte.szakdoga.network.MyClient;

public class DisconnectClient implements ActionListener {
	private MyClient client;

	public DisconnectClient(MyClient client) {
		this.client = client;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		client.disconnect();
	}

}
