package elte.szakdoga.game.graphics;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import elte.szakdoga.actors.models.player.Player;

@SuppressWarnings("serial")
public class CharacterCreationPanel extends JPanel implements ChangeObserver {
	private Map<Attributes, LabelAndTextField> stats;
	private JLabel pointsLabel;
	private JLabel name;
	private static final int MAX_POINTS = 1000;
	private int points = MAX_POINTS;
	private JTextField nameValue;
	private boolean pointCalculationDisabled;

	enum Attributes {
		Hp {

			@Override
			public void setValue(Player player, int value) {
				player.setMaxHP(value);
				player.setHP(player.getMaxHP());
			}

			@Override
			public int getValue(Player player) {

				return player.getMaxHP();
			}
		},
		Strength {
			@Override
			public void setValue(Player player, int value) {
				player.setStrength(value);
			}

			@Override
			public int getValue(Player player) {
				// TODO Auto-generated method stub
				return player.getStrength();
			}
		},
		Magic {
			@Override
			public void setValue(Player player, int value) {
				player.setMag(value);
			}

			@Override
			public int getValue(Player player) {
				// TODO Auto-generated method stub
				return player.getMagic();
			}
		},
		Endurance {
			@Override
			public void setValue(Player player, int value) {
				player.setEndurance(value);
			}

			@Override
			public int getValue(Player player) {
				// TODO Auto-generated method stub
				return player.getEndurance();
			}
		},
		Luck {
			@Override
			public void setValue(Player player, int value) {
				player.setLuck(value);
			}

			@Override
			public int getValue(Player player) {

				return player.getLuck();
			}
		},
		Agility {

			@Override
			public void setValue(Player player, int value) {
				player.setAgility(value);
			}

			@Override
			public int getValue(Player player) {
				// TODO Auto-generated method stub
				return player.getAgility();
			}

		};

		public abstract int getValue(Player player);

		public abstract void setValue(Player player, int value);
	}

//	points = stats.get(attribute).getValue();
//	pointsLabel.setText("" + points);
	public CharacterCreationPanel() {
		stats = new HashMap<>();
		for (Attributes attribute : Attributes.values()) {
			stats.put(attribute, new LabelAndTextField(this, attribute.toString()));
		}
		this.setBorder(
				BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.red), "Character creation"));
		this.setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();

		name = new JLabel("Name:");
		nameValue = new JTextField(10);
		pointsLabel = new JLabel("Remaining points: 100");
		int gridy = 0;
		constraints.ipadx = 100;
		constraints.ipady = 10;
		constraints.gridx = 0;
		this.add(name, constraints);
		constraints.gridx = 1;
		this.add(nameValue, constraints);
		constraints.gridy = 1;
		this.add(pointsLabel, constraints);
		gridy = 2;
		constraints.gridy = 2;
		for (LabelAndTextField field : stats.values()) {
			constraints.gridx = 0;
			constraints.gridy = gridy;
			add(field.getName(), constraints);
			constraints.gridx = 1;
			add(field.getTextField(), constraints);
			gridy++;
		}

	}

	public Player buildPlayer() {
		Player player = new Player();
		for (Attributes attribute : stats.keySet()) {
			attribute.setValue(player, stats.get(attribute).getValue());
		}
		player.setName(nameValue.getText());
		player.setGold(1000);
		return player;
	}

	public boolean everythingFilled() {
		for (LabelAndTextField label : stats.values()) {
			if (!label.isFilled()) {
				return false;
			}
		}
		return true;
	}

	public void fillPlayer(Player player) {
		for (Attributes attribute : stats.keySet()) {
			stats.get(attribute).setText(attribute.getValue(player));
		}
		nameValue.setText(player.getName());
	}

	public void calculatePoints() {
		int sum = 0;
		for (Attributes attribute : stats.keySet()) {
			if (stats.get(attribute).isFilled()) {

				sum += stats.get(attribute).getValue();
			}

		}
		pointsLabel.setText("Points remaining: " + (MAX_POINTS - sum));
		points = MAX_POINTS - sum;
	}

	public int getPoints() {
		return points;
	}

	@Override
	public void setEnabled(boolean value) {
		super.setEnabled(value);
		for (LabelAndTextField attribute : stats.values()) {
			attribute.getTextField().setEnabled(value);
		}
	}

	@Override
	public void update() {
		this.calculatePoints();
	}
	
	

	public boolean isPointCalculationDisabled() {
		return pointCalculationDisabled;
	}

	public void setPointCalculationDisabled(boolean pointCalculationDisabled) {
		this.pointCalculationDisabled = pointCalculationDisabled;
	}

	public void disablePointCalculation() {
		pointCalculationDisabled = false;
	}

}
