package elte.szakdoga.game.graphics;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.springframework.stereotype.Component;

import elte.szakdoga.game.graphics.menus.MainMenuGraphics;
import elte.szakdoga.logging.graphics.LoggingFieldLogger;

/**
 * Ez a főablak, ami tartalmazza a panelokat. Lehet módosítani a megjelenített panelt metódusok által.
 */
@SuppressWarnings("serial")
@Component
public class MainWindow extends JFrame {
    private JPanel currentPanel;
    private GamePanel gamePanel;    
    private static final GridBagConstraints GAME_PANEL_CONSTRAINTS = new GamePanelConstraints();
    private static final GridBagConstraints LOGGER_CONSTRAINTS = new LoggerConstraints();
    
    @PostConstruct
    public void init() {
		this.setVisible(true);
		this.pack();
    }
    
    /**
     * Létrehozzuk a főablakot.  
     */
    public MainWindow() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new GridBagLayout());        
        
        this.currentPanel = new MainMenuGraphics(this);
        this.setTitle("Szakdoga");
        add(currentPanel);
        add(LoggingFieldLogger.getLoggingField(), LOGGER_CONSTRAINTS);
    }  
        
    /**
     * Módosítjuk a megjelenítendő panelt. 
     * @param otherPanel A panel amit szeretnénk megjeleníteni.
     * @exception NullPointerException Ha a paraméter null, akkor NullPointerException várható.
     */
    public void changePanel(JPanel otherPanel) {
    	Objects.requireNonNull(otherPanel);
    	if(currentPanel != null)
    		this.remove(currentPanel);
    	this.currentPanel = otherPanel;    	
    	this.add(currentPanel, GAME_PANEL_CONSTRAINTS);
    	this.repaint();
    	this.revalidate();
    }
    
    /**
     * Visszaállítjuk a játékpanelt.
     * @throws AssertionError Ha a gamePanel null volt.
     */
    public void revertPanel() {    	    	
    	this.changePanel(gamePanel);
    }

	public void setGamePanel(GamePanel gamePanel) {
		this.gamePanel = gamePanel;
	}

	/**
	 * Vissdaadja az ablak középpontját.
	 * @return Az ablak középpontja.
	 */
	public Point getMiddlePoint() {
		return new Point(this.getWidth() / 2, this.getHeight() / 2);
	}
}
