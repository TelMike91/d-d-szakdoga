package elte.szakdoga.game.graphics.menus;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import elte.szakdoga.game.graphics.MainWindow;
import elte.szakdoga.game.graphics.PlayerTypeGraphics;
import elte.szakdoga.graphics.listeners.LocalGameJoinActionListener;
import elte.szakdoga.graphics.listeners.changepanel.ChangeToMultiPlayerGraphics;

@SuppressWarnings("serial")
public class JoinGameGraphics extends JPanel {	
	private JButton back;
	private JTextField connectToIp;
	private JButton connect;
	private Configuration configuration;
	private PlayerTypeGraphics playerType;
	
	public JoinGameGraphics(MainWindow mainWindow) {
		setLayout(new GridBagLayout());
	
		GridBagConstraints gc = new GridBagConstraints();
		connect = new JButton("Connect");
		back = new JButton("Back");
		connectToIp = new JTextField();
		playerType = new PlayerTypeGraphics();
		configuration = new ConfigurationMenuWithoutMapAndNarrator();
		connect.addActionListener(new LocalGameJoinActionListener(mainWindow, back, connectToIp, connect, playerType, configuration));		
		back.addActionListener(new ChangeToMultiPlayerGraphics(mainWindow));
		playerType.addChangeListener(this);
		
		gc.gridx = 0;
		gc.gridy = 0;
		add(playerType, gc);
		gc.gridy = 1;
		add(configuration.getPanel(), gc);
		gc.gridy = 2;
		add(connectToIp, gc);
		gc.gridy = 3;
		add(connect, gc);
		gc.gridy = 4;
		add(back, gc);
	}
	
	public void addConfiguration() {
		configuration.setEnabled(true);
	}

	public void removeConfiguration() {
		configuration.setEnabled(false);		
	}

}
