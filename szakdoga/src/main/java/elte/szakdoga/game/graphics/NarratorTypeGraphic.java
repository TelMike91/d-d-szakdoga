package elte.szakdoga.game.graphics;

import java.util.LinkedList;
import java.util.List;

import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

import elte.szakdoga.narrator.models.ai.NarratorAI.NarratorIntelligence;

@SuppressWarnings("serial")
public class NarratorTypeGraphic extends JPanel {
	public enum NarratorType {
		Aggressive {
			@Override
			public NarratorIntelligence getIntelligence() {
				// TODO Auto-generated method stub
				return NarratorIntelligence.ACTIVE;
			}
		}, Test {
			@Override
			public NarratorIntelligence getIntelligence() {
				// TODO Auto-generated method stub
				return NarratorIntelligence.TEST;
			}
		}, None {

			@Override
			public NarratorIntelligence getIntelligence() {
				return NarratorIntelligence.LISTENER_NARRATOR;
			}
			
		};
		
		public abstract NarratorIntelligence getIntelligence();
	}
	
	private class NarratorListModel implements ListModel<NarratorType> {
		private List<NarratorType> narratorTypes;
		
		public NarratorListModel() {
			narratorTypes = new LinkedList<>();
		}

		@Override
		public void addListDataListener(ListDataListener arg0) {
			
		}

		@Override
		public NarratorType getElementAt(int arg0) {		
			return narratorTypes.get(arg0);
		}

		@Override
		public int getSize() {
			return narratorTypes.size();
		}

		@Override
		public void removeListDataListener(ListDataListener arg0) {

		}

		public void addElement(NarratorType type) {
			narratorTypes.add(type);
		}
		
	}
	
	private JList<NarratorType> narratorType;
	
	public NarratorTypeGraphic() {
		NarratorListModel listModel = new NarratorListModel();
		
		for(NarratorType type : NarratorType.values()) {
			listModel.addElement(type);
		}
		
		narratorType = new JList<>(listModel);
		
		
		this.add(narratorType);
	}

	public NarratorType getNarratorType() {
		return narratorType.getSelectedValue();
	}
}
