package elte.szakdoga.game.graphics.menus;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.game.graphics.CharacterCreationPanel;
import elte.szakdoga.game.graphics.NarratorTypeGraphic;
import elte.szakdoga.game.graphics.NarratorTypeGraphic.NarratorType;

@SuppressWarnings("serial")
public class ConfigurationMenuWithoutMapAndNarrator extends JPanel implements Configuration {
	private JScrollPane scrollPaneForNarratorType;
	private JScrollPane scrollPaneForSavePlayers;
	private PlayersFromDatabase playerFromDatabase;
	private JList<String> listOfPlayers;
	private NarratorTypeGraphic narratorType;
	private CharacterCreationPanel characterCreation;
	
	public ConfigurationMenuWithoutMapAndNarrator() {		
		scrollPaneForNarratorType = new JScrollPane();
		scrollPaneForNarratorType.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.red), "Narrator intelligence"));
		scrollPaneForSavePlayers = new JScrollPane();
		playerFromDatabase = new PlayersFromDatabase();
		
		listOfPlayers = new JList<>(playerFromDatabase);
		listOfPlayers.addListSelectionListener(new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if(!e.getValueIsAdjusting()) {
					characterCreation.fillPlayer(playerFromDatabase.getPlayer(listOfPlayers.getSelectedIndex()));
					characterCreation.setEnabled(false);
					characterCreation.disablePointCalculation();
				}
			}
		});

		this.characterCreation = new CharacterCreationPanel();
		this.narratorType = new NarratorTypeGraphic();
		
		this.setLayout(new GridBagLayout());				
		
		scrollPaneForSavePlayers.add(listOfPlayers);
		scrollPaneForSavePlayers.setViewportView(listOfPlayers);
		
		scrollPaneForNarratorType.add(narratorType);
		scrollPaneForNarratorType.setViewportView(narratorType);
		
		addComponents();
	}
	
	private void addComponents() {
		GridBagConstraints gc = new GridBagConstraints();
		gc.gridx = 0;
		gc.gridy = 0;
		gc.insets = new Insets(0, 0, 32, 0);
		gc.fill = GridBagConstraints.BOTH;
		gc.anchor = GridBagConstraints.CENTER;
		add(scrollPaneForSavePlayers, gc);
		gc.gridy = 1;
		this.add(characterCreation,gc);					
	}

	public int getPoints() {
		return characterCreation.getPoints();
	}

	public NarratorType getNarratorType() {
		return narratorType.getNarratorType();
	}

	public boolean everythingFilled() {
		return characterCreation.everythingFilled();
	}

	public Player buildPlayer() {
		return characterCreation.buildPlayer();
	}
	
	@Override
	public void setEnabled(boolean value) {
		super.setEnabled(value);
		listOfPlayers.setEnabled(value);
		characterCreation.setEnabled(value);
	}

	@Override
	public String getMapName() {
		return "";
	}

	@Override
	public JPanel getPanel() {
		return this;
	}

	@Override
	public boolean isMapSelectable() {
		return false;
	}

	@Override
	public boolean isCalculatingDisabled() {
		return characterCreation.isPointCalculationDisabled();
	}
}
