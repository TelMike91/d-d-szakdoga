package elte.szakdoga.game.graphics.menus;

import javax.swing.JPanel;

import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.game.graphics.NarratorTypeGraphic.NarratorType;

public interface Configuration {
	public int getPoints();
	public NarratorType getNarratorType();

	public boolean everythingFilled();
	public Player buildPlayer();
	public void setEnabled(boolean value);
	public String getMapName();
	public JPanel getPanel();
	public boolean isMapSelectable();
	boolean isCalculatingDisabled();
}
