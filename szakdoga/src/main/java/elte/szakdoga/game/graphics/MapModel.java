package elte.szakdoga.game.graphics;

import java.util.LinkedList;
import java.util.List;

import javax.swing.ListModel;
import javax.swing.event.ListDataListener;

public class MapModel implements ListModel<String> {
	private List<String> names = new LinkedList<>();
	
	public void addName(String name) {
		names.add(name);
	}

	@Override
	public void addListDataListener(ListDataListener arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getElementAt(int arg0) {
		return names.get(arg0);
	}

	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		return names.size();
	}

	@Override
	public void removeListDataListener(ListDataListener arg0) {
		// TODO Auto-generated method stub
		
	}

	public void addNames(List<String> mapNames) {
		for(String name : mapNames) {
			names.add(name);
		}
	}
	
}