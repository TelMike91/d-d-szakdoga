package elte.szakdoga.game.graphics;

import javax.swing.ImageIcon;
import javax.swing.JButton;

@SuppressWarnings("serial")
public class BackgroundColorlessButton extends JButton {

	public BackgroundColorlessButton(String name) {
		ImageIcon test = new ImageIcon("assets/images/" + name + ".png");
		this.setIcon(test);
		this.setBorderPainted(true); 
		this.setContentAreaFilled(false); 
		this.setFocusPainted(false); 
		this.setOpaque(false);
	}

}
