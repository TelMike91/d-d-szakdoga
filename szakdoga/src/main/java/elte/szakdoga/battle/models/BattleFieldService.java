package elte.szakdoga.battle.models;

import java.util.LinkedList;
import java.util.List;

import elte.szakdoga.actors.models.Actor;

public class BattleFieldService {
	private List<Actor> round;
	
	public BattleFieldService() {
		round = new LinkedList<>();
	}
	
	public void initRound(BattleField battleField) {
		round.addAll(battleField.getFriends());
		round.addAll(battleField.getEnemies());
		
		round.sort((var1, var2) -> -(var1.getAgility() - var2.getAgility()));
	}
	
	public Actor nextActor() {			
		return round.remove(0);
	}
}
