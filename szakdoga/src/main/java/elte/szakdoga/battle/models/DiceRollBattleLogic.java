package elte.szakdoga.battle.models;

import java.util.List;
import java.util.Random;

import elte.szakdoga.actors.models.Actor;

/**
 * A kockadobásos harcrendszer megvalósítása.
 * 
 * @author Teleki Miklós
 *
 */
public class DiceRollBattleLogic implements BattleLogic {

	private int rollDice() {
		return new Random().nextInt(100) + 1;
	}

	@Override
	public boolean tryToFlee(List<Actor> fleeingParty, List<Actor> oppositionParty) {
		int fleeValue = 0;
		int protectValue = 0;
		protectValue += rollDice();
		fleeValue += rollDice();
		for (Actor fleeer : fleeingParty) {
			fleeValue += fleeer.getLuck();
		}
		for (Actor opposer : oppositionParty) {
			protectValue += opposer.getLuck();
		}
		return fleeValue > protectValue;
	}

	@Override
	public boolean tryToAttack(Actor attacker, Actor defender) {
		boolean successful = rollDice() + attacker.getAgility() >= defender.getAgility();
		if (successful) {
			calculateHP(attacker, defender);
		}
		return successful;
	}

	private void calculateHP(Actor attacker, Actor defender) {
		defender.damage(calculateDamage(attacker, defender));
	}

	private int calculateDamage(Actor attacker, Actor defender) {
		return Math.max(attacker.getStrength() - defender.getDefensePoint(), 1);
	}

	@Override
	public void attack(Actor attacker, Actor defender) {
		int damagePoint = attacker.getAttackPoint() + attacker.getStrength() - defender.getDefensePoint();
		damagePoint = damagePoint < 0 ? 0 : damagePoint;
		defender.damage(damagePoint);
	}

}
