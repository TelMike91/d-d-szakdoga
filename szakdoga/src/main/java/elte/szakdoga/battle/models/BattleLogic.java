package elte.szakdoga.battle.models;

import java.util.List;

import elte.szakdoga.actors.models.Actor;

/**
 * A harc logikájának interfésze. A harc logikájának, ennek az interfésznek a megvalósításával módosítható.
 * @author Teleki Miklós
 *
 */
public interface BattleLogic {
	/**
	 * Kiszámolja hogy a játékosok eltudnak-e szökni vagy sem.
	 * @param players A játékosok listája.
	 * @param enemies Az ellenfelek listája.
	 * @return Igaz, ha eltudnak szökni, hamis különben
	 */
	public boolean tryToFlee(List<Actor> players, List<Actor> enemies);
	
	/**
	 * Kiszámolja hogy a támadó eltudja-e találni az ellenfelét.
	 * @param attacker A támadó fél.
	 * @param defended A védekefő fél
	 * @return Igaz, ha eltalája, hamis különben.
	 */
	public boolean tryToAttack(Actor attacker, Actor defended);

	public void attack(Actor actor, Actor defender);
}
