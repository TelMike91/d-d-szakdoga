package elte.szakdoga.battle.models;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.actors.models.enemy.Enemy;
import elte.szakdoga.actors.models.player.Player;

/**
 * A harcmező modelje.
 * 
 * @author Teleki Miklós
 *
 */
public class BattleField {
	private List<Player> players;
	private List<Enemy> enemies;
	private Actor currentActor;
	private List<Actor> round;

	public BattleField() { }

	public BattleField(List<Enemy> enemies) {
		this.enemies = enemies;
		players = new LinkedList<>();
		round = new LinkedList<>();
	}

	public BattleField(BattleField battleField) {
		this.enemies = new LinkedList<>(battleField.getEnemies());
		players = new LinkedList<>(battleField.getFriends());
		round = new LinkedList<>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.battle.models.BattleFieldInterface#initRound()
	 */
	
	public void initRound() {
		round.addAll(players);
		round.addAll(enemies);

		round.sort((actor1, actor2) -> (actor1.getAgility() - actor2.getAgility()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.battle.models.BattleFieldInterface#nextActor()
	 */
	
	public void nextActor() {
		if (round.size() == 0) {
			initRound();
		}
		currentActor = round.remove(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * elte.szakdoga.battle.models.BattleFieldInterface#addPlayer(elte.szakdoga.
	 * actors.models.player.Player)
	 */
	
	public void addPlayer(Player player) {
		if (!players.contains(player))
			players.add(player);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.battle.models.BattleFieldInterface#addEnemy(elte.szakdoga.
	 * actors.models.enemy.Enemy)
	 */	
	public void addEnemy(Enemy enemy) {
		enemies.add(enemy);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.battle.models.BattleFieldInterface#setCurrentActor(elte.
	 * szakdoga.actors.models.Actor)
	 */	
	public void setCurrentActor(Actor actor) {
		this.currentActor = actor;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.battle.models.BattleFieldInterface#getNumberOfEnemies()
	 */
	
	public int getNumberOfEnemies() {
		return enemies.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.battle.models.BattleFieldInterface#getNumberOfFriends()
	 */
	
	public int getNumberOfFriends() {
		return players.size();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.battle.models.BattleFieldInterface#init()
	 */
	
	public void init() {
		initRound();
		nextActor();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.battle.models.BattleFieldInterface#getEnemy(int)
	 */
	
	public String getEnemyString(int m) {
		return enemies.get(m).getName() + " " + enemies.get(m).getHP();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.battle.models.BattleFieldInterface#getPlayer(int)
	 */
	
	public Player getPlayer(int chosenTarget) {
		return players.get(chosenTarget);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.battle.models.BattleFieldInterface#getCurrentActor()
	 */
	
	public Actor getCurrentActor() {
		return currentActor;
	}
	
	public List<Enemy> getEnemies() {
		return enemies;
	}

	
	public List<Player> getFriends() {
		return players;
	}

	
	public List<Player> getPlayers() {
		return players;
	}

	
	public void setEnemies(List<Enemy> enemyData) {
		this.enemies = enemyData;
	}

	
	public void setPlayers(List<Player> list) {
		this.players = list;
	}
	
	public Enemy getEnemy(int m) {
		return enemies.get(m);
	}
	
	public void removePlayer(Player player) {
		players.remove(player);
	}

	public List<Actor> getRound() {
		return Collections.unmodifiableList(round);
	}

	public void update() {
		enemies = enemies.stream().filter(e -> e.getHP() > 0).collect(Collectors.toList());
		players = players.stream().filter(e -> e.getHP() > 0).collect(Collectors.toList());	
		round = round.stream().filter(e -> e.getHP() > 0).collect(Collectors.toList());				
	}
}
