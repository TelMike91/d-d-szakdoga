package elte.szakdoga.battle.models.observer;

import elte.szakdoga.network.messages.UpdateBattle;

public interface BattleObserver {
	public void update(UpdateBattle battle);
}
