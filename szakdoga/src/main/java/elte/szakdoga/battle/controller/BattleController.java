package elte.szakdoga.battle.controller;

import java.util.Optional;

import javax.swing.JPanel;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.actors.models.enemy.Enemy;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.battle.controller.commands.AttackCommand;
import elte.szakdoga.battle.controller.commands.FleeCommand;
import elte.szakdoga.battle.graphics.BattlePanel;
import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.game.controllers.GameControllerInterface;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.logging.graphics.LoggingFieldLogger;

/**
 * Egy controller ami a harcot irányítja. A harc folyamatán frissíti majd a hozzá tartozó modelt, illetve a panelt.
 * Kapcsolatban áll a gameControllerrel, hogy a panelt tudja módosítani ha a harc véget ért vagy nem.
 * @author Teleki Miklós
 *
 */
public class BattleController implements BattleControllerInterface {
	private BattleField battleField;
	private BattlePanel battlePanel;
	private GameControllerInterface gameController;
	private BattleSystem battleSystem;
	
	public BattleController(BattleSystem battleSystem, GameControllerInterface gameControler) {
		this.gameController = gameControler;
		this.battleSystem = battleSystem;
	}
	
	public void setBattleField(BattleField battleField) {
		this.battleField = battleField;
	}
	
	public void initBattleField(BattleField battleField) {
		this.battleField = battleField;
		battlePanel = new BattlePanel(this, gameController.getCurrentPlayer());
		this.battleField.init();		
	}
	
	/* (non-Javadoc)
	 * @see elte.szakdoga.battle.controller.BattleControllerInterface#ownControl()
	 */
	@Override
	public boolean ownControl() {
		return battleField.getCurrentActor().equals(gameController.getCurrentPlayer());
	}
	
	/* (non-Javadoc)
	 * @see elte.szakdoga.battle.controller.BattleControllerInterface#attack(int)
	 */
	@Override
	public void attack(Actor actor) {
		boolean successful = battleSystem.executeAttackCommand(new AttackCommand(battleField.getCurrentActor(), actor));
		if(successful) {
			updateText("Attack successful");
		} else {
			updateText("Missed");
		}
		battleSystem.nextTurn(battleField);
		update();
	}
	
	/* (non-Javadoc)
	 * @see elte.szakdoga.battle.controller.BattleControllerInterface#doNothing()
	 */
	@Override
	public void doNothing() {
		battleSystem.execute(new DoNothingCommand(battleField));
		LoggingFieldLogger.addText("Doing nothing...");
	}
	
	/* (non-Javadoc)
	 * @see elte.szakdoga.battle.controller.BattleControllerInterface#checkIfOver()
	 */
	@Override
	public boolean checkIfOver() {
		boolean isOver = battleSystem.checkIfOver(battleField);
		if(isOver) {
			int gold = battleSystem.calculateGoldReward(battleField);
			battleField.getFriends().stream().forEach(player -> player.addGold(gold));
			gameController.changeToMapView();
			battleField = null;
		}
		return isOver;
	}

	/* (non-Javadoc)
	 * @see elte.szakdoga.battle.controller.BattleControllerInterface#getView()
	 */
	@Override
	public JPanel getView() {
		return battlePanel;
	}

	/* (non-Javadoc)
	 * @see elte.szakdoga.battle.controller.BattleControllerInterface#getNumberOfEnemies()
	 */
	@Override
	public int getNumberOfEnemies() {
		return battleField.getNumberOfEnemies();
	}
	
	/* (non-Javadoc)
	 * @see elte.szakdoga.battle.controller.BattleControllerInterface#getNumberOfFriends()
	 */
	@Override
	public int getNumberOfFriends() {
		return battleField.getNumberOfFriends();
	}

	/* (non-Javadoc)
	 * @see elte.szakdoga.battle.controller.BattleControllerInterface#getEnemy(int)
	 */
	@Override
	public String getEnemyStringRepresentation(int m) {
		return battleField.getEnemyString(m);
	}

	/* (non-Javadoc)
	 * @see elte.szakdoga.battle.controller.BattleControllerInterface#update()
	 */
	@Override
	public void update() {	
		battleField.update();
		if(!checkIfOver()) {
			if(ownControl()) {
				battlePanel.getControl();
			} else {
				battlePanel.loseControl();	
				battleField.getCurrentActor().takeAction(this);
			}
			battlePanel.update();
		}
	}

	/* (non-Javadoc)
	 * @see elte.szakdoga.battle.controller.BattleControllerInterface#getPlayer(int)
	 */
	@Override
	public String getPlayerStringRepresentation(int index) {
		return battleField.getPlayer(index).getName() + " " + battleField.getPlayer(index).getHP();
	}

	/* (non-Javadoc)
	 * @see elte.szakdoga.battle.controller.BattleControllerInterface#updateText(java.lang.String)
	 */
	@Override
	public void updateText(String answer) {
		LoggingFieldLogger.addText(answer);
	}

	/* (non-Javadoc)
	 * @see elte.szakdoga.battle.controller.BattleControllerInterface#flee()
	 */
	@Override
	public void flee() {
		battleSystem.executeFleeCommand(new FleeCommand());
	}

	/* (non-Javadoc)
	 * @see elte.szakdoga.battle.controller.BattleControllerInterface#defend()
	 */
	@Override
	public void defend() {
//		battleSystem.execute(new DefenseCommand());
	}

	@Override
	public BattleField getBattle() {
		return battleField;
	}

	@Override
	public Enemy getEnemy(int i) {
		return battleField.getEnemy(i);
	}

	@Override
	public Player getPlayer(int i) {
		return battleField.getPlayer(i);
	}

	@Override
	public void nextActor() {
		battleField.nextActor();
	}

	@Override
	public boolean isBattleInitiated() {
		return battleField != null;
	}

	@Override
	public void useItem(Actor user, Actor usedOn, Item item) {
		battleSystem.executeUseItemCommand(new UseItemCommand(user, usedOn, item));
		battleSystem.nextTurn(battleField);
		update();
	}

	@Override
	public Actor getActor(Actor find) {
		Optional<Enemy> enemy = battleField.getEnemies().stream()
				.filter(e -> e.equals(find))
				.findFirst();
		if(enemy.isPresent()) {
			return enemy.get();
		}
		Optional<Player> player = battleField.getFriends().stream()
				.filter(p -> p.equals(find))
				.findFirst();
		if(player.isPresent()) {
			return player.get();
		}
		return null;
	}

	@Override
	public Actor getCurrentActor() {
		return battleField.getCurrentActor();
	}
}
