package elte.szakdoga.battle.controller.commands;

public interface BattleElement {
	public void accept(BattleVisitor battleVisitor);
}
