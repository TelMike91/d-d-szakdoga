package elte.szakdoga.battle.controller;

import javax.swing.JPanel;

import com.esotericsoftware.kryonet.rmi.ObjectSpace;
import com.esotericsoftware.kryonet.rmi.TimeoutException;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.actors.models.enemy.Enemy;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.battle.graphics.BattlePanel;
import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.battle.models.observer.BattleObserver;
import elte.szakdoga.game.controllers.GameControllerInterface;
import elte.szakdoga.items.models.Item;
import elte.szakdoga.logging.graphics.LoggingFieldLogger;
import elte.szakdoga.network.MyClient;
import elte.szakdoga.network.messages.UpdateBattle;
import elte.szakdoga.network.rmi.BattleInterface;
import elte.szakdoga.network.rmi.ServerDataInterface;

public class BattleControllerWithNetwork implements BattleControllerInterface, BattleObserver {
	private MyClient client;
	private ServerDataInterface serverData;
	private BattleField battleField;
	private BattlePanel battlePanel;
	private GameControllerInterface gameController;
	private BattleSystem battleSystem;
	private int id;

	public BattleControllerWithNetwork(int id, MyClient client, ServerDataInterface serverData,
			GameControllerInterface gameControler) {
		this.serverData = serverData;
		this.battleField = serverData.getBattleField(id);
		serverData.initBattleField(id);
		this.battleField.init();
		this.id = id;
		this.client = client;
		this.gameController = gameControler;
		this.client = client;
		this.battleSystem = new BattleSystem();
		battlePanel = new BattlePanel(this, gameControler.getCurrentPlayer());
		update();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.battle.controller.BattleControllerInterface#ownControl()
	 */
	@Override
	public boolean ownControl() {
		Actor actor = serverData.getCurrentActor(id);
		Actor player = gameController.getCurrentPlayer();
		return actor.equals(player);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.battle.controller.BattleControllerInterface#attack(int)
	 */
	@Override
	public void attack(Actor indexOfEnemy) {
		serverData = ObjectSpace.getRemoteObject(client.getConnection(), 1, ServerDataInterface.class);
		BattleInterface battle = ObjectSpace.getRemoteObject(client.getConnection(), 3, BattleInterface.class);
		LoggingFieldLogger.addText("Attacking!");
		battle.playerAttack(id, indexOfEnemy);
		battleField = serverData.getBattleField(id);
		if (battleField != null)
			battlePanel.update();
		update();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.battle.controller.BattleControllerInterface#doNothing()
	 */
	@Override
	public void doNothing() {
		battleSystem.execute(new DoNothingCommand(battleField));
		LoggingFieldLogger.addText("Doing nothing...");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.battle.controller.BattleControllerInterface#checkIfOver()
	 */
	@Override
	public boolean checkIfOver() {
		boolean isOver = false;
		if (battleField == null)
			isOver = true;
		else
			isOver = battleSystem.checkIfOver(battleField);

		if (isOver) {
			gameController.changeToMapView();
		}
		return isOver;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.battle.controller.BattleControllerInterface#getView()
	 */
	@Override
	public JPanel getView() {
		return battlePanel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * elte.szakdoga.battle.controller.BattleControllerInterface#getNumberOfEnemies(
	 * )
	 */
	@Override
	public int getNumberOfEnemies() {
		return battleField.getNumberOfEnemies();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * elte.szakdoga.battle.controller.BattleControllerInterface#getNumberOfFriends(
	 * )
	 */
	@Override
	public int getNumberOfFriends() {
		return battleField.getNumberOfFriends();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.battle.controller.BattleControllerInterface#getEnemy(int)
	 */
	@Override
	public String getEnemyStringRepresentation(int m) {
		return battleField.getEnemyString(m);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.battle.controller.BattleControllerInterface#update()
	 */
	@Override
	public void update() {
		try {
			BattleInterface battle = ObjectSpace.getRemoteObject(client.getConnection(), 3, BattleInterface.class);
			while (!battle.ownControl(id) && !checkIfOver() && client.isConnected()) {
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				battle = ObjectSpace.getRemoteObject(client.getConnection(), 3, BattleInterface.class);
				battleField = serverData.getBattleField(id);
				if (!checkIfOver()) {
					if (battle.ownControl(id)) {
						battlePanel.getControl();
					} else {
						battlePanel.loseControl();
						battle.takeAction(id);
					}
					battlePanel.update();
				}
			}
			if (!checkIfOver()) {
				if (battle.ownControl(id)) {
					battlePanel.getControl();
				} else {
					battlePanel.loseControl();
					battle.takeAction(id);
				}
				battlePanel.update();
			}
		} catch (TimeoutException execption) {
			LoggingFieldLogger.addText("Server disconnected");
			gameController.gameOver();
		}		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.battle.controller.BattleControllerInterface#getPlayer(int)
	 */
	@Override
	public String getPlayerStringRepresentation(int index) {
		return battleField.getPlayer(index).getName() + " " + battleField.getPlayer(index).getHP();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * elte.szakdoga.battle.controller.BattleControllerInterface#updateText(java.
	 * lang.String)
	 */
	@Override
	public void updateText(String answer) {
		LoggingFieldLogger.addText(answer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.battle.controller.BattleControllerInterface#flee()
	 */
	@Override
	public void flee() {
		serverData = ObjectSpace.getRemoteObject(client.getConnection(), 1, ServerDataInterface.class);
		BattleInterface battle = ObjectSpace.getRemoteObject(client.getConnection(), 3, BattleInterface.class);
		battle.flee(id);
		battleField = serverData.getBattleField(id);
		update();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see elte.szakdoga.battle.controller.BattleControllerInterface#defend()
	 */
	@Override
	public void defend() {
		serverData = ObjectSpace.getRemoteObject(client.getConnection(), 1, ServerDataInterface.class);
		BattleInterface battle = ObjectSpace.getRemoteObject(client.getConnection(), 3, BattleInterface.class);
		battle.defend(id);
		battleField = serverData.getBattleField(id);
		battlePanel.update();
		update();
	}

	@Override
	public BattleField getBattle() {
		return battleField;
	}

	@Override
	public void update(UpdateBattle battle) {
		this.battleField = battle.getBattleField();
		if (!checkIfOver()) {
			if (ownControl()) {
				battlePanel.getControl();
			} else {
				battlePanel.loseControl();
			}
			battlePanel.update();
		}
	}

	@Override
	public Enemy getEnemy(int i) {
		return battleField.getEnemy(i);
	}

	@Override
	public Player getPlayer(int i) {
		return battleField.getPlayer(i);
	}

	@Override
	public void initBattleField(BattleField battleField) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nextActor() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isBattleInitiated() {
		return battleField != null;
	}

	@Override
	public void useItem(Actor user, Actor usedOn, Item item) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Actor getActor(Actor usedOn) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Actor getCurrentActor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setBattleField(BattleField battleField) {
		// TODO Auto-generated method stub
		
	}

}
