package elte.szakdoga.battle.controller;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.battle.controller.commands.BattleElement;
import elte.szakdoga.battle.controller.commands.BattleVisitor;
import elte.szakdoga.items.models.Item;

public class UseItemCommand implements BattleElement {
	private final Actor user;
	private final Actor usedOn;
	private final Item item;

	public UseItemCommand(Actor user, Actor usedOn, Item item) {
		this.user = user;
		this.usedOn = usedOn;
		this.item = item;
	}
	
	

	public Actor getUser() {
		return user;
	}



	public Actor getUsedOn() {
		return usedOn;
	}



	public Item getItem() {
		return item;
	}



	@Override
	public void accept(BattleVisitor battleVisitor) {
		
	}

}
