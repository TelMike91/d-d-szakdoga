package elte.szakdoga.battle.controller.commands;

import elte.szakdoga.battle.controller.DoNothingCommand;
import elte.szakdoga.battle.controller.UseItemCommand;

public interface BattleVisitor {
	void execute(AttackCommand attackCommand);

	void execute(DoNothingCommand doNothingCommand);

	void execute(UseItemCommand useItemCommand);
}
