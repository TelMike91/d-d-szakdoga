package elte.szakdoga.battle.controller;

import javax.swing.JPanel;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.actors.models.enemy.Enemy;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.items.models.Item;

public interface BattleControllerInterface {
	
	void initBattleField(BattleField battleField);

	/**
	 * Ellenőrízzük, hogy a játékos aki most a harcon belül van, az a játékos akié a játék.
	 * @return Igaz, ha a játékos egyezik, hamis ha nem.
	 */
	boolean ownControl();

	/**
	 * Támadás parancsot hajtja végre a megadott indezű ellenfélre.
	 * @param indexOfEnemy Az index amelyik ellenfelet támadjuk.
	 */
	void attack(Actor actor);

	/**
	 * Végrehajt egy "nem csinál semmit" parancsot.
	 * Egyszerűen továbbdobjuk az irányítást a körön következő játékosnak/ellenfélnek.
	 */
	void doNothing();

	/**
	 * Ellenőrzi hogy a harc véget ért.
	 * @return Igaz ha véget ért a harc, hamis különben.
	 */
	boolean checkIfOver();

	JPanel getView();

	int getNumberOfEnemies();

	int getNumberOfFriends();
	
	Enemy getEnemy(int i);
	
	Player getPlayer(int i);

	String getEnemyStringRepresentation(int m);

	/**
	 * Ellenőrzi hogy az irányítás nálunk van és attól függően kapunk funkciókat.
	 * @param answer 
	 */
	void update();

	/**
	 * Visszaadja a string reprezentációt a játékosnak
	 * @param index Melyik indexű játékosnak a reprezentációja kell.
	 * @return A reprezentáció
	 */
	String getPlayerStringRepresentation(int index);

	void updateText(String answer);

	void flee();

	void defend();

	BattleField getBattle();
	
	void useItem(Actor user, Actor usedOn, Item item);

	void nextActor();

	boolean isBattleInitiated();

	Actor getActor(Actor usedOn);

	Actor getCurrentActor();

	void setBattleField(BattleField battleField);

}