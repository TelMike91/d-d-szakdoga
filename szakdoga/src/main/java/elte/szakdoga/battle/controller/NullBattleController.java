package elte.szakdoga.battle.controller;

import javax.swing.JPanel;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.actors.models.enemy.Enemy;
import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.items.models.Item;

public class NullBattleController implements BattleControllerInterface {

	@Override
	public boolean ownControl() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void attack(Actor indexOfEnemy) {
		// TODO Auto-generated method stub

	}

	@Override
	public void doNothing() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean checkIfOver() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public JPanel getView() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getNumberOfEnemies() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getNumberOfFriends() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getEnemyStringRepresentation(int m) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub

	}

	@Override
	public String getPlayerStringRepresentation(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateText(String answer) {
		// TODO Auto-generated method stub

	}

	@Override
	public void flee() {
		// TODO Auto-generated method stub

	}

	@Override
	public void defend() {
		// TODO Auto-generated method stub

	}

	@Override
	public BattleField getBattle() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Enemy getEnemy(int i) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Player getPlayer(int i) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void initBattleField(BattleField battleField) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void nextActor() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isBattleInitiated() {
		return false;
	}

	@Override
	public void useItem(Actor user, Actor usedOn, Item item) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Actor getActor(Actor usedOn) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Actor getCurrentActor() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setBattleField(BattleField battleField) {
		// TODO Auto-generated method stub
		
	}

}
