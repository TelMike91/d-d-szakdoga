package elte.szakdoga.battle.controller.commands;

import elte.szakdoga.actors.models.Actor;

public class AttackCommand {
	private final Actor attacker;
	private final Actor defender;

	public AttackCommand(Actor attacker, Actor defender) {
		this.attacker = attacker;
		this.defender = defender;
	}

	public Actor getAttacker() {
		return attacker;
	}

	public Actor getDefender() {
		return defender;
	}
}
