package elte.szakdoga.battle.controller;

import elte.szakdoga.battle.controller.commands.BattleElement;
import elte.szakdoga.battle.controller.commands.BattleVisitor;
import elte.szakdoga.battle.models.BattleField;

public class DoNothingCommand implements BattleElement {
	private final BattleField battleField;

	public DoNothingCommand(BattleField battleField) {
		this.battleField = battleField;
	}

	@Override
	public void accept(BattleVisitor battleVisitor) {
		battleVisitor.execute(this);
	}

	public BattleField getBattleField() {
		return battleField;
	}

}
