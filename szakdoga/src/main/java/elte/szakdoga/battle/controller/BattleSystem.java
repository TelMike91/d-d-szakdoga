package elte.szakdoga.battle.controller;

import java.util.List;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.actors.models.enemy.Enemy;
import elte.szakdoga.battle.controller.commands.AttackCommand;
import elte.szakdoga.battle.controller.commands.FleeCommand;
import elte.szakdoga.battle.models.BattleField;
import elte.szakdoga.battle.models.BattleLogic;
import elte.szakdoga.battle.models.DiceRollBattleLogic;
import elte.szakdoga.items.controller.ItemService;

public class BattleSystem {	
	private BattleLogic battleLogic;
	private ItemService itemService;
	
	public BattleSystem() {		
		battleLogic = new DiceRollBattleLogic();
		itemService = new ItemService();
	}
	
	public boolean executeAttackCommand(AttackCommand attackCommand) {
		Actor attacker = attackCommand.getAttacker();
		Actor defender = attackCommand.getDefender();	
		return battleLogic.tryToAttack(attacker, defender);	
	}
	
	public void nextTurn(BattleField battleField) {
		battleField.nextActor();
	}
	
	public void executeFleeCommand(FleeCommand fleeCommand) {
		List<Actor> fleeingParty = fleeCommand.getParty();
		List<Actor> opposingParty = fleeCommand.getOpposition();
//		battleController.updateText("Trying to flee");
		boolean success = battleLogic.tryToFlee(fleeingParty, opposingParty);
		if (success) {
//			battleController.updateText("Fleeing successful");		
		}				
	}

	public boolean checkIfOver(BattleField battleField) {		
		return battleField.getNumberOfFriends() == 0 || battleField.getNumberOfEnemies() == 0;
	}

	public void execute(DoNothingCommand doNothingCommand) {
		nextTurn(doNothingCommand.getBattleField());
	}

	public int calculateGoldReward(BattleField battleField) {
		int sum = battleField.getEnemies().stream()
				.mapToInt(Actor::getGold)
				.sum();
		int remainingEnemiesGold = battleField.getRound().stream()
				.filter(e -> e instanceof Enemy)
				.mapToInt(Actor::getGold)
				.sum();
		
		return sum - remainingEnemiesGold;
	}

	public void executeUseItemCommand(UseItemCommand useItemCommand) {
		itemService.useItemOnOther(useItemCommand.getItem(), useItemCommand.getUser(), useItemCommand.getUsedOn());		
	}
}
