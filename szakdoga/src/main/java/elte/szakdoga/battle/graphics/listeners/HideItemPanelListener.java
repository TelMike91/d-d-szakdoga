package elte.szakdoga.battle.graphics.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import elte.szakdoga.battle.graphics.BattlePanel;

public class HideItemPanelListener implements ActionListener {

	private BattlePanel battlePanel;

	public HideItemPanelListener(BattlePanel battlePanel) {
		this.battlePanel = battlePanel;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		battlePanel.hideItemPanel();
	}

}
