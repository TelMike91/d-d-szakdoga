package elte.szakdoga.battle.graphics;

import java.awt.GridLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * A harcban a lehetőségeknek a grafikai reprezentációja.
 * @author Teleki Miklós
 *
 */
@SuppressWarnings("serial")
public class OptionsFieldGraphics extends JPanel {
	private JButton fight;
	private JButton defend;
	private JButton item;
	private JButton escape;
	private JButton back;
	
	/**
	 * Létrehozzuk az alap opciók reprezentációját.
	 */
	public OptionsFieldGraphics() {
		fight = new JButton("Fight");
		defend = new JButton("Defend");
		item = new JButton("Use item");
		escape = new JButton("Escape");
		back = new JButton("Back");
		
		this.setLayout(new GridLayout(4, 1));
		addComponent();
	}

	/**
	 * Hozzáadjuk a harci listeneret, ami a harc gomb megnyomásakor történjen.
	 * @param fightListener A listener, amit szeretnénk ha történjen.
	 */
	public void addFightListener(ActionListener fightListener) {
		fight.addActionListener(fightListener);
	}
	
	/**
	 * Hozzáadjuk a szökési listenert a lehetőségekhez.	 
	 * @param fleeListener A szökési listener
	 */
	public void addFleeListener(ActionListener fleeListener) {
		escape.addActionListener(fleeListener);
	}
	
	public void addUseItemListener(ActionListener useItemListener) {
		item.addActionListener(useItemListener);
	}
	
	/**
	 * Hozzáadjuk a visszagombhoz, aminek történnie kell ha rányomunk.
	 * @param backListener A listener, amit hozzá szeretnénk adni.
	 */
	public void addBackListener(ActionListener backListener) {
		back.addActionListener(backListener);
	}
	
	private void addComponent() {
		this.add(fight);
		this.add(defend);
		this.add(item);
		this.add(escape);
	}
	
	/**
	 * Megjelenítjük a back gombot a panelban.
	 */
	public void showBackButton() {
		this.removeAll();
		this.setLayout(new GridLayout(1,1));
		this.add(back);
		this.repaint();
		this.revalidate();
	}
	
	/**
	 * Megjelnítjük azokat a gombokat, amik alapból helyezkednek el.
	 */
	public void showBattleOptions() {
		this.removeAll();
		this.setLayout(new GridLayout(4, 1));
		addComponent();
		this.repaint();
		this.revalidate();
	}

	/**
	 * Ha megkapjuk az irányítást, akkor megkapjuk a lehetőséget, hogy a gombokat használhassuk.
	 */
	public void getControl() {
		fight.setEnabled(true);
		defend.setEnabled(true);
		item.setEnabled(true);
		escape.setEnabled(true);
		back.setEnabled(true);		
	}

	/**
	 * Ha elveszítjük az irányítást, akkor nem tudjuk használni a gombokat.
	 */
	public void loseControl() {
		fight.setEnabled(false);
		defend.setEnabled(false);
		item.setEnabled(false);
		escape.setEnabled(false);
		back.setEnabled(false);
	}

	public void addDefendListener(ActionListener defendActionListener) {
		defend.addActionListener(defendActionListener);
	}

	public void addItemListener(ActionListener useItemActionListener) {
		item.addActionListener(useItemActionListener);
	}
}
