package elte.szakdoga.battle.graphics;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import elte.szakdoga.actors.models.Actor;

@SuppressWarnings("serial")
public class FriendlyPartyRepresentation extends JLabel {
	private Actor representation;

	public FriendlyPartyRepresentation(ImageIcon imageIcon, Actor representation) {
		super(imageIcon);
		this.representation = representation;
	}

	public Actor getPartyMember() {
		return representation;
	}
	
}
