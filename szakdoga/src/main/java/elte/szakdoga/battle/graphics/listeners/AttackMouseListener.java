package elte.szakdoga.battle.graphics.listeners;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.battle.controller.BattleControllerInterface;

/**
 * Egy egér listener, ami a megfelelő eseményekre reagál aszerint, hogy mit támadunk, és kijelzi nekünk az ellenfelet is.
 * @author Teleki Miklós
 *
 */
public class AttackMouseListener implements MouseListener {
	private JLabel currentEnemy;
	private BattleControllerInterface battleController;	
	private Actor actor;
	
	public AttackMouseListener(JLabel currentEnemy, BattleControllerInterface battleController, Actor actor) {
		this.currentEnemy = currentEnemy;
		this.battleController = battleController;
		this.actor = actor;
	}

	@Override
	public void mouseClicked(MouseEvent event) {
		battleController.attack(actor);				
	}

	@Override
	public void mouseEntered(MouseEvent event) {
		currentEnemy.setBorder(BorderFactory.createLineBorder(Color.RED));
		currentEnemy.setToolTipText(actor.getName());
	}

	@Override
	public void mouseExited(MouseEvent event) {
		currentEnemy.setBorder(null);
	}

	@Override
	public void mousePressed(MouseEvent event) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent event) {
		// TODO Auto-generated method stub
		
	}
}
