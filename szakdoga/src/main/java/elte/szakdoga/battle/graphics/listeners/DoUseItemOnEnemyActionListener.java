package elte.szakdoga.battle.graphics.listeners;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import elte.szakdoga.actors.models.Actor;
import elte.szakdoga.battle.controller.BattleControllerInterface;
import elte.szakdoga.battle.graphics.BattlePanel;
import elte.szakdoga.items.controller.ItemControllerInterface;

public class DoUseItemOnEnemyActionListener implements MouseListener {
	private BattleControllerInterface battleController;
	private ItemControllerInterface itemController;
	private Actor usedOn;
	private BattlePanel battlePanel;
	// TODO Ezt fölbontani
	private JLabel representation;
	
	public DoUseItemOnEnemyActionListener(ItemControllerInterface itemController, 
			JLabel representation,
			BattleControllerInterface battleController, 
			Actor actor, 
			BattlePanel battlePanel) {
		this.battleController = battleController;
		this.itemController = itemController;
		this.usedOn = actor;
		this.battlePanel = battlePanel;
		this.representation = representation;
	}


	@Override
	public void mouseClicked(MouseEvent arg0) {
		battleController.useItem(battleController.getCurrentActor(), battleController.getActor(usedOn), itemController.getCurrentItem());
		battlePanel.hideItemPanel();
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		representation.setBorder(BorderFactory.createLineBorder(Color.red));

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		representation.setBorder(null);
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

}
