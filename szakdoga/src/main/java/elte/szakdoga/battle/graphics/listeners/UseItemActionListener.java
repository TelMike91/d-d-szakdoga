package elte.szakdoga.battle.graphics.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import elte.szakdoga.battle.graphics.BattlePanel;
import elte.szakdoga.battle.graphics.OptionsFieldGraphics;

public class UseItemActionListener implements ActionListener {
	private final OptionsFieldGraphics optionsGraphics;
	private BattlePanel battlePanel;
	
	public UseItemActionListener(OptionsFieldGraphics optionsGraphics, BattlePanel battlePanel) {
		this.optionsGraphics = optionsGraphics;
		this.battlePanel = battlePanel;
	}

	@Override
	public void actionPerformed(ActionEvent event) {				
		battlePanel.showItemPanel();
		optionsGraphics.showBackButton();		
	}
}
