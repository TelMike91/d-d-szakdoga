package elte.szakdoga.battle.graphics.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import elte.szakdoga.battle.graphics.BattleFieldGraphics;
import elte.szakdoga.battle.graphics.OptionsFieldGraphics;

public class FightActionListener implements ActionListener {
	private final OptionsFieldGraphics optionsGraphics;
	private final BattleFieldGraphics battleFieldGraphics;
	
	public FightActionListener(OptionsFieldGraphics optionsGraphics, BattleFieldGraphics battleFieldGraphics) {
		this.optionsGraphics = optionsGraphics;
		this.battleFieldGraphics = battleFieldGraphics;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		optionsGraphics.showBackButton();
		battleFieldGraphics.addClickingFightMouseListener();
	}

}
