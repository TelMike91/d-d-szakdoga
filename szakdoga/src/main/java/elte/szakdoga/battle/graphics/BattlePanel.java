package elte.szakdoga.battle.graphics;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import elte.szakdoga.actors.models.player.Player;
import elte.szakdoga.battle.controller.BattleControllerInterface;
import elte.szakdoga.battle.graphics.listeners.BackActionListener;
import elte.szakdoga.battle.graphics.listeners.DefendActionListener;
import elte.szakdoga.battle.graphics.listeners.FightActionListener;
import elte.szakdoga.battle.graphics.listeners.FleeActionListener;
import elte.szakdoga.battle.graphics.listeners.HideItemPanelListener;
import elte.szakdoga.battle.graphics.listeners.UseItemActionListener;
import elte.szakdoga.items.controller.ItemController;
import elte.szakdoga.items.graphics.ItemPanel;

/**
 * A panel ami tartalmazza a harc grafikát.
 * @author Teleki Miklós
 *
 */
@SuppressWarnings("serial")
public class BattlePanel extends JPanel {
	private BattleFieldGraphics battleFieldGraphics;
	private OptionsFieldGraphics optionsGraphics;	
	private ItemPanel itemPanel;
	private BattleControllerInterface controller;
	private Player player;

	public BattlePanel(BattleControllerInterface controller, Player player) {
		this.setLayout(new BorderLayout());
		this.controller = controller;
		this.player = player;
		battleFieldGraphics = new BattleFieldGraphics(controller);
		this.add(battleFieldGraphics, BorderLayout.WEST);
		optionsGraphics = new OptionsFieldGraphics();
		this.add(optionsGraphics, BorderLayout.CENTER);
		addFightListener();
		addItemListener();
		addBackListener();
		addFleeListener();
		addDefendListener();
	}
	
	private void addItemListener() {
		optionsGraphics.addItemListener(new UseItemActionListener(optionsGraphics, this));		
	}

	/**
	 * Frissítjük a grafikáját a harcnak.
	 */
	public void update() {
		battleFieldGraphics.updateGraphics();
	}
	
	/**
	 * Frissítjük az opciók grafikáját aszerint hogy több irányítást szeretnénk.
	 */
	public void getControl() {
		optionsGraphics.getControl();
		optionsGraphics.showBattleOptions();
	}
	
	/**
	 * Frissítjük az opciók grafikáját aszerint hogy kevesebb irányítást szeretnénk.
	 */
	public void loseControl() {
		optionsGraphics.loseControl();
		optionsGraphics.showBattleOptions();
	}	
	
	private void addDefendListener() {
		optionsGraphics.addDefendListener(new DefendActionListener(controller));
	}

	private void addFleeListener() {		
		optionsGraphics.addFleeListener(new FleeActionListener(controller));
	}

	private void addFightListener() {		
		optionsGraphics.addFightListener(new FightActionListener(optionsGraphics, battleFieldGraphics));
	}
	
	private void addBackListener() {
		optionsGraphics.addBackListener(new BackActionListener(optionsGraphics, battleFieldGraphics));
		optionsGraphics.addBackListener(new HideItemPanelListener(this));
	}

	public void showItemPanel() {
		ItemController test = new ItemController(player);		
		this.itemPanel = new ItemPanel(test);
		
		battleFieldGraphics.addUseItemListener(itemPanel.getItemController(), this);
		itemPanel.updateItemPanel(true);
		itemPanel.dontShowItemButton();
		add(itemPanel, BorderLayout.EAST);
	}

	public void hideItemPanel() {
		if(itemPanel != null)
			this.remove(itemPanel);
	}
}
