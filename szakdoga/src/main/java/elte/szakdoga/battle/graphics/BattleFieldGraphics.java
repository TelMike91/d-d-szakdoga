package elte.szakdoga.battle.graphics;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import elte.szakdoga.battle.controller.BattleControllerInterface;
import elte.szakdoga.battle.graphics.listeners.AttackMouseListener;
import elte.szakdoga.battle.graphics.listeners.DoUseItemOnEnemyActionListener;
import elte.szakdoga.items.controller.ItemControllerInterface;

/**
 * A harcmező grafikai megvalósítása.
 * 
 * @author Teleki Miklós
 *
 */
@SuppressWarnings("serial")
public class BattleFieldGraphics extends JPanel {
	private FriendlyPartyRepresentation[] enemies;
	private FriendlyPartyRepresentation[] party;
	private BattleControllerInterface battleController;

	public BattleFieldGraphics(BattleControllerInterface battleController) {
		enemies = new FriendlyPartyRepresentation[4];
		party = new FriendlyPartyRepresentation[4];
		this.battleController = battleController;
		this.setLayout(new GridBagLayout());
		GridBagConstraints gc = new GridBagConstraints();
		gc.ipady = 40;
		gc.ipadx = 20;
		for (int i = 0; i < battleController.getNumberOfEnemies(); i++) {
			gc.gridx = i;
			gc.gridy = 0;

			enemies[i] = new FriendlyPartyRepresentation(new ImageIcon(battleController.getEnemy(i).getFileName()), battleController.getEnemy(i));
			JLabel enemyHealth = new JLabel(String.valueOf(battleController.getEnemy(i).getHP()));
			enemyHealth.setForeground(Color.YELLOW);
			enemies[i].add(enemyHealth);
			this.add(enemies[i], gc);
		}

		for (int i = 0; i < battleController.getNumberOfFriends(); i++) {
			gc.gridx = i;
			gc.gridy = 1;

			party[i] = new FriendlyPartyRepresentation(new ImageIcon(battleController.getPlayer(i).getFileName()), battleController.getPlayer(i));
			this.add(party[i], gc);
		}
	}

	/**
	 * Hozzáadja a harci listenereket az ellenfelekhez.
	 */
	public void addClickingFightMouseListener() {
		for (int i = 0; i < battleController.getNumberOfEnemies(); i++) {
			JLabel currentEnemy = enemies[i];
			currentEnemy.addMouseListener(new AttackMouseListener(currentEnemy, battleController, battleController.getEnemy(i)));
		}
	}

	public void addUseItemListener(ItemControllerInterface itemController, BattlePanel battlePanel) {
		for (int i = 0; i < battleController.getNumberOfEnemies(); i++) {
			JLabel currentEnemy = enemies[i];
			currentEnemy.addMouseListener(
					new DoUseItemOnEnemyActionListener(itemController, currentEnemy, battleController, enemies[i].getPartyMember(), battlePanel));			
		}
		for (int i = 0; i < battleController.getNumberOfFriends(); i++) {
			JLabel currentPlayer = party[i];
			currentPlayer.addMouseListener(
					new DoUseItemOnEnemyActionListener(itemController, currentPlayer, battleController, party[i].getPartyMember(), battlePanel));			
		}
	}

	/**
	 * Eltávolítja a harci listenerekt az ellenfelektől.
	 */
	public void removeClickingMouseListeners() {
		for (int i = 0; i < battleController.getNumberOfEnemies(); i++) {
			MouseListener[] mouseListeners = enemies[i].getMouseListeners();
			for (int m = 0; m < mouseListeners.length; m++)
				enemies[i].removeMouseListener(mouseListeners[m]);
		}
		for (int i = 0; i < battleController.getNumberOfFriends(); i++) {
			MouseListener[] mouseListeners = party[i].getMouseListeners();
			for (int m = 0; m < mouseListeners.length; m++)
				party[i].removeMouseListener(mouseListeners[m]);
		}
	}

	/**
	 * Frissítjük a grafikát aszerint, ahogy alakult a harc. Ilyenkor elvesznek az
	 * ActionListenerek, amikre figyelni kell
	 */
	public void updateGraphics() {
		this.removeAll();
		GridBagConstraints gc = new GridBagConstraints();
		gc.ipady = 40;
		gc.ipadx = 20;
		for (int i = 0; i < battleController.getNumberOfEnemies(); i++) {
			gc.gridx = i;
			gc.gridy = 0;

			enemies[i] = new FriendlyPartyRepresentation(new ImageIcon(battleController.getEnemy(i).getFileName()), battleController.getEnemy(i));
			enemies[i].setLayout(new FlowLayout());
			JLabel enemyHealth = new JLabel("Enemy HP" + enemies[i].getPartyMember().getHP());
			enemyHealth.setForeground(Color.RED);
			enemies[i].add(enemyHealth);
			this.add(enemies[i], gc);
		}

		for (int i = 0; i < battleController.getNumberOfFriends(); i++) {
			gc.gridx = i;
			gc.gridy = 1;

			party[i] = new FriendlyPartyRepresentation(new ImageIcon(battleController.getPlayer(i).getFileName()), battleController.getPlayer(i));
			party[i].setLayout(new FlowLayout());
			
			JLabel playerHealth = new JLabel("My HP" + party[i].getPartyMember().getHP());
			playerHealth.setForeground(Color.RED);
			party[i].add(playerHealth);
			this.add(party[i], gc);
		}
		this.revalidate();
	}
}
