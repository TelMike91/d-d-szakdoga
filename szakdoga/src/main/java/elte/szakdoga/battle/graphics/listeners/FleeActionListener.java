package elte.szakdoga.battle.graphics.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import elte.szakdoga.battle.controller.BattleControllerInterface;

public class FleeActionListener implements ActionListener {
	private BattleControllerInterface battleController;

	public FleeActionListener(BattleControllerInterface battleController) {
		this.battleController = battleController;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		battleController.flee();
	}

}
