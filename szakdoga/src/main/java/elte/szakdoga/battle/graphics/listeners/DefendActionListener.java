package elte.szakdoga.battle.graphics.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import elte.szakdoga.battle.controller.BattleControllerInterface;

public class DefendActionListener implements ActionListener {
	private BattleControllerInterface battleController;

	public DefendActionListener(BattleControllerInterface controller) {
		this.battleController = controller;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		battleController.defend();
	}

}
