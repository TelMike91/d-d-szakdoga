package elte.szakdoga.messages.graphics;

import javax.swing.JButton;

import elte.szakdoga.graphics.decisions.Decision;
import elte.szakdoga.narrator.controllers.NarratorController;

public class DecisionButtonFactory {	

	public static JButton createDecisionButton(NarratorController controller, Decision decision) {
		DecisionVisitor visitor = new DecisionVisitor(controller);
		decision.visit(visitor);		
		return visitor.getLastVisited();
	}

}
