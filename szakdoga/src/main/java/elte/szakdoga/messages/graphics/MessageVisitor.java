package elte.szakdoga.messages.graphics;

import javax.swing.JOptionPane;

import elte.szakdoga.game.graphics.MainWindow;
import elte.szakdoga.messages.models.DecisionMessage;
import elte.szakdoga.messages.models.PlainMessage;
import elte.szakdoga.narrator.controllers.NarratorController;

/**
 * Ez egy Visitor Design pattern-t megvalósító osztály. Az üzenettől függően fogja megjeleníteni a játékosknak a megfelelő DialogBox-ot
 * @author Miki
 *
 */
public class MessageVisitor {
	private MainWindow window;
	private NarratorController controller;
	
	public MessageVisitor(NarratorController controller, MainWindow window) {
		this.controller = controller;
		this.window = window;
	}
	
	public void showMessage(DecisionMessage decisionMessage) {				
		DecisionJDialog dialogBox = new DecisionJDialog(decisionMessage.getMessage(), controller, decisionMessage.getDecisions());
		dialogBox.setLocation(window.getMiddlePoint());
		dialogBox.setVisible(true);
		dialogBox.pack();
	}
	
	public void showMessage(PlainMessage message) {
		JOptionPane.showMessageDialog(window, message.getMessage(), "Plain message", JOptionPane.PLAIN_MESSAGE);
	}
}
