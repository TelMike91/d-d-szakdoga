package elte.szakdoga.messages.graphics;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;

import elte.szakdoga.graphics.decisions.Decision;
import elte.szakdoga.narrator.controllers.NarratorController;

@SuppressWarnings("serial")
public class DecisionJDialog extends JDialog {
	private JLabel message;
	private List<JButton> decisions;
	private static final GridBagConstraints MESSAGE_POSITION = new GridBagConstraints();

	public DecisionJDialog(String message, NarratorController controller, List<Decision> decisions) {
		this.message = new JLabel(message);
		this.setLayout(new GridBagLayout());
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		MESSAGE_POSITION.gridx = 0;
		MESSAGE_POSITION.gridy = 0;
		this.add(this.message, MESSAGE_POSITION);
		this.decisions = new LinkedList<>();
		for(Decision decision : decisions) {
			this.decisions.add(DecisionButtonFactory.createDecisionButton(controller, decision));
		}
		GridBagConstraints buttonPosition = new GridBagConstraints();
		buttonPosition.gridx = 0;
		buttonPosition.gridy = 1;
		buttonPosition.insets = new Insets(0, 10, 0, 10);		
		for(JButton decision : this.decisions) {
			this.add(decision, buttonPosition);
			decision.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					closeDialog();
				}
			});			
			buttonPosition.gridx++;
		}		
	}

	protected void closeDialog() {
		dispose();
	}	
}
