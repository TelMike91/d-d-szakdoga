package elte.szakdoga.messages.graphics;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import elte.szakdoga.graphics.decisions.BattleDecision;
import elte.szakdoga.graphics.decisions.DoNothingDecision;
import elte.szakdoga.graphics.decisions.LoseHealthDecision;
import elte.szakdoga.narrator.controllers.NarratorController;

public class DecisionVisitor {	
	private JButton button;
	private NarratorController narrator;
	
	public DecisionVisitor(NarratorController narratorController) {
		narrator = narratorController;
	}

	public void visit(BattleDecision battleDecision) {
		button = new JButton("Fight");
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				battleDecision.doDecision(narrator.getNarrator());
			}
		});
	}		

	public JButton getLastVisited() {
		return button;
	}

	public void visit(DoNothingDecision doNothingDecision) {
		button = new JButton("Do nothing...");
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				doNothingDecision.doDecision(narrator.getNarrator());				
			}
		});
	}

	public void visit(LoseHealthDecision loseHealthDecision) {
		button = new JButton("Lose health");
		button.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				loseHealthDecision.doDecision(narrator.getNarrator());				
			}
		});
		
	}

}
