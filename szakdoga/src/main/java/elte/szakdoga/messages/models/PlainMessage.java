package elte.szakdoga.messages.models;

import elte.szakdoga.messages.graphics.MessageVisitor;

public class PlainMessage implements MyMessage {
	private String message;
	
	public PlainMessage() {
	
	}

	public PlainMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public void visit(MessageVisitor visitor) {
		visitor.showMessage(this);
	}

	@Override
	public boolean isVisibleInBattle() {
		return true;
	}
}
