package elte.szakdoga.messages.models;

import java.util.List;

import elte.szakdoga.graphics.decisions.Decision;
import elte.szakdoga.messages.graphics.MessageVisitor;
/**
 * Ez az osztály tárolja a döntési üzenetet, amit a narrátor küldhet a játékosoknak.
 * @author Miki
 *
 */
public class DecisionMessage implements MyMessage {
	
	private List<Decision> decisions;
	private String message;
	private boolean visibleInBattle;
	
	
		
	public void setVisibleInBattle(boolean visibleInBattle) {
		this.visibleInBattle = visibleInBattle;
	}

	public DecisionMessage() {
	
	}

	public DecisionMessage(String message, List<Decision> decisions) {
		this.message = message;
		this.decisions = decisions;
	}
	
	public List<Decision> getDecisions() {
		return decisions;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public void visit(MessageVisitor visitor) {
		visitor.showMessage(this);
	}

	@Override
	public boolean isVisibleInBattle() {
		return visibleInBattle;
	}
}
