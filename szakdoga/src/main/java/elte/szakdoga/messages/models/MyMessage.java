package elte.szakdoga.messages.models;

import elte.szakdoga.messages.graphics.MessageVisitor;

public interface MyMessage {
	public void visit(MessageVisitor visitor);

	public boolean isVisibleInBattle();
}
